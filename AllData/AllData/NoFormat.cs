﻿namespace System.Windows.Forms
{
    public static class Methods
    {
        /// <summary>
        /// Remove a formatação da máscara e retorna o texto sem formatação
        /// </summary>
        /// <param name="_mask">Máscara a ser retirada</param>
        /// <returns>Texto sem máscara</returns>
        public static string TextNoFormatting(this MaskedTextBox _mask)
        {
            _mask.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            string retString = _mask.Text;
            _mask.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
            return retString;
        }
    }
}
