﻿namespace AllData
{
    partial class EditStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditStudent));
            this.Head1 = new System.Windows.Forms.GroupBox();
            this.DeleteBtn1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.EditBtn1 = new System.Windows.Forms.Button();
            this.AlunoComb2 = new System.Windows.Forms.ComboBox();
            this.alunoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSet = new AllData.MainDataSet();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Status = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.RegisteringDate1 = new System.Windows.Forms.DateTimePicker();
            this.Birthdate1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RespComb2 = new System.Windows.Forms.ComboBox();
            this.responsavelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.NomeBox1 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Tel3Box3 = new System.Windows.Forms.MaskedTextBox();
            this.Tel2Box3 = new System.Windows.Forms.MaskedTextBox();
            this.Tel1Box3 = new System.Windows.Forms.MaskedTextBox();
            this.CommitBtn3 = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.alunoTableAdapter = new AllData.MainDataSetTableAdapters.AlunoTableAdapter();
            this.responsavelTableAdapter = new AllData.MainDataSetTableAdapters.ResponsavelTableAdapter();
            this.Head1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.Status.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.responsavelBindingSource)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Head1
            // 
            this.Head1.Controls.Add(this.DeleteBtn1);
            this.Head1.Controls.Add(this.label1);
            this.Head1.Controls.Add(this.EditBtn1);
            this.Head1.Controls.Add(this.AlunoComb2);
            this.Head1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Head1.Location = new System.Drawing.Point(0, 0);
            this.Head1.Name = "Head1";
            this.Head1.Size = new System.Drawing.Size(591, 93);
            this.Head1.TabIndex = 0;
            this.Head1.TabStop = false;
            this.Head1.Text = "Aluno";
            // 
            // DeleteBtn1
            // 
            this.DeleteBtn1.Location = new System.Drawing.Point(289, 64);
            this.DeleteBtn1.Name = "DeleteBtn1";
            this.DeleteBtn1.Size = new System.Drawing.Size(69, 23);
            this.DeleteBtn1.TabIndex = 3;
            this.DeleteBtn1.Text = "Deletar";
            this.DeleteBtn1.UseVisualStyleBackColor = true;
            this.DeleteBtn1.Click += new System.EventHandler(this.DeleteBtn1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Aluno";
            // 
            // EditBtn1
            // 
            this.EditBtn1.Location = new System.Drawing.Point(364, 64);
            this.EditBtn1.Name = "EditBtn1";
            this.EditBtn1.Size = new System.Drawing.Size(75, 23);
            this.EditBtn1.TabIndex = 1;
            this.EditBtn1.Text = "Editar";
            this.EditBtn1.UseVisualStyleBackColor = true;
            this.EditBtn1.Click += new System.EventHandler(this.EditBtn1_Click);
            // 
            // AlunoComb2
            // 
            this.AlunoComb2.DataSource = this.alunoBindingSource;
            this.AlunoComb2.DisplayMember = "Nome";
            this.AlunoComb2.FormattingEnabled = true;
            this.AlunoComb2.Location = new System.Drawing.Point(151, 19);
            this.AlunoComb2.Name = "AlunoComb2";
            this.AlunoComb2.Size = new System.Drawing.Size(288, 21);
            this.AlunoComb2.TabIndex = 0;
            this.AlunoComb2.ValueMember = "ID";
            // 
            // alunoBindingSource
            // 
            this.alunoBindingSource.DataMember = "Aluno";
            this.alunoBindingSource.DataSource = this.mainDataSet;
            // 
            // mainDataSet
            // 
            this.mainDataSet.DataSetName = "MainDataSet";
            this.mainDataSet.EnforceConstraints = false;
            this.mainDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(0, 93);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(382, 263);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informações";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Status);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.RegisteringDate1);
            this.groupBox3.Controls.Add(this.Birthdate1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 116);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(376, 144);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            // 
            // Status
            // 
            this.Status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.Status.Location = new System.Drawing.Point(3, 119);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(370, 22);
            this.Status.TabIndex = 7;
            this.Status.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(39, 17);
            this.StatusLabel.Text = "Status";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Matrícula";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Nascimento";
            // 
            // RegisteringDate1
            // 
            this.RegisteringDate1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.RegisteringDate1.Location = new System.Drawing.Point(126, 45);
            this.RegisteringDate1.Name = "RegisteringDate1";
            this.RegisteringDate1.ShowCheckBox = true;
            this.RegisteringDate1.Size = new System.Drawing.Size(200, 20);
            this.RegisteringDate1.TabIndex = 8;
            // 
            // Birthdate1
            // 
            this.Birthdate1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Birthdate1.Location = new System.Drawing.Point(126, 19);
            this.Birthdate1.Name = "Birthdate1";
            this.Birthdate1.ShowCheckBox = true;
            this.Birthdate1.Size = new System.Drawing.Size(200, 20);
            this.Birthdate1.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RespComb2);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.NomeBox1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(376, 100);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // RespComb2
            // 
            this.RespComb2.DataSource = this.responsavelBindingSource;
            this.RespComb2.DisplayMember = "Nome";
            this.RespComb2.FormattingEnabled = true;
            this.RespComb2.Location = new System.Drawing.Point(126, 45);
            this.RespComb2.Name = "RespComb2";
            this.RespComb2.Size = new System.Drawing.Size(200, 21);
            this.RespComb2.TabIndex = 3;
            this.RespComb2.ValueMember = "ID";
            // 
            // responsavelBindingSource
            // 
            this.responsavelBindingSource.DataMember = "Responsavel";
            this.responsavelBindingSource.DataSource = this.mainDataSet;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Responsável";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Nome";
            // 
            // NomeBox1
            // 
            this.NomeBox1.Location = new System.Drawing.Point(126, 19);
            this.NomeBox1.Name = "NomeBox1";
            this.NomeBox1.Size = new System.Drawing.Size(200, 20);
            this.NomeBox1.TabIndex = 2;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Tel3Box3);
            this.groupBox4.Controls.Add(this.Tel2Box3);
            this.groupBox4.Controls.Add(this.Tel1Box3);
            this.groupBox4.Controls.Add(this.CommitBtn3);
            this.groupBox4.Controls.Add(this.CancelBtn);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(382, 93);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(209, 263);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Telefones";
            // 
            // Tel3Box3
            // 
            this.Tel3Box3.Location = new System.Drawing.Point(70, 87);
            this.Tel3Box3.Mask = "(99) 9 0000-0000";
            this.Tel3Box3.Name = "Tel3Box3";
            this.Tel3Box3.PromptChar = ' ';
            this.Tel3Box3.Size = new System.Drawing.Size(127, 20);
            this.Tel3Box3.TabIndex = 6;
            // 
            // Tel2Box3
            // 
            this.Tel2Box3.Location = new System.Drawing.Point(70, 61);
            this.Tel2Box3.Mask = "(99) 9 0000-0000";
            this.Tel2Box3.Name = "Tel2Box3";
            this.Tel2Box3.PromptChar = ' ';
            this.Tel2Box3.Size = new System.Drawing.Size(127, 20);
            this.Tel2Box3.TabIndex = 5;
            // 
            // Tel1Box3
            // 
            this.Tel1Box3.Location = new System.Drawing.Point(70, 35);
            this.Tel1Box3.Mask = "(99) 9 0000-0000";
            this.Tel1Box3.Name = "Tel1Box3";
            this.Tel1Box3.PromptChar = ' ';
            this.Tel1Box3.Size = new System.Drawing.Size(127, 20);
            this.Tel1Box3.TabIndex = 4;
            // 
            // CommitBtn3
            // 
            this.CommitBtn3.Location = new System.Drawing.Point(122, 237);
            this.CommitBtn3.Name = "CommitBtn3";
            this.CommitBtn3.Size = new System.Drawing.Size(75, 23);
            this.CommitBtn3.TabIndex = 9;
            this.CommitBtn3.Text = "Confirmar";
            this.CommitBtn3.UseVisualStyleBackColor = true;
            this.CommitBtn3.Click += new System.EventHandler(this.CommitBtn3_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Location = new System.Drawing.Point(36, 237);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 23);
            this.CancelBtn.TabIndex = 7;
            this.CancelBtn.Text = "Cancelar";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Telefone 3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Telefone 2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Telefone 1";
            // 
            // alunoTableAdapter
            // 
            this.alunoTableAdapter.ClearBeforeFill = true;
            // 
            // responsavelTableAdapter
            // 
            this.responsavelTableAdapter.ClearBeforeFill = true;
            // 
            // EditStudent
            // 
            this.AcceptButton = this.CommitBtn3;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(591, 356);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.Head1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditStudent";
            this.Text = "Editar Informações";
            this.Load += new System.EventHandler(this.EditStudent_Load);
            this.Head1.ResumeLayout(false);
            this.Head1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.Status.ResumeLayout(false);
            this.Status.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.responsavelBindingSource)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Head1;
        private System.Windows.Forms.Button DeleteBtn1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button EditBtn1;
        private System.Windows.Forms.ComboBox AlunoComb2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.StatusStrip Status;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker RegisteringDate1;
        private System.Windows.Forms.DateTimePicker Birthdate1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox RespComb2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox NomeBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button CommitBtn3;
        private System.Windows.Forms.Button CancelBtn;
        private MainDataSet mainDataSet;
        private System.Windows.Forms.BindingSource alunoBindingSource;
        private MainDataSetTableAdapters.AlunoTableAdapter alunoTableAdapter;
        private System.Windows.Forms.BindingSource responsavelBindingSource;
        private MainDataSetTableAdapters.ResponsavelTableAdapter responsavelTableAdapter;
        private System.Windows.Forms.MaskedTextBox Tel1Box3;
        private System.Windows.Forms.MaskedTextBox Tel2Box3;
        private System.Windows.Forms.MaskedTextBox Tel3Box3;
    }
}