﻿namespace AllData
{
    partial class DuplicatasForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DuplicatasForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.TypeComb = new System.Windows.Forms.ComboBox();
            this.WatchDuplicatesBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TabelaComb = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.DuplicatasGridView = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DuplicatasGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.TypeComb);
            this.panel1.Controls.Add(this.WatchDuplicatesBtn);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.TabelaComb);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(631, 61);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Por";
            // 
            // TypeComb
            // 
            this.TypeComb.Enabled = false;
            this.TypeComb.FormattingEnabled = true;
            this.TypeComb.Items.AddRange(new object[] {
            "Nome",
            "CPF",
            "Nome e CPF"});
            this.TypeComb.Location = new System.Drawing.Point(88, 37);
            this.TypeComb.Name = "TypeComb";
            this.TypeComb.Size = new System.Drawing.Size(121, 21);
            this.TypeComb.TabIndex = 3;
            this.TypeComb.SelectedIndexChanged += new System.EventHandler(this.TypeComb_SelectedIndexChanged);
            // 
            // WatchDuplicatesBtn
            // 
            this.WatchDuplicatesBtn.Location = new System.Drawing.Point(305, 10);
            this.WatchDuplicatesBtn.Name = "WatchDuplicatesBtn";
            this.WatchDuplicatesBtn.Size = new System.Drawing.Size(112, 23);
            this.WatchDuplicatesBtn.TabIndex = 2;
            this.WatchDuplicatesBtn.Text = "Ver Duplicatas";
            this.WatchDuplicatesBtn.UseVisualStyleBackColor = true;
            this.WatchDuplicatesBtn.Click += new System.EventHandler(this.WatchDuplicatesBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tabela";
            // 
            // TabelaComb
            // 
            this.TabelaComb.FormattingEnabled = true;
            this.TabelaComb.Items.AddRange(new object[] {
            "Alunos",
            "Responsável"});
            this.TabelaComb.Location = new System.Drawing.Point(49, 12);
            this.TabelaComb.Name = "TabelaComb";
            this.TabelaComb.Size = new System.Drawing.Size(239, 21);
            this.TabelaComb.TabIndex = 0;
            this.TabelaComb.SelectedIndexChanged += new System.EventHandler(this.TabelaComb_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.DuplicatasGridView);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 61);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(631, 319);
            this.panel2.TabIndex = 1;
            // 
            // DuplicatasGridView
            // 
            this.DuplicatasGridView.AllowUserToAddRows = false;
            this.DuplicatasGridView.AllowUserToDeleteRows = false;
            this.DuplicatasGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DuplicatasGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DuplicatasGridView.Location = new System.Drawing.Point(0, 0);
            this.DuplicatasGridView.Name = "DuplicatasGridView";
            this.DuplicatasGridView.ReadOnly = true;
            this.DuplicatasGridView.Size = new System.Drawing.Size(631, 319);
            this.DuplicatasGridView.TabIndex = 0;
            // 
            // DuplicatasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 380);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DuplicatasForm";
            this.Text = "Dados duplicados";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DuplicatasGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView DuplicatasGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox TabelaComb;
        private System.Windows.Forms.Button WatchDuplicatesBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox TypeComb;
    }
}