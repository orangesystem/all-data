﻿namespace AllData
{
    partial class LogonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NomeBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PasswordBox1 = new System.Windows.Forms.TextBox();
            this.LogonBtn = new System.Windows.Forms.Button();
            this.ExitBtn1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // NomeBox1
            // 
            this.NomeBox1.Location = new System.Drawing.Point(61, 41);
            this.NomeBox1.Name = "NomeBox1";
            this.NomeBox1.Size = new System.Drawing.Size(203, 20);
            this.NomeBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Senha";
            // 
            // PasswordBox1
            // 
            this.PasswordBox1.Location = new System.Drawing.Point(61, 78);
            this.PasswordBox1.Name = "PasswordBox1";
            this.PasswordBox1.PasswordChar = '*';
            this.PasswordBox1.Size = new System.Drawing.Size(203, 20);
            this.PasswordBox1.TabIndex = 3;
            // 
            // LogonBtn
            // 
            this.LogonBtn.Location = new System.Drawing.Point(236, 7);
            this.LogonBtn.Name = "LogonBtn";
            this.LogonBtn.Size = new System.Drawing.Size(75, 23);
            this.LogonBtn.TabIndex = 4;
            this.LogonBtn.Text = "LOGON";
            this.LogonBtn.UseVisualStyleBackColor = true;
            this.LogonBtn.Click += new System.EventHandler(this.LogonBtn_Click);
            // 
            // ExitBtn1
            // 
            this.ExitBtn1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExitBtn1.Location = new System.Drawing.Point(155, 7);
            this.ExitBtn1.Name = "ExitBtn1";
            this.ExitBtn1.Size = new System.Drawing.Size(75, 23);
            this.ExitBtn1.TabIndex = 5;
            this.ExitBtn1.Text = "SAIR";
            this.ExitBtn1.UseVisualStyleBackColor = true;
            this.ExitBtn1.Click += new System.EventHandler(this.ExitBtn1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.NomeBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.PasswordBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(323, 180);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Logon no Servidor";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LogonBtn);
            this.panel1.Controls.Add(this.ExitBtn1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 147);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(323, 33);
            this.panel1.TabIndex = 7;
            // 
            // LogonForm
            // 
            this.AcceptButton = this.LogonBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ExitBtn1;
            this.ClientSize = new System.Drawing.Size(323, 180);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LogonForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Logon";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox NomeBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PasswordBox1;
        private System.Windows.Forms.Button LogonBtn;
        private System.Windows.Forms.Button ExitBtn1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
    }
}