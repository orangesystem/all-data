﻿using System;
using System.Windows.Forms;

namespace AllData
{
    public partial class DuplicatasForm : Form
    {
        enum ResponsavelFilterType { Nome = 0, CPF = 1, NomeCPF = 2};
        private ResponsavelFilterType resp;
        public DialogResult resultado;

        public DuplicatasForm()
        {
            InputForm fm = new InputForm();
            if (fm.ShowDialog() == DialogResult.OK)
            {
                resultado = DialogResult.OK;
                InitializeComponent();
            }
            else { resultado = DialogResult.No; Close(); }
        }

        private void TabelaComb_SelectedIndexChanged(object sender, EventArgs e)
        {
            TypeComb.Enabled = (TabelaComb.SelectedIndex == 1) ? true : false;
        }

        private void WatchDuplicatesBtn_Click(object sender, EventArgs e)
        {
            switch (TabelaComb.SelectedIndex)
            {
                case 0:
                    DuplicatasGridView.DataSource = DataHelper.GetAlunoDuplicatasByName();
                    break;

                case 1:
                    switch(resp)
                    {
                        case ResponsavelFilterType.CPF:
                            DuplicatasGridView.DataSource = DataHelper.GetResponsavelDuplicatasByCPF();
                            break;

                        case ResponsavelFilterType.Nome:
                            DuplicatasGridView.DataSource = DataHelper.GetResponsavelDuplicatasByName();
                            break;

                        case ResponsavelFilterType.NomeCPF:
                            DuplicatasGridView.DataSource = DataHelper.GetResponsavelDuplicatasByNameAndCPF();
                            break;

                        default:
                            DuplicatasGridView.DataSource = DataHelper.GetResponsavelDuplicatasByName();
                            break;
                    }
                    break;

                default:
                    DuplicatasGridView.DataSource = DataHelper.GetAlunoDuplicatasByName();
                    break;
            }
        }

        private void TypeComb_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(TypeComb.SelectedIndex)
            {
                case 0:
                    resp = ResponsavelFilterType.CPF;
                    break;

                case 1:
                    resp = ResponsavelFilterType.Nome;
                    break;

                case 2:
                    resp = ResponsavelFilterType.NomeCPF;
                    break;
            }
        }
    }
}
