﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace AllData
{
    public enum ComandType { Insert = 0, Update, Delete };

    public class OfflineModule
    {
        protected int OfflineIDComandCallID { get { SetNextComand(); return offlineIDComand; } }
        internal ComandType Comando { get { return _comando; } set { _comando = value; } }

        static int _offlineIDComand = 0;
        private int offlineIDComand;
        private ComandType _comando;

        private void SetNextComand()
        {
            offlineIDComand = _offlineIDComand;
            _offlineIDComand++;
        }

        static public void SetComand(int nextComand)
        {
            _offlineIDComand = nextComand;
        }
    }

    public class OfflineDataSet : DataSet
    {
        #region Modificadores
        #region Publicos
        public OfflineAlunoDataTable OfflineAlunoDataTable { get { return _offlineAlunoDataTable; } set { _offlineAlunoDataTable = value; } }
        public OfflineResponsavelDataTable OfflineResponsavelDataTable { get { return _offlineResponsavelDataTable; } set { _offlineResponsavelDataTable = value; } }
        public OfflinePagamentoDataTable OfflinePagamentoDataTable { get { return _offlinePagamentoDataTable; } set { _offlinePagamentoDataTable = value; } }
        public OfflinePontuacaoDataTable OfflinePontuacaoDataTable { get { return _offlinePontuacaoDataTable; } set { _offlinePontuacaoDataTable = value; } }

        public OfflineAlunoRow OfflineAlunoRow { get { return _offlineAlunoRow; } set { _offlineAlunoRow = value; } }
        public OfflineResponsavelRow OfflineResponsavelRow { get { return _offlineResponsavelRow; } set { _offlineResponsavelRow = value; } }
        public OfflinePagamentoRow OfflinePagamentoRow { get { return _offlinePagamentoRow; } set { _offlinePagamentoRow = value; } }
        public OfflinePontuacaoRow OfflinePontuacaoRow { get { return _offlinePontuacaoRow; } set { _offlinePontuacaoRow = value; } }
        #endregion

        #region Privados
        private OfflineAlunoDataTable _offlineAlunoDataTable;
        private OfflineResponsavelDataTable _offlineResponsavelDataTable;
        private OfflinePagamentoDataTable _offlinePagamentoDataTable;
        private OfflinePontuacaoDataTable _offlinePontuacaoDataTable;

        private OfflineAlunoRow _offlineAlunoRow;
        private OfflineResponsavelRow _offlineResponsavelRow;
        private OfflinePagamentoRow _offlinePagamentoRow;
        private OfflinePontuacaoRow _offlinePontuacaoRow;
        #endregion
        #endregion

        private string schemaPath;
        private string comandPath;

        public OfflineDataSet()
        {
            DataSetName = "Offline Comands DataSet";

            _offlineAlunoDataTable = new OfflineAlunoDataTable();
            _offlineResponsavelDataTable = new OfflineResponsavelDataTable();
            _offlinePagamentoDataTable = new OfflinePagamentoDataTable();
            _offlinePontuacaoDataTable = new OfflinePontuacaoDataTable();

            _offlineAlunoRow = new OfflineAlunoRow();
            _offlineResponsavelRow = new OfflineResponsavelRow();
            _offlinePagamentoRow = new OfflinePagamentoRow();
            _offlinePontuacaoRow = new OfflinePontuacaoRow();

            Tables.Add(OfflineAlunoDataTable);
            Tables.Add(OfflineResponsavelDataTable);
            Tables.Add(OfflinePagamentoDataTable);
            Tables.Add(OfflinePontuacaoDataTable);

            schemaPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\Orange System\Schema.adst";
            comandPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\Orange System\Comands.adtc";
        }

        public void ReadSavedData()
        {
            try { ReadXmlSchema(schemaPath); }
            catch
            {
                try
                {
                    WriteXmlSchema(schemaPath);
                }
                catch (DirectoryNotFoundException)
                {
                    Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\Orange System");
                }
            }
            try { ReadXml(comandPath); }
            catch { }
            int comand;
            comand = OfflineAlunoDataTable.Rows.Count;
            comand += OfflineResponsavelDataTable.Rows.Count;
            comand += OfflinePagamentoDataTable.Rows.Count;
            comand += _offlinePontuacaoDataTable.Rows.Count;

            OfflineModule.SetComand(comand);
        }

        public void SaveData()
        {
            try
            {
                FileStream cleaner = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\Orange System\Comands.adtc", FileMode.Truncate);
                cleaner.Close();
            }
            catch (FileNotFoundException) { }
            WriteXml(comandPath);
        }

        public void ClearSavedData()
        {
            try
            {
                FileStream cleaner = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\Orange System\Comands.adtc", FileMode.Truncate);
                cleaner.Close();
            }
            catch (FileNotFoundException) { }
        }

        static public ComandType ParseComand(string comando)
        {
            switch (comando)
            {
                case "Insert":
                    return ComandType.Insert;


                case "Update":
                    return ComandType.Update;


                case "Delete":
                    ;
                    return ComandType.Delete;

                default:
                    return ComandType.Insert;
            }
        }
    }

    #region Classes
    public class OfflineAluno : OfflineModule
    {
        public int OfflineIDComand { get { return offlineIdComand; } set { offlineIdComand = value; } }
        public Guid ID { get { return _id; } set { _id = value; } }
        public string Nome { get { return _nome; } set { _nome = value; } }
        public DateTime Nascimento { get { return _nascimento; } set { _nascimento = value; } }
        public string Telefone1 { get { return _telefone1; } set { _telefone1 = value; } }
        public string Telefone2 { get { return _telefone2; } set { _telefone2 = value; } }
        public string Telefone3 { get { return _telefone3; } set { _telefone3 = value; } }
        public int Responsavel { get { return _responsavel; } set { _responsavel = value; } }
        public DateTime? Matricula { get { return _matricula; } set { _matricula = value; } }

        private int offlineIdComand;
        private Guid _id;
        private string _nome;
        private DateTime _nascimento;
        private string _telefone1;
        private string _telefone2;
        private string _telefone3;
        private int _responsavel;
        private DateTime? _matricula;

        public OfflineAluno()
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = new Guid();
            _nome = "";
            _nascimento = new DateTime();
            _telefone1 = "";
            _telefone2 = "";
            _telefone3 = "";
            _responsavel = new int();
            _matricula = new DateTime();
        }

        public OfflineAluno(MainDataSet.AlunoRow aluno, ComandType comando)
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = aluno.ID;
            _nome = aluno.Nome;
            _nascimento = aluno.Nascimento;
            _telefone1 = aluno.Telefone1;
            _telefone2 = aluno.Telefone2;
            _telefone3 = aluno.Telefone3;
            _responsavel = aluno.Responsável;
            _matricula = aluno.Matricula;
            Comando = comando;

        }

        public OfflineAluno(Guid id, string nome, DateTime nascimento, string telefone1, string telefone2, string telefone3, int responsavel, DateTime? matricula, ComandType comando)
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = id;
            _nome = nome;
            _nascimento = nascimento;
            _telefone1 = telefone1;
            _telefone2 = telefone2;
            _telefone3 = telefone3;
            _responsavel = responsavel;
            _matricula = matricula;
            Comando = comando;
        }

    }

    public class OfflineResponsavel : OfflineModule
    {
        public int OfflineIDComand { get { return offlineIdComand; } set { offlineIdComand = value; } }
        public int ID { get { return _id; } set { _id = value; } }
        public string Nome { get { return _nome; } set { _nome = value; } }
        public string CPF { get { return _cpf; } set { _cpf = value; } }
        public string Telefone1 { get { return _telefone1; } set { _telefone1 = value; } }
        public string Telefone2 { get { return _telefone2; } set { _telefone2 = value; } }
        public string Telefone3 { get { return _telefone3; } set { _telefone3 = value; } }
        public DateTime? Nascimento { get { return _nascimento; } set { _nascimento = value; } }

        private int offlineIdComand;
        private int _id;
        private string _nome;
        private string _cpf;
        private string _telefone1;
        private string _telefone2;
        private string _telefone3;
        private DateTime? _nascimento;

        public OfflineResponsavel()
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = new int();
            _nome = "";
            _cpf = "";
            _telefone1 = "";
            _telefone2 = "";
            _telefone3 = "";
            _nascimento = new DateTime();
        }

        public OfflineResponsavel(MainDataSet.ResponsavelRow responsavel, ComandType comando)
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = responsavel.ID;
            _nome = responsavel.Nome;
            _cpf = responsavel.CPF;
            _telefone1 = responsavel.Telefone1;
            _telefone2 = responsavel.Telefone2;
            _telefone3 = responsavel.Telefone3;
            _nascimento = responsavel.Nascimento;
            Comando = comando;
        }

        public OfflineResponsavel(int id, string nome, string cpf, string telefone1, string telefone2, string telefone3, DateTime? nascimento, ComandType comando)
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = id;
            _nome = nome;
            _cpf = cpf;
            _telefone1 = telefone1;
            _telefone2 = telefone2;
            _telefone3 = telefone3;
            _nascimento = nascimento;
            Comando = comando;
        }
    }

    public class OfflinePagamento : OfflineModule
    {
        public int OfflineIDComand { get { return offlineIdComand; } set { offlineIdComand = value; } }
        public int ID { get { return _id; } set { _id = value; } }
        public Guid Aluno { get { return _aluno; } set { _aluno = value; } }
        public DateTime Referencia { get { return _referencia; } set { _referencia = value; } }
        public DateTime? DataPagamento { get { return _dataPagamento; } set { _dataPagamento = value; } }
        public decimal Valor { get { return _valor; } set { _valor = value; } }
        public bool Rematricula { get { return _rematricula; } set { _rematricula = value; } }

        private int offlineIdComand;
        private int _id;
        private Guid _aluno;
        private DateTime _referencia;
        private DateTime? _dataPagamento;
        private decimal _valor;
        private bool _rematricula;

        public OfflinePagamento()
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = new int();
            _aluno = new Guid();
            _referencia = new DateTime();
            _dataPagamento = new DateTime();
            _valor = new decimal();
            _rematricula = new bool();
        }

        public OfflinePagamento(MainDataSet.PagamentoRow pagamento, ComandType comando)
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = pagamento.ID;
            _aluno = pagamento.Aluno;
            _referencia = pagamento.Referencia;
            _dataPagamento = pagamento.DataPagamento;
            _valor = pagamento.Valor;
            _rematricula = pagamento.Rematricula;
            Comando = comando;
        }

        public OfflinePagamento(int id, Guid aluno, DateTime referencia, DateTime dataPagamento, decimal valor, bool rematricula, ComandType comando)
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = id;
            _aluno = aluno;
            _referencia = referencia;
            _dataPagamento = dataPagamento;
            _valor = valor;
            _rematricula = rematricula;
            Comando = comando;
        }
    }

    public class OfflinePontuacao : OfflineModule
    {
        public int OfflineIDComand { get { return offlineIdComand; } set { offlineIdComand = value; } }
        public int ID { get { return _id; } set { _id = value; } }
        public Guid Aluno { get { return _aluno; } set { _aluno = value; } }
        public string Modulo { get { return _modulo; } set { _modulo = value; } }
        public string Licao { get { return _licao; } set { _licao = value; } }
        public DateTime? Data { get { return _data; } set { _data = value; } }
        public decimal Pontuacao { get { return _pontuacao; } set { _pontuacao = value; } }

        private int offlineIdComand;
        private int _id;
        private Guid _aluno;
        private string _modulo;
        private string _licao;
        private DateTime? _data;
        private decimal _pontuacao;

        public OfflinePontuacao()
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = new int();
            _aluno = new Guid();
            _modulo = "";
            _licao = "";
            _data = new DateTime();
            _pontuacao = new decimal();
        }

        public OfflinePontuacao(MainDataSet.PontuacaoRow pontuacao, ComandType comando)
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = pontuacao.ID;
            _aluno = pontuacao.Aluno;
            _modulo = pontuacao.Modulo;
            _licao = pontuacao.Licao;
            _data = pontuacao.Data;
            _pontuacao = pontuacao.Resultado;
            Comando = comando;
        }

        public OfflinePontuacao(int id, Guid aluno, string modulo, string licao, DateTime data, decimal pontuacao, ComandType comando)
        {
            offlineIdComand = OfflineIDComandCallID;
            _id = id;
            _aluno = aluno;
            _modulo = modulo;
            _licao = licao;
            _data = data;
            _pontuacao = pontuacao;
            Comando = comando;
        }
    }
    #endregion

    #region RowsClasses
    public class OfflineAlunoRow
    {
        public DataColumn OfflineIDComandColumn { get { return _offlineIDCommand; } set { _offlineIDCommand = value; } }
        public DataColumn IdColumn { get { return _id; } set { _id = value; } }
        public DataColumn NomeColumn { get { return _nome; } set { _nome = value; } }
        public DataColumn NascimentoColumn { get { return _nascimento; } set { _nascimento = value; } }
        public DataColumn Telefone1Column { get { return _telefone1; } set { _telefone1 = value; } }
        public DataColumn Telefone2Column { get { return _telefone2; } set { _telefone2 = value; } }
        public DataColumn Telefone3Column { get { return _telefone3; } set { _telefone3 = value; } }
        public DataColumn ResponsavelColumn { get { return _responsavel; } set { _responsavel = value; } }
        public DataColumn MatriculaColumn { get { return _matricula; } set { _matricula = value; } }
        public DataColumn ComandoColumn { get { return _comando; } set { _comando = value; } }

        private DataColumn _offlineIDCommand;
        private DataColumn _id;
        private DataColumn _nome;
        private DataColumn _nascimento;
        private DataColumn _telefone1;
        private DataColumn _telefone2;
        private DataColumn _telefone3;
        private DataColumn _responsavel;
        private DataColumn _matricula;
        private DataColumn _comando;

        public OfflineAlunoRow()
        {
            _offlineIDCommand = new DataColumn("Offline ID Comand", typeof(int));
            _id = new DataColumn("ID", typeof(Guid));
            _nome = new DataColumn("Nome", typeof(string));
            _nascimento = new DataColumn("Nascimento", typeof(DateTime));
            _telefone1 = new DataColumn("Telefone1", typeof(string));
            _telefone2 = new DataColumn("Telefone2", typeof(string));
            _telefone3 = new DataColumn("Telefone3", typeof(string));
            _responsavel = new DataColumn("Responsável", typeof(int));
            _matricula = new DataColumn("Matrícula", typeof(DateTime));
            _comando = new DataColumn("Comando", typeof(string));
        }

    }

    public class OfflineResponsavelRow
    {
        public DataColumn OfflineIDComandColumn { get { return offlineIdComand; } set { offlineIdComand = value; } }
        public DataColumn IDColumn { get { return _id; } set { _id = value; } }
        public DataColumn NomeColumn { get { return _nome; } set { _nome = value; } }
        public DataColumn CPFColumn { get { return _cpf; } set { _cpf = value; } }
        public DataColumn Telefone1Column { get { return _telefone1; } set { _telefone1 = value; } }
        public DataColumn Telefone2Column { get { return _telefone2; } set { _telefone2 = value; } }
        public DataColumn Telefone3Column { get { return _telefone3; } set { _telefone3 = value; } }
        public DataColumn NascimentoColumn { get { return _nascimento; } set { _nascimento = value; } }
        public DataColumn ComandoColumn { get { return _comando; } set { _comando = value; } }

        private DataColumn offlineIdComand;
        private DataColumn _id;
        private DataColumn _nome;
        private DataColumn _cpf;
        private DataColumn _telefone1;
        private DataColumn _telefone2;
        private DataColumn _telefone3;
        private DataColumn _nascimento;
        private DataColumn _comando;

        public OfflineResponsavelRow()
        {
            offlineIdComand = new DataColumn("Offline ID Command", typeof(int));
            _id = new DataColumn("ID", typeof(int));
            _nome = new DataColumn("Nome", typeof(string));
            _cpf = new DataColumn("CPF", typeof(string));
            _telefone1 = new DataColumn("Telefone1", typeof(string));
            _telefone2 = new DataColumn("Telefone2", typeof(string));
            _telefone3 = new DataColumn("Telefone3", typeof(string));
            _nascimento = new DataColumn("Nascimento", typeof(DateTime));
            _comando = new DataColumn("Comando", typeof(ComandType));
        }
    }

    public class OfflinePagamentoRow
    {
        public DataColumn OfflineIDComandColumn { get { return offlineIdComand; } }
        public DataColumn IDColumn { get { return _id; } set { _id = value; } }
        public DataColumn AlunoColumn { get { return _aluno; } set { _aluno = value; } }
        public DataColumn ReferenciaColumn { get { return _referencia; } set { _referencia = value; } }
        public DataColumn DataPagamentoColumn { get { return _dataPagamento; } set { _dataPagamento = value; } }
        public DataColumn ValorColumn { get { return _valor; } set { _valor = value; } }
        public DataColumn RematriculaColumn { get { return _rematricula; } set { _rematricula = value; } }
        public DataColumn ComandoColumn { get { return _comando; } set { _comando = value; } }

        private DataColumn offlineIdComand;
        private DataColumn _id;
        private DataColumn _aluno;
        private DataColumn _referencia;
        private DataColumn _dataPagamento;
        private DataColumn _valor;
        private DataColumn _rematricula;
        private DataColumn _comando;

        public OfflinePagamentoRow()
        {
            offlineIdComand = new DataColumn("Offline ID Command", typeof(int));
            _id = new DataColumn("ID", typeof(int));
            _aluno = new DataColumn("Aluno", typeof(Guid));
            _referencia = new DataColumn("Referencia", typeof(DateTime));
            _dataPagamento = new DataColumn("DataPagamento", typeof(DateTime));
            _valor = new DataColumn("Valor", typeof(decimal));
            _rematricula = new DataColumn("Rematrícula", typeof(bool));
            _comando = new DataColumn("Comando", typeof(ComandType));
        }
    }

    public class OfflinePontuacaoRow
    {
        public DataColumn OfflineIDComandColumn { get { return offlineIdComand; } }
        public DataColumn IDColumn { get { return _id; } set { _id = value; } }
        public DataColumn AlunoColumn { get { return _aluno; } set { _aluno = value; } }
        public DataColumn ModuloColumn { get { return _modulo; } set { _modulo = value; } }
        public DataColumn LicaoColumn { get { return _licao; } set { _licao = value; } }
        public DataColumn DataColumn { get { return _data; } set { _data = value; } }
        public DataColumn PontuacaoColumn { get { return _pontuacao; } set { _pontuacao = value; } }
        public DataColumn ComandoColumn { get { return _comando; } set { _comando = value; } }

        private DataColumn offlineIdComand;
        private DataColumn _id;
        private DataColumn _aluno;
        private DataColumn _modulo;
        private DataColumn _licao;
        private DataColumn _data;
        private DataColumn _pontuacao;
        private DataColumn _comando;

        public OfflinePontuacaoRow()
        {
            offlineIdComand = new DataColumn("Offline ID Command", typeof(int));
            _id = new DataColumn("ID", typeof(int));
            _aluno = new DataColumn("Aluno", typeof(Guid));
            _modulo = new DataColumn("Modulo", typeof(string));
            _licao = new DataColumn("Lição", typeof(string));
            _data = new DataColumn("Data", typeof(DateTime));
            _pontuacao = new DataColumn("Pontuação", typeof(decimal));
            _comando = new DataColumn("Comando", typeof(ComandType));
        }
    }
    #endregion

    #region DataTables
    public class OfflineAlunoDataTable : DataTable
    {
        private OfflineAlunoRow templateRow = new OfflineAlunoRow();

        public OfflineAlunoDataTable()
        {
            this.TableName = "Offline Aluno DataTable";

            DataColumn[] pk = new DataColumn[1] { templateRow.OfflineIDComandColumn };

            Columns.Add(templateRow.OfflineIDComandColumn);
            Columns.Add(templateRow.IdColumn);
            Columns.Add(templateRow.NomeColumn);
            Columns.Add(templateRow.NascimentoColumn);
            Columns.Add(templateRow.Telefone1Column);
            Columns.Add(templateRow.Telefone2Column);
            Columns.Add(templateRow.Telefone3Column);
            Columns.Add(templateRow.ResponsavelColumn);
            Columns.Add(templateRow.MatriculaColumn);
            Columns.Add(templateRow.ComandoColumn);

            PrimaryKey = pk;
        }

        public void Add(OfflineAluno aluno)
        {
            object[] valores = new object[10];

            valores[0] = aluno.OfflineIDComand;
            valores[1] = aluno.ID;
            valores[2] = aluno.Nome;
            valores[3] = aluno.Nascimento;
            valores[4] = aluno.Telefone1;
            valores[5] = aluno.Telefone2;
            valores[6] = aluno.Telefone3;
            valores[7] = aluno.Responsavel;
            valores[8] = aluno.Matricula;
            valores[9] = aluno.Comando;

            Rows.Add(valores);
        }

        public OfflineAluno[] GetSavedData()
        {
            object[] valores;
            List<OfflineAluno> aluno = new List<OfflineAluno>();
            for (int i = 0; i < Rows.Count; i++)
            {
                OfflineAluno tmpAluno = new OfflineAluno();
                valores = Rows[i].ItemArray;

                tmpAluno.OfflineIDComand = (int)valores[0];
                tmpAluno.ID = (Guid)valores[1];
                tmpAluno.Nome = (string)valores[2];
                tmpAluno.Nascimento = (DateTime)valores[3];
                tmpAluno.Telefone1 = (string)valores[4];
                tmpAluno.Telefone2 = (string)valores[5];
                tmpAluno.Telefone3 = (string)valores[6];
                tmpAluno.Responsavel = (int)valores[7];
                tmpAluno.Matricula = (DateTime)valores[8];
                tmpAluno.Comando = OfflineDataSet.ParseComand((string)valores[9]);

                aluno.Add(tmpAluno);
            }

            return aluno.ToArray();
        }
    }

    public class OfflineResponsavelDataTable : DataTable
    {
        private OfflineResponsavelRow templateRow = new OfflineResponsavelRow();

        public OfflineResponsavelDataTable()
        {
            this.TableName = "Offline Responsavel DataTable";

            DataColumn[] pk = new DataColumn[1] { templateRow.OfflineIDComandColumn };

            Columns.Add(templateRow.OfflineIDComandColumn);
            Columns.Add(templateRow.IDColumn);
            Columns.Add(templateRow.NomeColumn);
            Columns.Add(templateRow.CPFColumn);
            Columns.Add(templateRow.Telefone1Column);
            Columns.Add(templateRow.Telefone2Column);
            Columns.Add(templateRow.Telefone3Column);
            Columns.Add(templateRow.NascimentoColumn);
            Columns.Add(templateRow.ComandoColumn);

            PrimaryKey = pk;
        }

        public void Add(OfflineResponsavel responsavel)
        {
            object[] valores = new object[9];
            valores[0] = responsavel.OfflineIDComand;
            valores[1] = responsavel.ID;
            valores[2] = responsavel.Nome;
            valores[3] = responsavel.CPF;
            valores[4] = responsavel.Telefone1;
            valores[5] = responsavel.Telefone2;
            valores[6] = responsavel.Telefone3;
            valores[7] = responsavel.Nascimento;
            valores[8] = responsavel.Comando;

            Rows.Add(valores);
        }

        public OfflineResponsavel[] GetSavedData()
        {
            object[] valores;
            List<OfflineResponsavel> responsavel = new List<OfflineResponsavel>();

            for (int i = 0; i < Rows.Count; i++)
            {
                valores = Rows[i].ItemArray;
                OfflineResponsavel tmpResponsavel = new OfflineResponsavel();

                tmpResponsavel.OfflineIDComand = (int)valores[0];
                tmpResponsavel.ID = (int)valores[1];
                tmpResponsavel.Nome = (string)valores[2];
                tmpResponsavel.CPF = (string)valores[3];
                tmpResponsavel.Telefone1 = (string)valores[4];
                tmpResponsavel.Telefone2 = (string)valores[5];
                tmpResponsavel.Telefone3 = (string)valores[6];
                tmpResponsavel.Nascimento = (DateTime)valores[7];
                tmpResponsavel.Comando = OfflineDataSet.ParseComand((string)valores[8]);

                responsavel.Add(tmpResponsavel);
            }

            return responsavel.ToArray();
        }
    }

    public class OfflinePagamentoDataTable : DataTable
    {
        private OfflinePagamentoRow templateRow = new OfflinePagamentoRow();

        public OfflinePagamentoDataTable()
        {
            this.TableName = "Offline Pagamento DataTable";

            DataColumn[] pk = new DataColumn[1] { templateRow.OfflineIDComandColumn };

            Columns.Add(templateRow.OfflineIDComandColumn);
            Columns.Add(templateRow.IDColumn);
            Columns.Add(templateRow.AlunoColumn);
            Columns.Add(templateRow.ReferenciaColumn);
            Columns.Add(templateRow.DataPagamentoColumn);
            Columns.Add(templateRow.ValorColumn);
            Columns.Add(templateRow.RematriculaColumn);
            Columns.Add(templateRow.ComandoColumn);

            PrimaryKey = pk;
        }

        public void Add(OfflinePagamento pagamento)
        {
            object[] valores = new object[8];

            valores[0] = pagamento.OfflineIDComand;
            valores[1] = pagamento.ID;
            valores[2] = pagamento.Aluno;
            valores[3] = pagamento.Referencia;
            valores[4] = pagamento.DataPagamento;
            valores[5] = pagamento.Valor;
            valores[6] = pagamento.Rematricula;
            valores[7] = pagamento.Comando;

            Rows.Add(valores);
        }

        public OfflinePagamento[] GetSavedData()
        {
            object[] valores;
            List<OfflinePagamento> pagamento = new List<OfflinePagamento>();

            for(int i=0; i<Rows.Count; i++)
            {
                OfflinePagamento tmpPagamento = new OfflinePagamento();
                valores = Rows[i].ItemArray;

                tmpPagamento.OfflineIDComand = (int)valores[0];
                tmpPagamento.ID = (int)valores[1];
                tmpPagamento.Aluno = (Guid)valores[2];
                tmpPagamento.Referencia = (DateTime)valores[3];
                tmpPagamento.DataPagamento = (DateTime)valores[4];
                tmpPagamento.Valor = (decimal)valores[5];
                tmpPagamento.Rematricula = (bool)valores[6];
                tmpPagamento.Comando = OfflineDataSet.ParseComand((string)valores[7]);

                pagamento.Add(tmpPagamento);
            }

            return pagamento.ToArray();
        }
    }

    public class OfflinePontuacaoDataTable : DataTable
    {
        private OfflinePontuacaoRow templateRow = new OfflinePontuacaoRow();

        public OfflinePontuacaoDataTable()
        {
            this.TableName = "Offline Pontuação DataTable";

            DataColumn[] pk = new DataColumn[1] { templateRow.OfflineIDComandColumn };

            Columns.Add(templateRow.OfflineIDComandColumn);
            Columns.Add(templateRow.IDColumn);
            Columns.Add(templateRow.AlunoColumn);
            Columns.Add(templateRow.ModuloColumn);
            Columns.Add(templateRow.LicaoColumn);
            Columns.Add(templateRow.DataColumn);
            Columns.Add(templateRow.PontuacaoColumn);
            Columns.Add(templateRow.ComandoColumn);

            PrimaryKey = pk;
        }

        public void Add(OfflinePontuacao pontuacao)
        {
            object[] valores = new object[8];

            valores[0] = pontuacao.OfflineIDComand;
            valores[1] = pontuacao.ID;
            valores[2] = pontuacao.Aluno;
            valores[3] = pontuacao.Modulo;
            valores[4] = pontuacao.Licao;
            valores[5] = pontuacao.Data;
            valores[6] = pontuacao.Pontuacao;
            valores[7] = pontuacao.Comando;

            Rows.Add(valores);
        }

        public OfflinePontuacao[] GetSavedData()
        {
            object[] valores;
            List<OfflinePontuacao> pontuacao = new List<OfflinePontuacao>();

            for(int i=0; i<Rows.Count; i++)
            {
                OfflinePontuacao tmpPontuacao = new OfflinePontuacao();
                valores = Rows[i].ItemArray;

                tmpPontuacao.OfflineIDComand = (int)valores[0];
                tmpPontuacao.ID = (int)valores[1];
                tmpPontuacao.Aluno = (Guid)valores[2];
                tmpPontuacao.Modulo = (string)valores[3];
                tmpPontuacao.Licao = (string)valores[4];
                tmpPontuacao.Data = (DateTime)valores[5];
                tmpPontuacao.Pontuacao = (decimal)valores[6];
                tmpPontuacao.Comando = OfflineDataSet.ParseComand((string)valores[7]);

                pontuacao.Add(tmpPontuacao);
            }

            return pontuacao.ToArray();
        }
    }
    #endregion
}