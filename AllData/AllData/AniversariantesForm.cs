﻿using System;
using System.Windows.Forms;

namespace AllData
{
    public partial class AniversariantesForm : Form
    {
        public enum AnniversaryFilter { Month = 0, Week, Day};
        AnniversaryFilter ann = 0;
        public DialogResult resultado;

        public AniversariantesForm()
        {
            InputForm fm = new InputForm();
            if (fm.ShowDialog() == DialogResult.OK)
            {
                resultado = DialogResult.OK;
                InitializeComponent();
            }
            else { resultado = DialogResult.No; Close(); }
        }

        /// <summary>
        /// Inicializa a form a partir de um filtro de aniversariantes
        /// </summary>
        /// <param name="aniversario">Filtro a ser usado</param>
        public AniversariantesForm(AnniversaryFilter aniversario)
        {
            InputForm fm = new InputForm();
            if (fm.ShowDialog() == DialogResult.OK)
            {
                InitializeComponent();

                ann = aniversario;
                comboBox1.SelectedIndex = 0;
                RefreshList();
            }
            else { Close(); }
            
        }

        #region Controles
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshList();
        }

        private void ThisMonthRB_CheckedChanged(object sender, EventArgs e)
        {
            if (ThisMonthRB.Checked)
            {
                ann = AnniversaryFilter.Month;
                ThisWeekRB.Checked = false;
                TodayRB.Checked = false;

                RefreshList();
            }
        }

        private void ThisWeekRB_CheckedChanged(object sender, EventArgs e)
        {
            if (ThisWeekRB.Checked)
            {
                ann = AnniversaryFilter.Week;
                ThisMonthRB.Checked = false;
                TodayRB.Checked = false;

                RefreshList();
            }
        }

        private void TodayRB_CheckedChanged(object sender, EventArgs e)
        {
            if(TodayRB.Checked)
            {
                ann = AnniversaryFilter.Day;
                ThisMonthRB.Checked = false;
                ThisWeekRB.Checked = false;

                RefreshList();
            }
        }
        #endregion

        /// <summary>
        /// Atualiza a lista de aniversariantes
        /// </summary>
        void RefreshList()
        {
            //Seleciona o tipo de filtro: 
            // 0 = Aluno
            // 1 = Responsável
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    switch (ann)
                    {
                        case AnniversaryFilter.Month:
                            dataGridView1.DataSource = DataHelper.GetAlunoMonthBirthdates();
                            break;

                        case AnniversaryFilter.Week:
                            dataGridView1.DataSource = DataHelper.GetAlunoWeekBirthdates();
                            break;

                        case AnniversaryFilter.Day:
                            dataGridView1.DataSource = DataHelper.GetAlunoDayBirthdates();
                            break;
                    }
                    break;

                case 1:
                    switch (ann)
                    {
                        case AnniversaryFilter.Month:
                            dataGridView1.DataSource = DataHelper.GetResponsavelMonthBirthdates();
                            break;

                        case AnniversaryFilter.Week:
                            dataGridView1.DataSource = DataHelper.GetResponsavelWeekBirthdates();
                            break;

                        case AnniversaryFilter.Day:
                            dataGridView1.DataSource = DataHelper.GetResponsavelDayBirthdates();
                            break;
                    }
                    break;

                    //Padrão: Aluno
                default:
                    switch (ann)
                    {
                        case AnniversaryFilter.Month:
                            dataGridView1.DataSource = DataHelper.GetAlunoMonthBirthdates();
                            break;

                        case AnniversaryFilter.Week:
                            dataGridView1.DataSource = DataHelper.GetAlunoWeekBirthdates();
                            break;

                        case AnniversaryFilter.Day:
                            dataGridView1.DataSource = DataHelper.GetAlunoDayBirthdates();
                            break;
                    }
                    break;
            }
        }
    }
}
