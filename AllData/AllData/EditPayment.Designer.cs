﻿namespace AllData
{
    partial class EditPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditPayment));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RemCheck1 = new System.Windows.Forms.CheckBox();
            this.fKPagamentoAlunoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.alunoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSet = new AllData.MainDataSet();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.EditBtn = new System.Windows.Forms.Button();
            this.RemoveBtn = new System.Windows.Forms.Button();
            this.ReferenceComb1 = new System.Windows.Forms.ComboBox();
            this.NomeComb1 = new System.Windows.Forms.ComboBox();
            this.alunoTableAdapter = new AllData.MainDataSetTableAdapters.AlunoTableAdapter();
            this.pagamentoTableAdapter = new AllData.MainDataSetTableAdapters.PagamentoTableAdapter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RemCheck2 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ValorBox = new System.Windows.Forms.TextBox();
            this.NomeComb2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ReferenceDate = new System.Windows.Forms.DateTimePicker();
            this.PayDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.CommitBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fKPagamentoAlunoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RemCheck1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.EditBtn);
            this.groupBox1.Controls.Add(this.RemoveBtn);
            this.groupBox1.Controls.Add(this.ReferenceComb1);
            this.groupBox1.Controls.Add(this.NomeComb1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(710, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Aluno";
            // 
            // RemCheck1
            // 
            this.RemCheck1.AutoCheck = false;
            this.RemCheck1.AutoSize = true;
            this.RemCheck1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.fKPagamentoAlunoBindingSource, "Rematricula", true));
            this.RemCheck1.Location = new System.Drawing.Point(245, 49);
            this.RemCheck1.Name = "RemCheck1";
            this.RemCheck1.Size = new System.Drawing.Size(84, 17);
            this.RemCheck1.TabIndex = 6;
            this.RemCheck1.Text = "Rematrícula";
            this.RemCheck1.UseVisualStyleBackColor = true;
            // 
            // fKPagamentoAlunoBindingSource
            // 
            this.fKPagamentoAlunoBindingSource.DataMember = "FK_Pagamento_Aluno";
            this.fKPagamentoAlunoBindingSource.DataSource = this.alunoBindingSource;
            // 
            // alunoBindingSource
            // 
            this.alunoBindingSource.DataMember = "Aluno";
            this.alunoBindingSource.DataSource = this.mainDataSet;
            // 
            // mainDataSet
            // 
            this.mainDataSet.DataSetName = "MainDataSet";
            this.mainDataSet.EnforceConstraints = false;
            this.mainDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Referência";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Aluno";
            // 
            // EditBtn
            // 
            this.EditBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.EditBtn.Location = new System.Drawing.Point(306, 70);
            this.EditBtn.Name = "EditBtn";
            this.EditBtn.Size = new System.Drawing.Size(75, 23);
            this.EditBtn.TabIndex = 2;
            this.EditBtn.Text = "Editar";
            this.EditBtn.UseVisualStyleBackColor = true;
            this.EditBtn.Click += new System.EventHandler(this.EditBtn_Click);
            // 
            // RemoveBtn
            // 
            this.RemoveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RemoveBtn.Location = new System.Drawing.Point(225, 70);
            this.RemoveBtn.Name = "RemoveBtn";
            this.RemoveBtn.Size = new System.Drawing.Size(75, 23);
            this.RemoveBtn.TabIndex = 2;
            this.RemoveBtn.Text = "Remover";
            this.RemoveBtn.UseVisualStyleBackColor = true;
            this.RemoveBtn.Click += new System.EventHandler(this.RemoveBtn_Click);
            // 
            // ReferenceComb1
            // 
            this.ReferenceComb1.DataSource = this.fKPagamentoAlunoBindingSource;
            this.ReferenceComb1.DisplayMember = "Referencia";
            this.ReferenceComb1.FormattingEnabled = true;
            this.ReferenceComb1.Location = new System.Drawing.Point(98, 46);
            this.ReferenceComb1.Name = "ReferenceComb1";
            this.ReferenceComb1.Size = new System.Drawing.Size(121, 21);
            this.ReferenceComb1.TabIndex = 1;
            this.ReferenceComb1.ValueMember = "ID";
            // 
            // NomeComb1
            // 
            this.NomeComb1.DataSource = this.alunoBindingSource;
            this.NomeComb1.DisplayMember = "Nome";
            this.NomeComb1.FormattingEnabled = true;
            this.NomeComb1.Location = new System.Drawing.Point(98, 19);
            this.NomeComb1.Name = "NomeComb1";
            this.NomeComb1.Size = new System.Drawing.Size(121, 21);
            this.NomeComb1.TabIndex = 0;
            this.NomeComb1.ValueMember = "ID";
            // 
            // alunoTableAdapter
            // 
            this.alunoTableAdapter.ClearBeforeFill = true;
            // 
            // pagamentoTableAdapter
            // 
            this.pagamentoTableAdapter.ClearBeforeFill = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(0, 101);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(710, 150);
            this.panel1.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RemCheck2);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.ValorBox);
            this.groupBox3.Controls.Add(this.NomeComb2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(367, 150);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informações";
            // 
            // RemCheck2
            // 
            this.RemCheck2.AutoSize = true;
            this.RemCheck2.Location = new System.Drawing.Point(234, 57);
            this.RemCheck2.Name = "RemCheck2";
            this.RemCheck2.Size = new System.Drawing.Size(84, 17);
            this.RemCheck2.TabIndex = 5;
            this.RemCheck2.Text = "Rematrícula";
            this.RemCheck2.UseVisualStyleBackColor = true;
            this.RemCheck2.CheckedChanged += new System.EventHandler(this.RemCheck2_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Valor";
            // 
            // ValorBox
            // 
            this.ValorBox.Location = new System.Drawing.Point(98, 55);
            this.ValorBox.Name = "ValorBox";
            this.ValorBox.Size = new System.Drawing.Size(100, 20);
            this.ValorBox.TabIndex = 4;
            // 
            // NomeComb2
            // 
            this.NomeComb2.Cursor = System.Windows.Forms.Cursors.No;
            this.NomeComb2.DataSource = this.alunoBindingSource;
            this.NomeComb2.DisplayMember = "Nome";
            this.NomeComb2.Enabled = false;
            this.NomeComb2.FormattingEnabled = true;
            this.NomeComb2.Location = new System.Drawing.Point(98, 28);
            this.NomeComb2.Name = "NomeComb2";
            this.NomeComb2.Size = new System.Drawing.Size(121, 21);
            this.NomeComb2.TabIndex = 3;
            this.NomeComb2.ValueMember = "ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Aluno";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ReferenceDate);
            this.groupBox2.Controls.Add(this.PayDate);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox2.Location = new System.Drawing.Point(367, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(343, 150);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Referência";
            // 
            // ReferenceDate
            // 
            this.ReferenceDate.CustomFormat = "yyyy";
            this.ReferenceDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ReferenceDate.Location = new System.Drawing.Point(122, 48);
            this.ReferenceDate.Name = "ReferenceDate";
            this.ReferenceDate.Size = new System.Drawing.Size(200, 20);
            this.ReferenceDate.TabIndex = 7;
            // 
            // PayDate
            // 
            this.PayDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.PayDate.Location = new System.Drawing.Point(122, 24);
            this.PayDate.Name = "PayDate";
            this.PayDate.Size = new System.Drawing.Size(200, 20);
            this.PayDate.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Referência";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Data de Pagamento";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 229);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(710, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(39, 17);
            this.StatusLabel.Text = "Status";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.CancelBtn);
            this.panel2.Controls.Add(this.CommitBtn);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 182);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(710, 47);
            this.panel2.TabIndex = 3;
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Location = new System.Drawing.Point(542, 13);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 23);
            this.CancelBtn.TabIndex = 1;
            this.CancelBtn.Text = "Cancelar";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // CommitBtn
            // 
            this.CommitBtn.Location = new System.Drawing.Point(623, 13);
            this.CommitBtn.Name = "CommitBtn";
            this.CommitBtn.Size = new System.Drawing.Size(75, 23);
            this.CommitBtn.TabIndex = 8;
            this.CommitBtn.Text = "Confirmar";
            this.CommitBtn.UseVisualStyleBackColor = true;
            this.CommitBtn.Click += new System.EventHandler(this.CommitBtn_Click);
            // 
            // EditPayment
            // 
            this.AcceptButton = this.CommitBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(710, 251);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditPayment";
            this.Text = "Editar Informações";
            this.Load += new System.EventHandler(this.EditPayment_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fKPagamentoAlunoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ReferenceComb1;
        private System.Windows.Forms.ComboBox NomeComb1;
        private MainDataSet mainDataSet;
        private System.Windows.Forms.BindingSource alunoBindingSource;
        private MainDataSetTableAdapters.AlunoTableAdapter alunoTableAdapter;
        private System.Windows.Forms.BindingSource fKPagamentoAlunoBindingSource;
        private MainDataSetTableAdapters.PagamentoTableAdapter pagamentoTableAdapter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button EditBtn;
        private System.Windows.Forms.Button RemoveBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ValorBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker ReferenceDate;
        private System.Windows.Forms.DateTimePicker PayDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button CommitBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox NomeComb2;
        private System.Windows.Forms.CheckBox RemCheck1;
        private System.Windows.Forms.CheckBox RemCheck2;
    }
}