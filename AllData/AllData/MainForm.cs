﻿using System;
using System.Drawing;
using System.Threading;
using Microsoft.Win32;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using AllData.Properties;
using System.Configuration;
using AllData.Security;

namespace AllData
{
    public partial class MainForm : Form
    {
        static public Thread backWork;
        AniversariantesForm.AnniversaryFilter ann;
        enum InfoClick { Aniversariantes = 0, Duplicatas};
        LivroCaixa LC = new LivroCaixa();

        InfoClick janela = InfoClick.Aniversariantes;
        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        /// <summary>
        /// Intervalo do timer (em minutos)
        /// </summary>
        int IntervalTime
        {
            get
            {
                Settings s = new Settings();
                return s.SaveCaixa;
            }

            set
            {
                Settings s = new Settings();
                s.SaveCaixa = value;
                s.Save();
            }
        }

        public MainForm()
        {
            InitializeComponent();
        }

        #region Funções Form

        #region ToolStrip

        #region Aluno

        //Novo Aluno    ### Aluno/Novo Aluno
        private void novoAlunoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Student studentWindow = new Student();
            studentWindow.Show();
        }

        //Editar Informações ### Aluno/Editar Informações
        private void editarInformaçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditStudent edit = new EditStudent();
            edit.Show();
        }

        //Novo Pagamento     ### Aluno/Pagamento/Novo Pagamento
        private void pagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Payment pm = new Payment();
            pm.Show();
        }

        //Editar Pagamento  ### Aluno/Pagamento/Editar Pagamento
        private void EditarPagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditPayment edit = new EditPayment();
            edit.Show();
        }

        //Nova Pontuação     ### Aluno/Pontuação/Nova Pontuação
        private void pontuaçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Points p = new Points();
            p.Show();
        }

        //Editar Pontuação  ### Aluno/Pontuação/Editar Pontuação
        private void editarPontuaçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditPoints edit = new EditPoints();
            edit.Show();
        }

        //Ficha do Estudante    ### Aluno/Ficha do Estudante
        private void fichaDoEstudanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StudentChart sc = new StudentChart();
            sc.Show();
        }

        #endregion Aluno

        #region Responsável

        //Novo Responsável  ### Responsável/Novo Responsável
        private void novoResponsávelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Responsavel r = new Responsavel();
            r.Show();
        }

        //Editar Informações    ### Responsável/Editar Informações
        private void editarInformaçãoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            EditResponsavel edit = new EditResponsavel();
            edit.Show();
        }

        //Ficha do Responsável  ### Responsável/ Ficha do Responsável
        private void fichaDoResponsávelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResponsableChart resp = new ResponsableChart();
            resp.Show();
        }

        #endregion Responsável

        #region Ferramentas

        //Lista de Aniversariantes  ### Ferramentas/Lista de Aniversariantes
        private void listaDeAniversariantesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    AniversariantesForm af = new AniversariantesForm();
                    if (af.resultado == DialogResult.OK)
                    { af.Show(); }

                    break;

                case false:
                    MessageBox.Show("Indisponível no modo offline!");
                    break;
            }
        }

        //Dados Duplicados  ### Ferramentas/Dados Duplicados
        private void dadosDuplicadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    DuplicatasForm df = new DuplicatasForm();
                    if (df.resultado == DialogResult.OK)
                    { df.Show(); }
                    break;

                case false:
                    MessageBox.Show("Indisponível no modo offline!");
                    break;
            }
        }

        #endregion Ferramentas

        #region Opções

        //Alterar Senha    ### Opções/Alterar Senha
        private void alterarSenhaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePasswordForm cp = new ChangePasswordForm();
            cp.Show();
        }

        private void autosaveDoCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings s = new Settings();
            s.Reload();
        }

        #endregion Opções

        #region Ajuda

        //Sobre     ### Ajuda/Sobre
        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 ab = new AboutBox1();
            ab.ShowDialog();
        }

        //Suporte   ### Ajuda/Suporte
        private void informaçõesDoProgramadorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Support sup = new Support();
            sup.Show();
        }

        #endregion Ajuda


        private void inserirDadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        #endregion ToolStrip

        #region Caixa

        //Abrir Caixa
        private void AbrirCaixaBtn_Click(object sender, EventArgs e)
        {
            LC = new LivroCaixa();
            LC.MdiParent = this;
            if (LC.EditarCaixaHoje())
            {
                pictureBox1.Controls.Add(LC);
                LC.Show();
                ShowCaixaOpcoes();
                Saver.Interval = (IntervalTime * 60000);
                Saver.Start();
            }

            else
            {
                MessageBox.Show("Os dados no caixa ja estão sincronizados ou parcialmente sincronizados com o servidor e, portanto, não é possível editá-los!\n" +
                    "Você ainda pode ver o caixa no histórico de caixa.");
            }
        }

        #region Opções do Caixa

        //Ver ID
        private void WatchID_CheckedChanged(object sender, EventArgs e)
        {
            LC.ChangeID(WatchID.Checked);
        }

        //Ver valor no Caixa
        private void WatchCaixaValue_CheckedChanged(object sender, EventArgs e)
        {
            LC.ShowCaixaValor(WatchCaixaValue.Checked);
        }

        #endregion Opções do Caixa

        //Fechar Caixa
        private void PauseCaixa_Click(object sender, EventArgs e)
        {
            Saver.Stop();
            LC.PausarCaixa();
            LC.Close();
            HideCaixaOpcoes();
        }

        //Sincronizar e Fechar
        private void FecharCaixaBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Internal.SincronizarDados, "Sincronizar Dados", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Saver.Stop();
                LC.FecharCaixa();
                LC.Close();
                HideCaixaOpcoes();
            }
        }

        #endregion Caixa

        #region Outros Eventos

        private void MainForm_Load(object sender, EventArgs e)
        {
            Enabled = false;
            LogonForm lf = new LogonForm();
            if (lf.ShowDialog() == DialogResult.OK)
            {
                Enabled = true;
                if (ConnectionHelper.Online)
                {
                    backWork = new Thread(new ThreadStart(CheckCaixa));
                    backWork.IsBackground = true;
                    backWork.Priority = ThreadPriority.Lowest;
                    backWork.Start();
                    StatusLabel2.Text = "ALL DATA - VERSÃO " + AssemblyVersion;
                }
                else
                {
                    statusStrip1.BackColor = Color.Gray;
                    StatusLabel2.Text = string.Format("ALL DATA - VERSÃO {0} | OFFLINE", AssemblyVersion);
                }
            }
        }

        private void MainForm_Enter(object sender, EventArgs e)
        {
            if (ConnectionHelper.Online)
            {
                statusStrip1.BackColor = Color.FromArgb(128, 128, 255);
                StatusLabel2.Text = "ALL DATA - VERSÃO " + AssemblyVersion;
            }
            else
            {
                statusStrip1.BackColor = Color.Gray;
                StatusLabel2.Text = string.Format("ALL DATA - VERSÃO {0} | OFFLINE", AssemblyVersion);
            }
        }

        private void NotificationIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            switch (janela)
            {
                case InfoClick.Aniversariantes:
                    try
                    {
                        AniversariantesForm af = new AniversariantesForm();
                        af.Show();
                    }
                    catch { }
                    break;

                case InfoClick.Duplicatas:
                    try
                    {
                        DuplicatasForm df = new DuplicatasForm();
                        df.Show();
                    }
                    catch { }
                    break;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (LC.IsFormOpen)
            { LC.PausarCaixa(); }
        }

        private void Saver_Tick(object sender, EventArgs e)
        {
            if (LC.IsFormOpen)
            { LC.PausarCaixa(); }
        }

        #endregion Outros Eventos

        #endregion Funções Form

        /// <summary>
        /// Checa se há algum dado no caixa a ser sincronizado
        /// </summary>
        public void CheckCaixa()
        {
            #region Checagem do caixa do último dia
            string caminho = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            caminho += @"\AllData\Caixa";

            string[] arquivos = Directory.GetFiles(caminho, "*.adlc");
            DateTime maisRecente = DateTime.MinValue;
            string arquivoMaisRecente = "";

            //Verifica nos arquivos salvos, qual é o mais recente
            foreach (string arquivo in arquivos)
            {
                //O arquivo mais recente não pode ser o de hoje
                if(maisRecente <  Directory.GetLastWriteTime(arquivo) && Directory.GetLastWriteTime(arquivo).Date != DateTime.Today.Date)
                {
                    maisRecente = Directory.GetLastWriteTime(arquivo);
                    arquivoMaisRecente = arquivo;
                }
            }

            if (arquivoMaisRecente != "")
            {
                FileInfo f = new FileInfo(arquivoMaisRecente);
                LC.SincronizarDadosSalvos(f.Name);
            }
            #endregion Checagem do caixa do último dia

            #region Checagem nos Registros
            //Verifica nos registros se há algum arquivo a terminar a sincronização
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"Software\AllData\Caixa");
            string[] registros = key.GetValueNames();


            foreach (string valorAtual in registros)
            {
                string arquivo = (string)key.GetValue(valorAtual);

                try
                {
                    LC.SincronizarDadosSalvos(arquivo);
                    key.DeleteValue(valorAtual);
                }

                catch
                {
                    if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        InputForm IP = new InputForm();
                        if (IP.ShowDialog() == DialogResult.OK)
                        {
                            ConnectionHelper.GoOfflineMode();
                        }
                    }
                    break;
                }
            }
            #endregion Checagem nos Registros
            CheckOfflineData();
        }

        /// <summary>
        /// Verifica se há dados offline para serem salvos
        /// </summary>
        public void CheckOfflineData()
        {
            try
            {
                if (DataHelper.SaveOfflineDataIntoDataBase())
                { NotificationIcon.ShowBalloonTip(2000, "Dados inseridos no banco de dados!", "Os dados salvos na ultima sessão offline foram agora inseridos com sucesso no banco de dados!", ToolTipIcon.Info); }
            }
            catch (Exception err)
            {
                MessageBox.Show(string.Format("Erro inesperado ao inserir os dados salvos offline no banco de dados!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
            }

            Thread.Sleep(2000);
            CheckBirthdates();
        }

        /// <summary>
        /// Verifica os aniversários
        /// </summary>
        public void CheckBirthdates()
        {
            int advice = 0;

            if (DataHelper.CheckDayBirthdates())
            { advice = 3; ann = AniversariantesForm.AnniversaryFilter.Day; }
            else if (DataHelper.CheckWeekBirthdates())
            { advice = 2; ann = AniversariantesForm.AnniversaryFilter.Week; }
            else if (DataHelper.CheckMonthBirthdates())
            { advice = 1; ann = AniversariantesForm.AnniversaryFilter.Month; }

            janela = InfoClick.Aniversariantes;
            switch (advice)
            {
                case 1:
                    NotificationIcon.ShowBalloonTip(2000, "Aniversários!!!", "Há aniversariantes nesse mês! \n Clique aqui para conferir", ToolTipIcon.Info);
                    break;

                case 2:
                    NotificationIcon.ShowBalloonTip(2000, "Aniversários!!!", "Há aniversarios bem pertinhos de acontecer. Confira!", ToolTipIcon.Info);
                    break;

                case 3:
                    NotificationIcon.ShowBalloonTip(2000, "Aniversários!!!", "A data especial de uma pessoa está coladinha a hoje, dê uma olhada!", ToolTipIcon.Info);
                    break;

                default:
                    break;
            }
            Thread.Sleep(2000);

            CheckDuplicates();
        }

        /// <summary>
        /// Verifica os dados duplicados
        /// </summary>
        void CheckDuplicates()
        {
            if(DataHelper.CheckDuplicatas())
            {
                janela = InfoClick.Duplicatas;
                NotificationIcon.ShowBalloonTip(2000, "Dados duplicados detectados!", "Foi detectado dados que se repetem sem necessidade dentro do banco de dados", ToolTipIcon.Warning);
            }
        }

        void ShowCaixaOpcoes()
        {
            WatchID.Visible = true;
            WatchCaixaValue.Visible = true;

            FecharCaixaBtn.Enabled = true;
            PauseCaixa.Enabled = true;
            AbrirCaixaBtn.Enabled = false;
        }

        void HideCaixaOpcoes()
        {
            WatchID.Visible = false;
            WatchCaixaValue.Visible = false;

            FecharCaixaBtn.Enabled = false;
            PauseCaixa.Enabled = false;
            AbrirCaixaBtn.Enabled = true;
        }
    }
}
