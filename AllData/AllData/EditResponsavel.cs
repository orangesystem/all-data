﻿using System;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AllData
{
    public partial class EditResponsavel : Form
    {
        bool controlesValidos = false;

        public EditResponsavel()
        {
            InitializeComponent();
        }

        private void EditResponsavel_Load(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    ConnectionHelper.ResponsavelTable.Fill(mainDataSet.Responsavel);
                    break;

                case false:
                    MessageBox.Show("Indisponível no modo offline!");
                    Close();
                    break;
            }
        }

        #region Botões
        //Botão editar
        private void EditBtn_Click(object sender, EventArgs e)
        {
            //Carrega as informações do responsável e as renderiza nos controles de edição
            InputForm iF = new InputForm();
            if (iF.ShowDialog() == DialogResult.OK)
            {
                try {
                    MainDataSet.ResponsavelRow responsavel = mainDataSet.Responsavel.FindByID((int)RespComb.SelectedValue);
                    LoadInformations(responsavel);
                }
                catch (NullReferenceException) { MessageBox.Show("Não há registros a serem editados!"); }
                catch (Exception err)
                {
                    MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                    Close();
                }
            }
        }

        //Botão confirmar
        private void commitbtn_Click(object sender, EventArgs e)
        {
            //Confirma se o usuário realmente deseja alterar os dados
            if (MessageBox.Show("Se você confirmar, os dados serão atualizados, deseja prosseguir?", "Atualizar dados", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                MainDataSet.ResponsavelRow responsavel = mainDataSet.Responsavel.FindByID((int)RespComb.SelectedValue);
                try { responsavel = ValidarControles(responsavel); }
                catch { MessageBox.Show("Verifique se todos os campos estão preenchidos corretamente!"); }
                //Pega as informações contidas nos controles de edição

                if (Birthdate.Checked) { responsavel.Nascimento = Birthdate.Value; }
                else { responsavel.Nascimento = new DateTime(); }

                if(controlesValidos)
                switch (ConnectionHelper.Online)
                {
                    case true:
                        try
                        {
                            //Tenta fazer as alterações no banco de dados
                            DataSet tmpData = mainDataSet.GetChanges();
                            ConnectionHelper.ResponsavelTable.Update(responsavel);
                            mainDataSet.Merge(tmpData);
                            mainDataSet.AcceptChanges();
                            StatusLabelInfoResp.Text = "Dados atualizados com sucesso!";

                            StatusLabelInfoResp.Text = "Esperando por background";
                            Thread.Sleep(1500);
                        }

                        //Caso não seja possível, informará ao usuário
                        catch (SqlException message)
                        {
                            statusStrip1.BackColor = Color.Red;
                            StatusLabelInfoResp.Text = "Erro ao Atualizar Tabela!";
                            if (MessageBox.Show("Erro ao atulizar dados: " + message.Message) == DialogResult.OK)
                            {
                                statusStrip1.BackColor = Color.White;
                                StatusLabelInfoResp.Text = "Ultima atualização não realizada!";
                            }

                            if (message.ErrorCode == -2146232060)
                            {
                                if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    InputForm IP = new InputForm();
                                    if (IP.ShowDialog() == DialogResult.OK)
                                    {
                                        ConnectionHelper.GoOfflineMode();
                                        OfflineResponsavel responsavelOff = new OfflineResponsavel(responsavel, ComandType.Update);
                                        ConnectionHelper.OfflineData.OfflineResponsavelDataTable.Add(responsavelOff);
                                        ConnectionHelper.SaveOfflineData();

                                        StatusLabelInfoResp.Text = "Dados salvos com sucesso!";
                                        ClearControls();
                                    }
                                }
                            }
                        }
                        finally
                        {
                            //Limpa os controles de edição e ativa o cabeçalho de seleção de dados
                            ClearControls();
                            InfoGBox.Enabled = false;
                            ContatoGBox.Enabled = false;
                            AcaoPainel.Enabled = false;
                            HeaderGBox.Enabled = true;
                        }
                        break;

                    case false:
                            MessageBox.Show("Não é possível editar no modo offline!");
                        break;
                }
            }
        }

        //Botão cancelar
        private void CancelBtn_Click(object sender, EventArgs e)
        {
            //Confirma com o usuário se ele realmente deseja cancelar as alterações feitas
            if (MessageBox.Show("Se você cancelar, os dados que não foram atualizados serão perdidos, deseja prosseguir?", "Cancelar operação", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                //Limpa os controles e ativa o cabeçalho de seleção de dados
                ClearControls();
                InfoGBox.Enabled = false;
                ContatoGBox.Enabled = false;
                AcaoPainel.Enabled = false;
                HeaderGBox.Enabled = true;

                StatusLabelInfoResp.Text = "Operação Cancelada!";
            }
        }

        //Botão Deletar
        private void RemoveBtn_Click(object sender, EventArgs e)
        {
            //Confirma com o usuário se ele realmente deseja deletar os dados
            if (MessageBox.Show("Deseja realmente remover este responsável? Se prosseguir, as informações do mesmo podem ser movidas para o banco de arquivos-mortos " +
              "Se houver informações associadas a este responsável, como registro de alunos, os mesmos também serão movidos.", "Remover responsável", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                MainDataSet.ResponsavelRow responsavel = null;
                //Se o usuário confirmar, procura o responsável no dataset pela sua ID descrita na seleção e o remove
                try
                {
                    responsavel = mainDataSet.Responsavel.FindByID((int)RespComb.SelectedValue);
                    try
                    {
                        ConnectionHelper.ResponsavelTable.Delete(responsavel.ID, responsavel.Nome, responsavel.CPF, responsavel.Telefone1,
                      responsavel.Telefone2, responsavel.Telefone3, responsavel.Nascimento);

                        responsavel.Delete();
                    }
                    catch
                    {
                        ConnectionHelper.ResponsavelTable.Delete(responsavel.ID, responsavel.Nome, responsavel.CPF, responsavel.Telefone1,
                    responsavel.Telefone2, responsavel.Telefone3, null);

                        responsavel.Delete();
                    }
                    StatusLabelInfoResp.Text = "responsável deletado com sucesso!";
                }
                catch (SqlException message)
                {
                    statusStrip1.BackColor = Color.Red;
                    StatusLabelInfoResp.Text = "Erro ao Atualizar Tabela!";
                    if (MessageBox.Show("Não foi possível remover dados: " + message.Message) == DialogResult.OK)
                    {
                        statusStrip1.BackColor = Color.White;
                        StatusLabelInfoResp.Text = "Ultima atualização não realizada!";
                    }

                    if (message.ErrorCode == -2146232060)
                    {
                        if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            InputForm IP = new InputForm();
                            if (IP.ShowDialog() == DialogResult.OK)
                            {
                                ConnectionHelper.GoOfflineMode();
                                OfflineResponsavel responsavelDeletado = new OfflineResponsavel(responsavel, ComandType.Delete);
                                ConnectionHelper.OfflineData.OfflineResponsavelDataTable.Add(responsavelDeletado);
                                ConnectionHelper.SaveOfflineData();

                                StatusLabelInfoResp.Text = "Dados salvos com sucesso!";
                                ClearControls();
                            }
                        }
                    }
                }
                catch (NullReferenceException) { MessageBox.Show("Não há registros a serem removidos!"); }

                catch (Exception err)
                {
                    MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                }
            }
        }
        #endregion

        /// <summary>
        /// Carrega as informações nos controles de edição de dados
        /// </summary>
        /// <param name="responsavel"></param>
        void LoadInformations(MainDataSet.ResponsavelRow responsavel)
        {
            //Remove a máscara dos MaskBoxes
            CPFBox.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            Tel1.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            Tel2.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            Tel3.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;

            //Carrega os dados nos controles
            try { NameBox.Text = responsavel.Nome; } catch  { }
            try { CPFBox.Text = responsavel.CPF; } catch  { }
            try { Tel1.Text = responsavel.Telefone1; } catch  { }
            try { Tel2.Text = responsavel.Telefone2; } catch { }
            try { Tel3.Text = responsavel.Telefone3; } catch  { }

            try
            {
                Birthdate.Value = responsavel.Nascimento;
                Birthdate.Checked = true;
            }
            catch { Birthdate.Checked = false; }

            //Ativa os controles de edição e desativa o cabeçalho de seleção de dados
            InfoGBox.Enabled = true;
            ContatoGBox.Enabled = true;
            AcaoPainel.Enabled = true;
            HeaderGBox.Enabled = false;
        }

        /// <summary>
        /// Limpa os controles de edição de dados
        /// </summary>
        void ClearControls()
        {
            NameBox.Clear();
            CPFBox.Clear();
            Birthdate.Value = DateTime.Today;
            Tel1.Clear();
            Tel2.Clear();
            Tel3.Clear();
        }

        MainDataSet.ResponsavelRow ValidarControles (MainDataSet.ResponsavelRow responsavel)
        {
            if(NameBox.Text.Length > 0)
            {
                if(NameBox.Text == "" || NameBox.Text == " ")
                {
                    controlesValidos = false;
                    throw new InvalidControlValue();
                }
                else { responsavel.Nome = NameBox.Text; controlesValidos = true; }
            }
            else
            {
                controlesValidos = false;
                throw new InvalidControlValue();
            }

            if(CPFBox.TextNoFormatting().Length != 11)
            {
                if(CPFBox.TextNoFormatting().Length > 0)
                {
                    controlesValidos = false;
                    throw new InvalidControlValue();
                }
                else
                {
                    responsavel.CPF = CPFBox.TextNoFormatting();
                    controlesValidos = true;
                }
            }
            else
            {
                responsavel.CPF = CPFBox.TextNoFormatting();
                controlesValidos = true;
            }

            responsavel.Telefone1 = Tel1.TextNoFormatting();
            responsavel.Telefone2 = Tel2.TextNoFormatting();
            responsavel.Telefone3 = Tel3.TextNoFormatting();

            return responsavel;
        }
    }
}