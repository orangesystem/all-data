﻿namespace AllData
{
    partial class EditPoints
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditPoints));
            this.Header = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.EditBtn = new System.Windows.Forms.Button();
            this.ModuleComb = new System.Windows.Forms.TextBox();
            this.fKPontuacaoAlunoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.alunoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSet = new AllData.MainDataSet();
            this.LessonComb = new System.Windows.Forms.ComboBox();
            this.NomeComb = new System.Windows.Forms.ComboBox();
            this.alunoTableAdapter = new AllData.MainDataSetTableAdapters.AlunoTableAdapter();
            this.pontuacaoTableAdapter = new AllData.MainDataSetTableAdapters.PontuacaoTableAdapter();
            this.Bottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.CommitBtn = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.Body = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TestDate = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.NomeComb2 = new System.Windows.Forms.ComboBox();
            this.PointsBox = new System.Windows.Forms.TextBox();
            this.LessonBox = new System.Windows.Forms.TextBox();
            this.Header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fKPontuacaoAlunoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).BeginInit();
            this.Bottom.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.Body.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Header
            // 
            this.Header.Controls.Add(this.label3);
            this.Header.Controls.Add(this.label2);
            this.Header.Controls.Add(this.label1);
            this.Header.Controls.Add(this.DeleteBtn);
            this.Header.Controls.Add(this.EditBtn);
            this.Header.Controls.Add(this.ModuleComb);
            this.Header.Controls.Add(this.LessonComb);
            this.Header.Controls.Add(this.NomeComb);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(433, 107);
            this.Header.TabIndex = 0;
            this.Header.TabStop = false;
            this.Header.Text = "Selecionar dado";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(250, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Modulo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Lição";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Aluno";
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Location = new System.Drawing.Point(271, 78);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(75, 23);
            this.DeleteBtn.TabIndex = 4;
            this.DeleteBtn.Text = "Deletar";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // EditBtn
            // 
            this.EditBtn.Location = new System.Drawing.Point(352, 78);
            this.EditBtn.Name = "EditBtn";
            this.EditBtn.Size = new System.Drawing.Size(75, 23);
            this.EditBtn.TabIndex = 1;
            this.EditBtn.Text = "Editar";
            this.EditBtn.UseVisualStyleBackColor = true;
            this.EditBtn.Click += new System.EventHandler(this.EditBtn_Click);
            // 
            // ModuleComb
            // 
            this.ModuleComb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fKPontuacaoAlunoBindingSource, "Modulo", true));
            this.ModuleComb.Location = new System.Drawing.Point(298, 47);
            this.ModuleComb.Name = "ModuleComb";
            this.ModuleComb.ReadOnly = true;
            this.ModuleComb.Size = new System.Drawing.Size(119, 20);
            this.ModuleComb.TabIndex = 2;
            // 
            // fKPontuacaoAlunoBindingSource
            // 
            this.fKPontuacaoAlunoBindingSource.DataMember = "FK_Pontuacao_Aluno";
            this.fKPontuacaoAlunoBindingSource.DataSource = this.alunoBindingSource;
            // 
            // alunoBindingSource
            // 
            this.alunoBindingSource.DataMember = "Aluno";
            this.alunoBindingSource.DataSource = this.mainDataSet;
            // 
            // mainDataSet
            // 
            this.mainDataSet.DataSetName = "MainDataSet";
            this.mainDataSet.EnforceConstraints = false;
            this.mainDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LessonComb
            // 
            this.LessonComb.DataSource = this.fKPontuacaoAlunoBindingSource;
            this.LessonComb.DisplayMember = "Licao";
            this.LessonComb.FormattingEnabled = true;
            this.LessonComb.Location = new System.Drawing.Point(92, 47);
            this.LessonComb.Name = "LessonComb";
            this.LessonComb.Size = new System.Drawing.Size(121, 21);
            this.LessonComb.TabIndex = 1;
            this.LessonComb.ValueMember = "ID";
            // 
            // NomeComb
            // 
            this.NomeComb.DataSource = this.alunoBindingSource;
            this.NomeComb.DisplayMember = "Nome";
            this.NomeComb.FormattingEnabled = true;
            this.NomeComb.Location = new System.Drawing.Point(92, 19);
            this.NomeComb.Name = "NomeComb";
            this.NomeComb.Size = new System.Drawing.Size(121, 21);
            this.NomeComb.TabIndex = 0;
            this.NomeComb.ValueMember = "ID";
            // 
            // alunoTableAdapter
            // 
            this.alunoTableAdapter.ClearBeforeFill = true;
            // 
            // pontuacaoTableAdapter
            // 
            this.pontuacaoTableAdapter.ClearBeforeFill = true;
            // 
            // Bottom
            // 
            this.Bottom.Controls.Add(this.panel1);
            this.Bottom.Controls.Add(this.statusStrip1);
            this.Bottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Bottom.Enabled = false;
            this.Bottom.Location = new System.Drawing.Point(0, 312);
            this.Bottom.Name = "Bottom";
            this.Bottom.Size = new System.Drawing.Size(433, 58);
            this.Bottom.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CancelBtn);
            this.panel1.Controls.Add(this.CommitBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(433, 33);
            this.panel1.TabIndex = 1;
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Location = new System.Drawing.Point(268, 3);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 23);
            this.CancelBtn.TabIndex = 5;
            this.CancelBtn.Text = "Cancelar";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // CommitBtn
            // 
            this.CommitBtn.Location = new System.Drawing.Point(349, 3);
            this.CommitBtn.Name = "CommitBtn";
            this.CommitBtn.Size = new System.Drawing.Size(75, 23);
            this.CommitBtn.TabIndex = 4;
            this.CommitBtn.Text = "Confirmar";
            this.CommitBtn.UseVisualStyleBackColor = true;
            this.CommitBtn.Click += new System.EventHandler(this.CommitBtn_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 36);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(433, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(39, 17);
            this.StatusLabel.Text = "Status";
            // 
            // Body
            // 
            this.Body.Controls.Add(this.groupBox4);
            this.Body.Controls.Add(this.groupBox3);
            this.Body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Body.Enabled = false;
            this.Body.Location = new System.Drawing.Point(0, 107);
            this.Body.Name = "Body";
            this.Body.Size = new System.Drawing.Size(433, 205);
            this.Body.TabIndex = 4;
            this.Body.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.TestDate);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(3, 125);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(427, 100);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Referência";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Data da Prova";
            // 
            // TestDate
            // 
            this.TestDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.TestDate.Location = new System.Drawing.Point(89, 33);
            this.TestDate.Name = "TestDate";
            this.TestDate.Size = new System.Drawing.Size(221, 20);
            this.TestDate.TabIndex = 3;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.NomeComb2);
            this.groupBox3.Controls.Add(this.PointsBox);
            this.groupBox3.Controls.Add(this.LessonBox);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(3, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(427, 109);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informações";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Pontuação";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Lição";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Aluno";
            // 
            // NomeComb2
            // 
            this.NomeComb2.Cursor = System.Windows.Forms.Cursors.No;
            this.NomeComb2.DataSource = this.alunoBindingSource;
            this.NomeComb2.DisplayMember = "Nome";
            this.NomeComb2.Enabled = false;
            this.NomeComb2.FormattingEnabled = true;
            this.NomeComb2.Location = new System.Drawing.Point(77, 20);
            this.NomeComb2.Name = "NomeComb2";
            this.NomeComb2.Size = new System.Drawing.Size(121, 21);
            this.NomeComb2.TabIndex = 0;
            this.NomeComb2.ValueMember = "ID";
            // 
            // PointsBox
            // 
            this.PointsBox.Location = new System.Drawing.Point(77, 73);
            this.PointsBox.Name = "PointsBox";
            this.PointsBox.Size = new System.Drawing.Size(100, 20);
            this.PointsBox.TabIndex = 2;
            // 
            // LessonBox
            // 
            this.LessonBox.Location = new System.Drawing.Point(77, 47);
            this.LessonBox.Name = "LessonBox";
            this.LessonBox.Size = new System.Drawing.Size(100, 20);
            this.LessonBox.TabIndex = 1;
            // 
            // EditPoints
            // 
            this.AcceptButton = this.CommitBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(433, 370);
            this.Controls.Add(this.Body);
            this.Controls.Add(this.Bottom);
            this.Controls.Add(this.Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditPoints";
            this.Text = "Editar Pontuação";
            this.Load += new System.EventHandler(this.EditPoints_Load);
            this.Header.ResumeLayout(false);
            this.Header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fKPontuacaoAlunoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).EndInit();
            this.Bottom.ResumeLayout(false);
            this.Bottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.Body.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Header;
        private System.Windows.Forms.ComboBox NomeComb;
        private MainDataSet mainDataSet;
        private System.Windows.Forms.BindingSource alunoBindingSource;
        private MainDataSetTableAdapters.AlunoTableAdapter alunoTableAdapter;
        private System.Windows.Forms.ComboBox LessonComb;
        private System.Windows.Forms.BindingSource fKPontuacaoAlunoBindingSource;
        private MainDataSetTableAdapters.PontuacaoTableAdapter pontuacaoTableAdapter;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.Button EditBtn;
        private System.Windows.Forms.TextBox ModuleComb;
        private System.Windows.Forms.Panel Bottom;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox Body;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker TestDate;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox NomeComb2;
        private System.Windows.Forms.TextBox PointsBox;
        private System.Windows.Forms.TextBox LessonBox;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button CommitBtn;
    }
}