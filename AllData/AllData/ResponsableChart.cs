﻿using System;
using System.Windows.Forms;

namespace AllData
{
    public partial class ResponsableChart : Form
    {
        public ResponsableChart()
        {
            InitializeComponent();
        }

        private void ResponsableChart_Load(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    ConnectionHelper.AlunoTable.Fill(mainDataSet.Aluno);
                    ConnectionHelper.ResponsavelTable.Fill(mainDataSet.Responsavel);
                    break;

                case false:
                    break;
            }
        }

        private void verToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InputForm ipf = new InputForm();
            if(ipf.ShowDialog() == DialogResult.OK)
            { CPFBox.Visible = true; }
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
