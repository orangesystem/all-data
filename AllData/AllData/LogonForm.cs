﻿using System;
using System.Windows.Forms;

namespace AllData
{
    public partial class LogonForm : Form
    {
        public LogonForm()
        {
            InitializeComponent();
        }

        //Botão de Logon
        private void LogonBtn_Click(object sender, EventArgs e)
        {
            string nome = NomeBox1.Text;
            string senha = PasswordBox1.Text;
            if(nome == "trg5764_quentyrdomariazye035673425")
            { if (ConnectionHelper.PseudoAwake()){ DialogResult = DialogResult.OK; Close(); } }
            else if(ConnectionHelper.Awake(nome, senha))
            { DialogResult = DialogResult.OK ; Close(); }
        }

        //Botão de Sair
        private void ExitBtn1_Click(object sender, EventArgs e)
        { Application.Exit(); }
    }
}
