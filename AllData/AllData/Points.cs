﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using AllData.MainDataSetTableAdapters;

namespace AllData
{
    public partial class Points : Form
    {
        private bool controlesValidos = false;

        public Points()
        {
            InitializeComponent();
        }

        private void CommitBtn3_Click(object sender, EventArgs e)
        {
            //TODO: Corrigir o sistema offline em versões posteriores
            DateTime? rday;
            MainDataSet.PontuacaoRow pontuacaoValida = null;
            try { pontuacaoValida = ValidarControles(); }
            catch { MessageBox.Show("Preencha todos os campos obrigatórios e certifique-se de que a pontuação está corretamente inserida!"); }
            if (ReferenceDate1.Checked) { rday = ReferenceDate1.Value; } else { rday = null; }

            if(controlesValidos)
            switch (ConnectionHelper.Online)
            {
                case true:
                        try
                        {
                            PontuacaoTableAdapter pontos = ConnectionHelper.PontuacaoTable;
                            pontos.Insert((Guid)AlunoComb2.SelectedValue, (string)ModuleComb1.SelectedItem, LessonBox1.Text, ReferenceDate1.Value, decimal.Parse(ResultBox1.Text));
                            ClearControls();
                            StatusLabelPontuacao.Text = "Dados inseridos com sucesso!";
                        }

                        catch (SqlException message)
                        {
                            MessageBox.Show("Não foi possível inserir os dados: " + message.Message);

                            if (message.ErrorCode == -2146232060)
                            {
                                if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    InputForm IP = new InputForm();
                                    if (IP.ShowDialog() == DialogResult.OK)
                                    {
                                        ConnectionHelper.GoOfflineMode();
                                        OfflinePontuacao pontuacao = new OfflinePontuacao(pontuacaoValida, ComandType.Insert);
                                        pontuacao.Data = rday;
                                        ConnectionHelper.OfflineData.OfflinePontuacaoDataTable.Add(pontuacao);

                                        ConnectionHelper.SaveOfflineData();
                                        StatusLabelPontuacao.Text = "Dados salvos com sucesso!";
                                        ClearControls();
                                    }
                                }
                            }
                        }
                        catch (Exception err)
                        {
                            MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                            this.Close();
                        }
                    break;

                case false:
                    OfflinePontuacao pontuacao2 = new OfflinePontuacao(pontuacaoValida, ComandType.Insert);
                    pontuacao2.Data = rday;
                        ConnectionHelper.OfflineData.OfflinePontuacaoDataTable.Add(pontuacao2);

                    ConnectionHelper.SaveOfflineData();
                    StatusLabelPontuacao.Text = "Dados salvos com sucesso!";
                    ClearControls();
                    break;
            }
        }

        private void Points_Load(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    ConnectionHelper.AlunoTable.Fill(this.mainDataSet.Aluno);
                    break;
                case false:
                    MessageBox.Show("Indisponível no modo offline!");
                    this.Close();
                    break;
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Descartar dados?", "Cancelar dados", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                ClearControls();
                StatusLabelPontuacao.Text = "Operação cancelada!";
            }
        }

        void ClearControls()
        {
            LessonBox1.Clear();
            ReferenceDate1.Value = DateTime.Today;
            ResultBox1.Clear();
        }

        private MainDataSet.PontuacaoRow ValidarControles()
        {
            MainDataSet dataset = new MainDataSet();
            MainDataSet.PontuacaoRow pontuacaoValida = dataset.Pontuacao.NewPontuacaoRow();

            #region Checkaluno
            try { pontuacaoValida.Aluno = (Guid)AlunoComb2.SelectedValue; controlesValidos = true; }
            catch { controlesValidos = false; throw new InvalidControlValue(); }
            #endregion

            #region CheckModulo
            try { pontuacaoValida.Modulo = (string)ModuleComb1.SelectedItem; controlesValidos = true; }
            catch {controlesValidos = false; throw new InvalidControlValue(); }
            #endregion

            #region CheckLesson
            if (LessonBox1.Text.Length > 0)
            {
                if(LessonBox1.Text == "" || LessonBox1.Text == " ")
                {
                    controlesValidos = false;
                    throw new InvalidControlValue();
                }
                else { pontuacaoValida.Licao = LessonBox1.Text; controlesValidos = true; }
            }
            #endregion

            #region CheckResult
            if (ResultBox1.Text.Length > 0)
            {
                if(ResultBox1.Text == "" || ResultBox1.Text == " ")
                {
                    controlesValidos = false;
                    throw new InvalidControlValue();
                }
                else
                {
                    try { pontuacaoValida.Resultado = decimal.Parse(ResultBox1.Text); controlesValidos = true; }
                    catch { controlesValidos = false; throw new InvalidControlValue(); }
                }
            }
            #endregion

            return pontuacaoValida;
        }
    }
}
