﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using AllData.MainDataSetTableAdapters;

namespace AllData
{
    public partial class Responsavel : Form
    {
        private bool controlesValidos = false;
        public Responsavel()
        {
            InitializeComponent();
        }

        //Botão Confirmar
        private void CommitBtn3_Click(object sender, EventArgs e)
        {

            MainDataSet.ResponsavelRow responsavelValido = null;
            try { responsavelValido = ValidarControles(); } catch { MessageBox.Show("Preencha todos os controles obrigatórios!"); }

            DateTime? bday;
            if (Birthdate1.Checked) { bday = Birthdate1.Value; } else { bday = null; }

            if(controlesValidos)
            switch (ConnectionHelper.Online)
            {
                case true:
                    try
                    {
                        ResponsavelTableAdapter responsive = ConnectionHelper.ResponsavelTable;

                        StatusLabelResp.Text = "Validando nome...por favor aguarde";
                        if (DataHelper.Exist(responsavelValido.Nome, DataHelper.ResponsavelCheckingType.Nome))
                        {
                            if (MessageBox.Show("Já existe um registro com o nome do responsável que está tentando inserir, deseja continuar mesmo assim?", "Ocorrencia de possível duplicata detectada!", MessageBoxButtons.OKCancel) == DialogResult.OK)
                            {
                                responsive.Insert(responsavelValido.Nome, responsavelValido.CPF, responsavelValido.Telefone1, responsavelValido.Telefone2, responsavelValido.Telefone3, bday);
                                StatusLabelResp.Text = "Dados inseridos com sucesso!";
                                ClearControls();
                            }
                        }
                        else
                        {
                            responsive.Insert(responsavelValido.Nome, responsavelValido.CPF, responsavelValido.Telefone1, responsavelValido.Telefone2, responsavelValido.Telefone3, bday);
                            StatusLabelResp.Text = "Dados inseridos com sucesso!";
                            ClearControls();
                        }
                    }

                    catch (SqlException message)
                    {
                        MessageBox.Show("Erro ao inserir dados: " + message.Message);

                        if (message.ErrorCode == -2146232060)
                        {
                            if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                            {
                                InputForm IP = new InputForm();
                                if (IP.ShowDialog() == DialogResult.OK)
                                {
                                    ConnectionHelper.GoOfflineMode();
                                    OfflineResponsavel responsavel = new OfflineResponsavel(responsavelValido, ComandType.Insert);
                                    responsavel.Nascimento = Birthdate1.Value;

                                        ConnectionHelper.OfflineData.OfflineResponsavelDataTable.Add(responsavel);
                                    ConnectionHelper.SaveOfflineData();

                                    StatusLabelResp.Text = "Dados salvos com sucesso!";
                                    ClearControls();
                                }
                            }
                        }
                    }
                    catch(Exception err)
                    {
                        MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n {0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                    }
                    break;

                case false:
                    OfflineResponsavel responsavel2 = new OfflineResponsavel(responsavelValido, ComandType.Insert);
                        try
                        { responsavel2.Nascimento = bday; }
                        catch { MessageBox.Show("Aqui"); }

                        ConnectionHelper.OfflineData.OfflineResponsavelDataTable.Add(responsavel2);
                    ConnectionHelper.SaveOfflineData();
                    StatusLabelResp.Text = "Dados salvos com sucesso!";
                    ClearControls();
                    break;
            }
        }

        void ClearControls()
        {
            NameBox3.Clear();
            Birthdate1.Value = DateTime.Today;
            CPFBox1.Clear();
            Tel1Box2.Clear();
            Tel2Box2.Clear();
            Tel3Box2.Clear();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Descartar dados?", "Cancelar dados", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                ClearControls();
                StatusLabelResp.Text = "Operação cancelada!";
            }
        }


        private MainDataSet.ResponsavelRow ValidarControles()
        {
            MainDataSet dataset = new MainDataSet();
            MainDataSet.ResponsavelRow responsavelValido = dataset.Responsavel.NewResponsavelRow();

            #region CheckNome
            if(NameBox3.Text.Length > 0)
            {
                if(NameBox3.Text == "" || NameBox3.Text == " ")
                { controlesValidos = false; throw new InvalidControlValue(); }

                else { controlesValidos = true; responsavelValido.Nome = NameBox3.Text; }
            }
            else { controlesValidos = false; throw new InvalidControlValue(); }
            #endregion

            #region CheckCPF
            if (CPFBox1.TextNoFormatting().Length != 11)
            {
                if (CPFBox1.Text.Length == 0)
                { controlesValidos = true; responsavelValido.CPF = CPFBox1.TextNoFormatting(); }
                else {
                    controlesValidos = false;
                    throw new InvalidControlValue();
                }
            }
            else
            {
                controlesValidos = true;
                responsavelValido.CPF = CPFBox1.TextNoFormatting();
            }
            #endregion

            #region CheckTelefone
            string tel1 = Tel1Box2.TextNoFormatting();
            string tel2 = Tel2Box2.TextNoFormatting();
            string tel3 = Tel3Box2.TextNoFormatting();
            controlesValidos = true;

            responsavelValido.Telefone1 = tel1;
            responsavelValido.Telefone2 = tel2;
            responsavelValido.Telefone3 = tel3;
            #endregion

            responsavelValido.Nascimento = new DateTime();
            return responsavelValido;
        }

        private void Responsavel_Load(object sender, EventArgs e)
        {
            if(!ConnectionHelper.Online)
            {
                MessageBox.Show("Indisponível no modo offline!");
                Close();
            }
        }
    }
}
