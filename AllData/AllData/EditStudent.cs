﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AllData
{
    public partial class EditStudent : Form
    {
        bool controlesValidos = false;
        public EditStudent()
        {
            InitializeComponent();
        }

        private void EditStudent_Load(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    try
                    {
                        ConnectionHelper.ResponsavelTable.Fill(this.mainDataSet.Responsavel);
                        ConnectionHelper.AlunoTable.Fill(this.mainDataSet.Aluno);
                    }
                    catch (Exception err)
                    {
                        MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                        Close();
                    }
                    break;

                case false:
                    MessageBox.Show("Indisponível no modo offline!");
                    break;
            }

        }

        #region Botões
        //Botão editar informações
        private void EditBtn1_Click(object sender, EventArgs e)
        {
            try
            {
                MainDataSet.AlunoRow aluno = mainDataSet.Aluno.FindByID((Guid)AlunoComb2.SelectedValue);

                LoadInformations(aluno);
            }
            catch (NullReferenceException) { MessageBox.Show("Não há registros a serem editados!"); }
            catch (Exception err)
            {
                MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                Close();
            }
        }

        //Botão para confirmar alterações de dados
        private void CommitBtn3_Click(object sender, EventArgs e)
        {
            //Pergunta ao usuário se deseja confirmar alterações
            if (MessageBox.Show("Se você confirmar, os dados serão atualizados, deseja prosseguir?", "Atualizar dados", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                //Passa aterações dos controles para os dados do usuário
                MainDataSet.AlunoRow aluno = mainDataSet.Aluno.FindByID((Guid)AlunoComb2.SelectedValue);
                try { aluno = ValidarControles(aluno); }
                catch { MessageBox.Show("Preencha os campos necessários!"); }

                DateTime mday;
                DateTime bday;
                if (Birthdate1.Checked) { bday = Birthdate1.Value; } else { bday = new DateTime(); }
                if (RegisteringDate1.Checked) { mday = RegisteringDate1.Value; } else { mday = new DateTime(); }

                aluno.Nascimento = bday; aluno.Matricula = mday;

                if (controlesValidos)
                    switch (ConnectionHelper.Online)
                    {
                        case true:
                            //Tenta atualizar as informações no Dataset e no banco de dados
                            try
                            {
                                DataSet tempData = mainDataSet.GetChanges();
                                ConnectionHelper.AlunoTable.Update(aluno);

                                mainDataSet.Merge(tempData);
                                mainDataSet.AcceptChanges();
                                StatusLabel.Text = "Dados Atualizados com sucesso!";

                                ClearControls();
                                groupBox1.Enabled = false;
                                groupBox4.Enabled = false;
                                Head1.Enabled = true;
                            }
                            //Caso não seja possível, informará ao usuário
                            catch (SqlException message)
                            {
                                Status.BackColor = Color.Red;
                                StatusLabel.Text = "Erro ao Atualizar Tabela!";
                                if (MessageBox.Show("Erro ao atulizar dados: " + message.Message) == DialogResult.OK)
                                {
                                    Status.BackColor = Color.White;
                                    StatusLabel.Text = "Ultima atualização não realizada!";
                                }

                                if (message.ErrorCode == -2146232060)
                                {
                                    if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                                    {
                                        InputForm IP = new InputForm();
                                        if (IP.ShowDialog() == DialogResult.OK)
                                        {
                                            ConnectionHelper.GoOfflineMode();
                                            OfflineAluno alunoOffline = new OfflineAluno(aluno, ComandType.Update);
                                            alunoOffline.Nascimento = bday;
                                            alunoOffline.Matricula = mday;
                                            ConnectionHelper.OfflineData.OfflineAlunoDataTable.Add(alunoOffline);
                                            ConnectionHelper.SaveOfflineData();

                                            StatusLabel.Text = "Dados salvos com sucesso!";
                                            ClearControls();
                                        }
                                    }
                                }
                            }
                            catch (Exception err)
                            {
                                MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                            }
                            break;

                        case false:
                            break;
                    }
            }
        }

        //Botão cancelar
        private void CancelBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Se você cancelar, os dados que não foram atualizados serão perdidos, deseja prosseguir?", "Cancelar operação", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                ClearControls();
                groupBox1.Enabled = false;
                groupBox4.Enabled = false;
                Head1.Enabled = true;
                StatusLabel.Text = "Operação cancelada!";
            }
        }

        //Botão Deletar
        private void DeleteBtn1_Click(object sender, EventArgs e)
        {
            //Confirma com o usuário se ele realmente deseja deletar os dados
            if (MessageBox.Show("Deseja realmente remover este aluno? Se prosseguir, as informações do mesmo incluindo seu registro de notas e pagamentos " +
               "serão movidos para outro banco de dados onde são armazenados os arquivos-mortos", "Remover aluno", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                MainDataSet.AlunoRow aluno = null;

                switch (ConnectionHelper.Online)
                {
                    case true:
                        //Se o usuário confirmar, procura o aluno no dataset pela sua ID descrita na seleção e o remove
                        try
                        {
                            aluno = mainDataSet.Aluno.FindByID((Guid)AlunoComb2.SelectedValue);
                            ConnectionHelper.AlunoTable.Delete(aluno.ID, aluno.Nome, aluno.Nascimento, aluno.Telefone1, aluno.Telefone2,
                                aluno.Telefone3, aluno.Responsável, aluno.Matricula);
                            aluno.Delete();

                            StatusLabel.Text = "Aluno deletado com sucesso!";
                        }

                        //Caso ocorra algum erro, avisa ao usuário sobre o mesmo
                        catch (SqlException message)
                        {
                            Status.BackColor = Color.Red;
                            StatusLabel.Text = "Erro ao Atualizar Tabela!";
                            if (MessageBox.Show("Não foi possível remover dados: " + message.Message) == DialogResult.OK)
                            {
                                Status.BackColor = Color.White;
                                StatusLabel.Text = "Ultima atualização não realizada!";

                                if (message.ErrorCode == -2146232060)
                                {
                                    if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                                    {
                                        InputForm IP = new InputForm();
                                        if (IP.ShowDialog() == DialogResult.OK)
                                        {
                                            ConnectionHelper.GoOfflineMode();
                                            OfflineAluno alunoDeletado = new OfflineAluno(aluno, ComandType.Delete);
                                            ConnectionHelper.OfflineData.OfflineAlunoDataTable.Add(alunoDeletado);
                                            ConnectionHelper.SaveOfflineData();

                                            StatusLabel.Text = "Dados salvos com sucesso!";
                                            ClearControls();
                                        }
                                    }
                                }
                            }
                        }
                        catch (NullReferenceException) { MessageBox.Show("Não há registros a serem removidos!"); }

                        catch (Exception err)
                        {
                            MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                        }
                        break;

                    case false:
                        MessageBox.Show("Não é possível editar no modo offline");
                        break;
                }
            }
        }
        #endregion

        /// <summary>
        /// Limpa os controles de edição
        /// </summary>
        void ClearControls()
        {
            NomeBox1.Clear();
            Birthdate1.Value = DateTime.Today;
            RegisteringDate1.Value = DateTime.Today;

            Tel1Box3.Clear();
            Tel2Box3.Clear();
            Tel3Box3.Clear();
        }

        /// <summary>
        /// Carrega para os controles as informações do aluno selecionado
        /// </summary>
        /// <param name="aluno">Aluno a ser modificado</param>
        void LoadInformations(MainDataSet.AlunoRow aluno)
        {
            //Carrega as informações nos controles
            try { NomeBox1.Text = aluno.Nome; } catch { }

            try
            {
                Birthdate1.Value = aluno.Nascimento;
                Birthdate1.Checked = true;
            }
            catch { Birthdate1.Checked = false; }
            try
            {
                RegisteringDate1.Value = aluno.Matricula;
                RegisteringDate1.Checked = true;
            }
            catch { RegisteringDate1.Checked = false; }

            //Evita problemas na hora de passar os dados dos telefones para os controles por conta das mascaras
            Tel1Box3.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            Tel2Box3.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            Tel3Box3.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;

            try { Tel1Box3.Text = aluno.Telefone1; } catch { Tel1Box3.Text = ""; }
            try { Tel2Box3.Text = aluno.Telefone2; } catch { Tel2Box3.Text = ""; }
            try { Tel3Box3.Text = aluno.Telefone3; } catch { Tel3Box3.Text = ""; }
            RespComb2.SelectedValue = aluno.Responsável;
            RespComb2.DisplayMember = aluno.ResponsavelRow.Nome;

            //Desativa o cabeçalho e ativa os paineis de edição
            groupBox1.Enabled = true;
            groupBox4.Enabled = true;
            Head1.Enabled = false;
        }

        MainDataSet.AlunoRow ValidarControles(MainDataSet.AlunoRow alunoLinha)
        {
            if (NomeBox1.Text.Length > 0)
            {
                if (NomeBox1.Text == "" || NomeBox1.Text == " ")
                {
                    controlesValidos = false;
                    throw new InvalidControlValue();
                }
                else
                {
                    alunoLinha.Nome = NomeBox1.Text;
                    controlesValidos = true;
                }
            }
            else
            {
                controlesValidos = false;
                throw new InvalidControlValue();
            }

            try
            {
                alunoLinha.Responsável = (int)RespComb2.SelectedValue;
                controlesValidos = true;
            }
            catch
            {
                alunoLinha.Responsável = new int(); controlesValidos = true;
            }

            alunoLinha.Telefone1 = Tel1Box3.TextNoFormatting();
            alunoLinha.Telefone2 = Tel2Box3.TextNoFormatting();
            alunoLinha.Telefone3 = Tel3Box3.TextNoFormatting();

            return alunoLinha;
        }
    }
}
