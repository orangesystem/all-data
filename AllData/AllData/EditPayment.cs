﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AllData
{
    public partial class EditPayment : Form
    {
        bool controlesValidos = false;
        public EditPayment()
        {
            InitializeComponent();
        }

        private void EditPayment_Load(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    try {
                        ConnectionHelper.PagamentoTable.Fill(mainDataSet.Pagamento);
                        ConnectionHelper.AlunoTable.Fill(mainDataSet.Aluno);
                    }
                    catch (Exception err)
                    {
                        MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                        Close();
                    }
                    break;

                case false:
                    MessageBox.Show("indisponível no modo offline!");
                    Close();
                    break;
            }

        }

        #region Botões
        //Botão Editar
        private void EditBtn_Click(object sender, EventArgs e)
        {
            //Carrega os dados escolhidos do banco de dados e tentá enviá-los para os controles de edição
            try { MainDataSet.PagamentoRow pagamento = mainDataSet.Pagamento.FindByID((int)ReferenceComb1.SelectedValue);

                LoadInformations(pagamento);

                //Desativa o cabeçalho de seleção e ativa os controles de edição
                panel1.Enabled = true;
                panel2.Enabled = true;
                groupBox1.Enabled = false;
            }
            //Informa ao usuário que não existe nenhum registro de pagamento do aluno escolhido
            catch (NullReferenceException)
            {
                MessageBox.Show("Não há registros de pagamento para o aluno escolhido!");
            }
            catch (Exception err)
            {
                MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                Close();
            }
        }

        //Botão Confirmar
        private void CommitBtn_Click(object sender, EventArgs e)
        {
            //Confirma se o usuário realmente deseja alterar os dados 
            if (MessageBox.Show("Se você confirmar, os dados serão atualizados, deseja prosseguir?", "Atualizar dados", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                //Busca no banco de dados o registro a ser editado a partir da seleção feita no ReferenceComb1
                MainDataSet.PagamentoRow pagamento = mainDataSet.Pagamento.FindByID((int)ReferenceComb1.SelectedValue);

                try { pagamento = ValidarControles(pagamento); }
                catch { MessageBox.Show("certifique-se de que os controles estão corretamente preenchidos!"); }

                if(controlesValidos)
                switch (ConnectionHelper.Online)
                {
                    case true:
                        try
                        {
                            //Tenta alterar os dados no banco de dados
                            DataSet tmpData = mainDataSet.GetChanges();

                            ConnectionHelper.PagamentoTable.Update(pagamento);
                            mainDataSet.Merge(tmpData);
                            StatusLabel.Text = "Dados atualizados com sucesso!";
                        }
                        //Caso ocorra algum erro, informa ao usuário
                        catch (SqlException message)
                        {
                            statusStrip1.BackColor = Color.Red;
                            StatusLabel.Text = "Erro ao Atualizar Tabela!";
                            if (MessageBox.Show("Erro ao atulizar dados: " + message.Message) == DialogResult.OK)
                            {
                                statusStrip1.BackColor = Color.White;
                                StatusLabel.Text = "Ultima atualização não realizada!";
                            }

                            if (message.ErrorCode == -2146232060)
                            {
                                if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    InputForm IP = new InputForm();
                                    if (IP.ShowDialog() == DialogResult.OK)
                                    {
                                        ConnectionHelper.GoOfflineMode();
                                        OfflinePagamento pagamentoOff = new OfflinePagamento(pagamento, ComandType.Update);
                                        ConnectionHelper.OfflineData.OfflinePagamentoDataTable.Add(pagamentoOff);
                                        ConnectionHelper.SaveOfflineData();

                                        StatusLabel.Text = "Dados salvos com sucesso!";
                                    }
                                }
                            }
                        }
                        catch (Exception err)
                        {
                            MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                        }
                        //Limpa os controles de edição e ativa o cabeçalho de seleção e desativa os demais controles de edição
                        finally
                        {
                            ClearControls();
                            panel1.Enabled = false;
                            panel2.Enabled = false;
                            groupBox1.Enabled = true;
                        }
                        break;
                }
            }
        }

        //Botão Deletar
        private void RemoveBtn_Click(object sender, EventArgs e)
        {
            //Confirma se o usuário realmente deseja remover o registro selecionado
            if (MessageBox.Show("Deseja realmente remover este pagamento? Se prosseguir, as informações do mesmo podem ser movidas para o banco de arquivos-mortos " +
              "", "Remover pagamento", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                MainDataSet.PagamentoRow pagamento = null;

                switch (ConnectionHelper.Online)
                {
                    case true:
                        //Se o usuário confirmar, procura o pagamento no dataset pela sua ID descrita na seleção e o remove
                        try
                        {
                            pagamento = mainDataSet.Pagamento.FindByID((int)ReferenceComb1.SelectedValue);
                            ConnectionHelper.PagamentoTable.Delete(pagamento.ID, pagamento.Aluno, pagamento.Referencia, pagamento.DataPagamento, pagamento.Valor, pagamento.Rematricula);
                            pagamento.Delete();
                            StatusLabel.Text = "Pagamento removido com sucesso!";
                        }

                        //Caso ocorra algum erro, informa ao usuário
                        catch (SqlException message)
                        {
                            statusStrip1.BackColor = Color.Red;
                            StatusLabel.Text = "Erro ao Atualizar Tabela!";
                            if (MessageBox.Show("Não foi possível remover dados: " + message.Message) == DialogResult.OK)
                            {
                                statusStrip1.BackColor = Color.White;
                                StatusLabel.Text = "Ultima atualização não realizada!";
                            }

                            if (message.ErrorCode == -2146232060)
                            {
                                if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    InputForm IP = new InputForm();
                                    if (IP.ShowDialog() == DialogResult.OK)
                                    {
                                        ConnectionHelper.GoOfflineMode();
                                        OfflinePagamento pagamentoRemovido = new OfflinePagamento(pagamento, ComandType.Delete);
                                        ConnectionHelper.OfflineData.OfflinePagamentoDataTable.Add(pagamentoRemovido);
                                        ConnectionHelper.SaveOfflineData();

                                        StatusLabel.Text = "Dados salvos com sucesso!";
                                        ClearControls();
                                    }
                                }
                            }
                        }
                        catch (NullReferenceException) { MessageBox.Show("Não ha registros para serem removidos!"); }

                        catch (Exception err)
                        {
                            MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                        }
                        break;

                    case false:
                        MessageBox.Show("Não é possível editar no modo offline!");
                        break;
                }
            }
        }

        //Botão Cancelar
        private void CancelBtn_Click(object sender, EventArgs e)
        {
            //Confirma com o usuário se ele realmente deseja cancelar as alterações feitas
            if (MessageBox.Show("Se você cancelar, os dados que não foram atualizados serão perdidos, deseja prosseguir?", "Cancelar operação", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                //Limpa os controles, desativa os controles de edição e ativa o painel de seleção
                ClearControls();
                panel1.Enabled = false;
                panel2.Enabled = false;
                groupBox1.Enabled = true;

                StatusLabel.Text = "Operação Cancelada!";
            }
        }
        #endregion

        /// <summary>
        /// Carrega as informações nos controles de edição
        /// </summary>
        /// <param name="pagamento">Informações de registro de pagamento a serem editadas</param>
        void LoadInformations(MainDataSet.PagamentoRow pagamento)
        {
            //Carrega os dados nos controles
            NomeComb2.SelectedValue = pagamento.AlunoRow.ID;
            NomeComb2.SelectedItem = pagamento.AlunoRow.Nome;

            ValorBox.Text = pagamento.Valor.ToString();
            PayDate.Value = pagamento.DataPagamento;
            ReferenceDate.Value = pagamento.Referencia;
            RemCheck2.Checked = RemCheck1.Checked;
        }

        /// <summary>
        /// Limpa os controles de edição
        /// </summary>
        void ClearControls()
        {
            ValorBox.Clear();
            PayDate.Value = DateTime.Today;
            ReferenceDate.Value = DateTime.Today;
        }

        private void RemCheck2_CheckedChanged(object sender, EventArgs e)
        {
            ReferenceDate.Format = (RemCheck2.Checked) ? DateTimePickerFormat.Custom : DateTimePickerFormat.Short;
        }

        MainDataSet.PagamentoRow ValidarControles(MainDataSet.PagamentoRow pagamento)
        {
            try { pagamento.Aluno = (Guid)NomeComb2.SelectedValue; controlesValidos = true; }
            catch { controlesValidos = false; throw new InvalidControlValue(); }

            if (ValorBox.Text.Length > 0)
            {
                if (ValorBox.Text == "" || ValorBox.Text == " ")
                {
                    controlesValidos = false;
                    throw new InvalidControlValue();
                }
                else
                {
                    try
                    { pagamento.Valor = decimal.Parse(ValorBox.Text); controlesValidos = true; }
                    catch
                    {
                        controlesValidos = false;
                        throw new InvalidControlValue();
                    }
                }
            }
            else { controlesValidos = false; throw new InvalidControlValue(); }

            pagamento.Rematricula = RemCheck2.Checked;

            pagamento.DataPagamento = PayDate.Value;
            pagamento.Referencia = ReferenceDate.Value;

            return pagamento;
        }
    }
}
