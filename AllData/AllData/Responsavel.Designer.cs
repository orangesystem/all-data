﻿namespace AllData
{
    partial class Responsavel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NameBox3 = new System.Windows.Forms.TextBox();
            this.CPFBox1 = new System.Windows.Forms.MaskedTextBox();
            this.Tel1Box2 = new System.Windows.Forms.MaskedTextBox();
            this.Tel2Box2 = new System.Windows.Forms.MaskedTextBox();
            this.Tel3Box2 = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CommitBtn3 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.Birthdate1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabelResp = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // NameBox3
            // 
            this.NameBox3.Location = new System.Drawing.Point(136, 19);
            this.NameBox3.Name = "NameBox3";
            this.NameBox3.Size = new System.Drawing.Size(217, 20);
            this.NameBox3.TabIndex = 0;
            // 
            // CPFBox1
            // 
            this.CPFBox1.Location = new System.Drawing.Point(136, 71);
            this.CPFBox1.Mask = "000,000,000-00";
            this.CPFBox1.Name = "CPFBox1";
            this.CPFBox1.PromptChar = ' ';
            this.CPFBox1.Size = new System.Drawing.Size(97, 20);
            this.CPFBox1.TabIndex = 1;
            // 
            // Tel1Box2
            // 
            this.Tel1Box2.Location = new System.Drawing.Point(139, 19);
            this.Tel1Box2.Mask = "(99) 9 0000-0000";
            this.Tel1Box2.Name = "Tel1Box2";
            this.Tel1Box2.PromptChar = ' ';
            this.Tel1Box2.Size = new System.Drawing.Size(94, 20);
            this.Tel1Box2.TabIndex = 2;
            // 
            // Tel2Box2
            // 
            this.Tel2Box2.Location = new System.Drawing.Point(139, 45);
            this.Tel2Box2.Mask = "(99) 9 0000-0000";
            this.Tel2Box2.Name = "Tel2Box2";
            this.Tel2Box2.PromptChar = ' ';
            this.Tel2Box2.Size = new System.Drawing.Size(94, 20);
            this.Tel2Box2.TabIndex = 3;
            // 
            // Tel3Box2
            // 
            this.Tel3Box2.Location = new System.Drawing.Point(139, 71);
            this.Tel3Box2.Mask = "(99) 9 0000-0000";
            this.Tel3Box2.Name = "Tel3Box2";
            this.Tel3Box2.PromptChar = ' ';
            this.Tel3Box2.Size = new System.Drawing.Size(94, 20);
            this.Tel3Box2.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Nome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "CPF";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Telefone 1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Telefone 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Telefone 3";
            // 
            // CommitBtn3
            // 
            this.CommitBtn3.Location = new System.Drawing.Point(318, 3);
            this.CommitBtn3.Name = "CommitBtn3";
            this.CommitBtn3.Size = new System.Drawing.Size(75, 23);
            this.CommitBtn3.TabIndex = 10;
            this.CommitBtn3.Text = "Confirmar";
            this.CommitBtn3.UseVisualStyleBackColor = true;
            this.CommitBtn3.Click += new System.EventHandler(this.CommitBtn3_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Nascimento";
            // 
            // Birthdate1
            // 
            this.Birthdate1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Birthdate1.Location = new System.Drawing.Point(136, 45);
            this.Birthdate1.Name = "Birthdate1";
            this.Birthdate1.ShowCheckBox = true;
            this.Birthdate1.Size = new System.Drawing.Size(217, 20);
            this.Birthdate1.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.NameBox3);
            this.groupBox1.Controls.Add(this.Birthdate1);
            this.groupBox1.Controls.Add(this.CPFBox1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 112);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informações";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabelResp});
            this.statusStrip1.Location = new System.Drawing.Point(0, 272);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(396, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabelResp
            // 
            this.StatusLabelResp.Name = "StatusLabelResp";
            this.StatusLabelResp.Size = new System.Drawing.Size(39, 17);
            this.StatusLabelResp.Text = "Status";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CancelBtn);
            this.panel1.Controls.Add(this.CommitBtn3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 236);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(396, 36);
            this.panel1.TabIndex = 15;
            // 
            // CancelBtn
            // 
            this.CancelBtn.Location = new System.Drawing.Point(237, 3);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 23);
            this.CancelBtn.TabIndex = 11;
            this.CancelBtn.Text = "Cancelar";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Tel1Box2);
            this.groupBox2.Controls.Add(this.Tel2Box2);
            this.groupBox2.Controls.Add(this.Tel3Box2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(12, 130);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(381, 100);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Contato";
            // 
            // Responsavel
            // 
            this.AcceptButton = this.CommitBtn3;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 294);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Responsavel";
            this.Text = "Responsável";
            this.Load += new System.EventHandler(this.Responsavel_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NameBox3;
        private System.Windows.Forms.MaskedTextBox CPFBox1;
        private System.Windows.Forms.MaskedTextBox Tel1Box2;
        private System.Windows.Forms.MaskedTextBox Tel2Box2;
        private System.Windows.Forms.MaskedTextBox Tel3Box2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button CommitBtn3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker Birthdate1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabelResp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}