﻿namespace AllData
{
    partial class AniversariantesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AniversariantesForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TodayRB = new System.Windows.Forms.RadioButton();
            this.ThisWeekRB = new System.Windows.Forms.RadioButton();
            this.ThisMonthRB = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(759, 85);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TodayRB);
            this.groupBox1.Controls.Add(this.ThisWeekRB);
            this.groupBox1.Controls.Add(this.ThisMonthRB);
            this.groupBox1.Location = new System.Drawing.Point(236, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 76);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // TodayRB
            // 
            this.TodayRB.AutoSize = true;
            this.TodayRB.Location = new System.Drawing.Point(6, 47);
            this.TodayRB.Name = "TodayRB";
            this.TodayRB.Size = new System.Drawing.Size(103, 17);
            this.TodayRB.TabIndex = 2;
            this.TodayRB.TabStop = true;
            this.TodayRB.Text = "Hoje ou amanhã";
            this.TodayRB.UseVisualStyleBackColor = true;
            this.TodayRB.CheckedChanged += new System.EventHandler(this.TodayRB_CheckedChanged);
            // 
            // ThisWeekRB
            // 
            this.ThisWeekRB.AutoSize = true;
            this.ThisWeekRB.Location = new System.Drawing.Point(6, 29);
            this.ThisWeekRB.Name = "ThisWeekRB";
            this.ThisWeekRB.Size = new System.Drawing.Size(93, 17);
            this.ThisWeekRB.TabIndex = 1;
            this.ThisWeekRB.TabStop = true;
            this.ThisWeekRB.Text = "Nesta semana";
            this.ThisWeekRB.UseVisualStyleBackColor = true;
            this.ThisWeekRB.CheckedChanged += new System.EventHandler(this.ThisWeekRB_CheckedChanged);
            // 
            // ThisMonthRB
            // 
            this.ThisMonthRB.AutoSize = true;
            this.ThisMonthRB.Location = new System.Drawing.Point(6, 13);
            this.ThisMonthRB.Name = "ThisMonthRB";
            this.ThisMonthRB.Size = new System.Drawing.Size(75, 17);
            this.ThisMonthRB.TabIndex = 0;
            this.ThisMonthRB.TabStop = true;
            this.ThisMonthRB.Text = "Neste mês";
            this.ThisMonthRB.UseVisualStyleBackColor = true;
            this.ThisMonthRB.CheckedChanged += new System.EventHandler(this.ThisMonthRB_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tabela";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Alunos",
            "Responsáveis"});
            this.comboBox1.Location = new System.Drawing.Point(109, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 85);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(759, 295);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(759, 295);
            this.dataGridView1.TabIndex = 0;
            // 
            // AniversariantesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 380);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AniversariantesForm";
            this.Text = "Lista de Aniversariantes";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton TodayRB;
        private System.Windows.Forms.RadioButton ThisWeekRB;
        private System.Windows.Forms.RadioButton ThisMonthRB;
    }
}