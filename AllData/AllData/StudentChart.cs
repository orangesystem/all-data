﻿using System;
using System.Windows.Forms;

namespace AllData
{
    public partial class StudentChart : Form
    {
        public StudentChart()
        {
            InitializeComponent();
        }

        private void StudentChart_Load(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    ConnectionHelper.PagamentoTable.Fill(this.mainDataSet.Pagamento);
                    ConnectionHelper.ResponsavelTable.Fill(this.mainDataSet.Responsavel);
                    ConnectionHelper.AlunoTable.Fill(mainDataSet.Aluno);

                    try
                    {
                        MainDataSet.AlunoRow aluno = mainDataSet.Aluno.FindByID((Guid)NomeComb.SelectedValue);
                        ResponsavelLabel.Text = aluno.ResponsavelRow.Nome;
                    }
                    catch { MessageBox.Show("Não há registros de estudantes!"); Close(); }

                    try
                    {
                        ConnectionHelper.PontuacaoTable.FillBy(mainDataSet.Pontuacao, (string)ModuloCombBox.SelectedItem);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    try
                    {
                        ConnectionHelper.PagamentoTable.FillBy(this.mainDataSet.Pagamento, ReferenceCalendar.Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;

                case false:
                    break;
            }
        }

        private void ModuloCombBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    try
                    {
                        ConnectionHelper.PontuacaoTable.FillBy(mainDataSet.Pontuacao, (string)ModuloCombBox.SelectedItem);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;


                case false:
                    break;
            }
        }

        private void NomeComb_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    try { MainDataSet.AlunoRow aluno = mainDataSet.Aluno.FindByID((Guid)NomeComb.SelectedValue);
                        ResponsavelLabel.Text = aluno.ResponsavelRow.Nome;
                    }
                    catch { }
                    break;

                case false:
                    break;
            }
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ReferenceCalendar_ValueChanged(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    try
                    {
                        ConnectionHelper.PagamentoTable.FillBy(this.mainDataSet.Pagamento, ReferenceCalendar.Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;

                case false:
                    break;
            }
        }
    }
}
