﻿namespace AllData
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MenuPanel = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.WatchCaixaValue = new System.Windows.Forms.CheckBox();
            this.WatchID = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PauseCaixa = new System.Windows.Forms.Button();
            this.FecharCaixaBtn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.AbrirCaixaBtn = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.alunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoAlunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarInformaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.pagamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoPagamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditarPagamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pontuaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novaPontuaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarPontuaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.fichaDoEstudanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.responsávelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoResponsávelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarInformaçãoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.fichaDoResponsávelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ferramentasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.inserirDadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registroRápidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alunoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.responsavelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pagamentoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pontuaçãoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.dadosDuplicadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaDeAniversariantesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.históricoDoCaixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarSenhaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autosaveDoCaixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.informaçõesDoProgramadorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.NotificationIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.Saver = new System.Windows.Forms.Timer(this.components);
            this.MenuPanel.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuPanel
            // 
            this.MenuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MenuPanel.Controls.Add(this.panel4);
            this.MenuPanel.Controls.Add(this.panel3);
            this.MenuPanel.Controls.Add(this.panel2);
            this.MenuPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuPanel.Location = new System.Drawing.Point(0, 24);
            this.MenuPanel.Name = "MenuPanel";
            this.MenuPanel.Size = new System.Drawing.Size(191, 470);
            this.MenuPanel.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBox1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 75);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(191, 288);
            this.panel4.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.WatchCaixaValue);
            this.groupBox1.Controls.Add(this.WatchID);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Location = new System.Drawing.Point(12, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(173, 78);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opções";
            // 
            // WatchCaixaValue
            // 
            this.WatchCaixaValue.AutoSize = true;
            this.WatchCaixaValue.Location = new System.Drawing.Point(14, 42);
            this.WatchCaixaValue.Name = "WatchCaixaValue";
            this.WatchCaixaValue.Size = new System.Drawing.Size(111, 17);
            this.WatchCaixaValue.TabIndex = 1;
            this.WatchCaixaValue.Text = "Ver valor no caixa";
            this.WatchCaixaValue.UseVisualStyleBackColor = true;
            this.WatchCaixaValue.Visible = false;
            this.WatchCaixaValue.CheckedChanged += new System.EventHandler(this.WatchCaixaValue_CheckedChanged);
            // 
            // WatchID
            // 
            this.WatchID.AutoSize = true;
            this.WatchID.Location = new System.Drawing.Point(14, 19);
            this.WatchID.Name = "WatchID";
            this.WatchID.Size = new System.Drawing.Size(56, 17);
            this.WatchID.TabIndex = 0;
            this.WatchID.Text = "Ver ID";
            this.WatchID.UseVisualStyleBackColor = true;
            this.WatchID.Visible = false;
            this.WatchID.CheckedChanged += new System.EventHandler(this.WatchID_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.PauseCaixa);
            this.panel3.Controls.Add(this.FecharCaixaBtn);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 363);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(191, 107);
            this.panel3.TabIndex = 3;
            // 
            // PauseCaixa
            // 
            this.PauseCaixa.BackColor = System.Drawing.Color.Transparent;
            this.PauseCaixa.Enabled = false;
            this.PauseCaixa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PauseCaixa.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.PauseCaixa.Location = new System.Drawing.Point(26, 16);
            this.PauseCaixa.Name = "PauseCaixa";
            this.PauseCaixa.Size = new System.Drawing.Size(127, 25);
            this.PauseCaixa.TabIndex = 2;
            this.PauseCaixa.Text = "Fechar Caixa";
            this.PauseCaixa.UseVisualStyleBackColor = false;
            this.PauseCaixa.Click += new System.EventHandler(this.PauseCaixa_Click);
            // 
            // FecharCaixaBtn
            // 
            this.FecharCaixaBtn.BackColor = System.Drawing.Color.Transparent;
            this.FecharCaixaBtn.Enabled = false;
            this.FecharCaixaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FecharCaixaBtn.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.FecharCaixaBtn.Location = new System.Drawing.Point(26, 63);
            this.FecharCaixaBtn.Name = "FecharCaixaBtn";
            this.FecharCaixaBtn.Size = new System.Drawing.Size(127, 25);
            this.FecharCaixaBtn.TabIndex = 1;
            this.FecharCaixaBtn.Text = "Sincronizar e Fechar";
            this.FecharCaixaBtn.UseVisualStyleBackColor = false;
            this.FecharCaixaBtn.Click += new System.EventHandler(this.FecharCaixaBtn_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.AbrirCaixaBtn);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(191, 75);
            this.panel2.TabIndex = 2;
            // 
            // AbrirCaixaBtn
            // 
            this.AbrirCaixaBtn.BackColor = System.Drawing.Color.Transparent;
            this.AbrirCaixaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AbrirCaixaBtn.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.AbrirCaixaBtn.Location = new System.Drawing.Point(26, 44);
            this.AbrirCaixaBtn.Name = "AbrirCaixaBtn";
            this.AbrirCaixaBtn.Size = new System.Drawing.Size(127, 25);
            this.AbrirCaixaBtn.TabIndex = 0;
            this.AbrirCaixaBtn.Text = "Abrir Caixa";
            this.AbrirCaixaBtn.UseVisualStyleBackColor = false;
            this.AbrirCaixaBtn.Click += new System.EventHandler(this.AbrirCaixaBtn_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alunoToolStripMenuItem,
            this.responsávelToolStripMenuItem,
            this.ferramentasToolStripMenuItem,
            this.opçõesToolStripMenuItem,
            this.ajudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(915, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // alunoToolStripMenuItem
            // 
            this.alunoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoAlunoToolStripMenuItem,
            this.editarInformaçãoToolStripMenuItem,
            this.toolStripSeparator1,
            this.pagamentoToolStripMenuItem,
            this.pontuaçãoToolStripMenuItem,
            this.toolStripSeparator2,
            this.fichaDoEstudanteToolStripMenuItem});
            this.alunoToolStripMenuItem.Name = "alunoToolStripMenuItem";
            this.alunoToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.alunoToolStripMenuItem.Text = "Aluno";
            // 
            // novoAlunoToolStripMenuItem
            // 
            this.novoAlunoToolStripMenuItem.Name = "novoAlunoToolStripMenuItem";
            this.novoAlunoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.novoAlunoToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.novoAlunoToolStripMenuItem.Text = "Novo Aluno";
            this.novoAlunoToolStripMenuItem.Click += new System.EventHandler(this.novoAlunoToolStripMenuItem_Click);
            // 
            // editarInformaçãoToolStripMenuItem
            // 
            this.editarInformaçãoToolStripMenuItem.Name = "editarInformaçãoToolStripMenuItem";
            this.editarInformaçãoToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.editarInformaçãoToolStripMenuItem.Text = "Editar Informação";
            this.editarInformaçãoToolStripMenuItem.Click += new System.EventHandler(this.editarInformaçãoToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(211, 6);
            // 
            // pagamentoToolStripMenuItem
            // 
            this.pagamentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoPagamentoToolStripMenuItem,
            this.EditarPagamentoToolStripMenuItem});
            this.pagamentoToolStripMenuItem.Name = "pagamentoToolStripMenuItem";
            this.pagamentoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.pagamentoToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.pagamentoToolStripMenuItem.Text = "Pagamento";
            this.pagamentoToolStripMenuItem.Click += new System.EventHandler(this.pagamentoToolStripMenuItem_Click);
            // 
            // novoPagamentoToolStripMenuItem
            // 
            this.novoPagamentoToolStripMenuItem.Name = "novoPagamentoToolStripMenuItem";
            this.novoPagamentoToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.novoPagamentoToolStripMenuItem.Text = "Novo Pagamento";
            this.novoPagamentoToolStripMenuItem.Click += new System.EventHandler(this.pagamentoToolStripMenuItem_Click);
            // 
            // EditarPagamentoToolStripMenuItem
            // 
            this.EditarPagamentoToolStripMenuItem.Name = "EditarPagamentoToolStripMenuItem";
            this.EditarPagamentoToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.EditarPagamentoToolStripMenuItem.Text = "Editar Pagamento";
            this.EditarPagamentoToolStripMenuItem.Click += new System.EventHandler(this.EditarPagamentoToolStripMenuItem_Click);
            // 
            // pontuaçãoToolStripMenuItem
            // 
            this.pontuaçãoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaPontuaçãoToolStripMenuItem,
            this.editarPontuaçãoToolStripMenuItem});
            this.pontuaçãoToolStripMenuItem.Name = "pontuaçãoToolStripMenuItem";
            this.pontuaçãoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.P)));
            this.pontuaçãoToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.pontuaçãoToolStripMenuItem.Text = "Pontuação";
            this.pontuaçãoToolStripMenuItem.Click += new System.EventHandler(this.pontuaçãoToolStripMenuItem_Click);
            // 
            // novaPontuaçãoToolStripMenuItem
            // 
            this.novaPontuaçãoToolStripMenuItem.Name = "novaPontuaçãoToolStripMenuItem";
            this.novaPontuaçãoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.novaPontuaçãoToolStripMenuItem.Text = "Nova Pontuação";
            this.novaPontuaçãoToolStripMenuItem.Click += new System.EventHandler(this.pontuaçãoToolStripMenuItem_Click);
            // 
            // editarPontuaçãoToolStripMenuItem
            // 
            this.editarPontuaçãoToolStripMenuItem.Name = "editarPontuaçãoToolStripMenuItem";
            this.editarPontuaçãoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.editarPontuaçãoToolStripMenuItem.Text = "Editar Pontuação";
            this.editarPontuaçãoToolStripMenuItem.Click += new System.EventHandler(this.editarPontuaçãoToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(211, 6);
            // 
            // fichaDoEstudanteToolStripMenuItem
            // 
            this.fichaDoEstudanteToolStripMenuItem.Name = "fichaDoEstudanteToolStripMenuItem";
            this.fichaDoEstudanteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.fichaDoEstudanteToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.fichaDoEstudanteToolStripMenuItem.Text = "Ficha do Estudante";
            this.fichaDoEstudanteToolStripMenuItem.Click += new System.EventHandler(this.fichaDoEstudanteToolStripMenuItem_Click);
            // 
            // responsávelToolStripMenuItem
            // 
            this.responsávelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoResponsávelToolStripMenuItem,
            this.editarInformaçãoToolStripMenuItem1,
            this.toolStripSeparator3,
            this.fichaDoResponsávelToolStripMenuItem});
            this.responsávelToolStripMenuItem.Name = "responsávelToolStripMenuItem";
            this.responsávelToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.responsávelToolStripMenuItem.Text = "Responsável";
            // 
            // novoResponsávelToolStripMenuItem
            // 
            this.novoResponsávelToolStripMenuItem.Name = "novoResponsávelToolStripMenuItem";
            this.novoResponsávelToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.novoResponsávelToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.novoResponsávelToolStripMenuItem.Text = "Novo Responsável";
            this.novoResponsávelToolStripMenuItem.Click += new System.EventHandler(this.novoResponsávelToolStripMenuItem_Click);
            // 
            // editarInformaçãoToolStripMenuItem1
            // 
            this.editarInformaçãoToolStripMenuItem1.Name = "editarInformaçãoToolStripMenuItem1";
            this.editarInformaçãoToolStripMenuItem1.Size = new System.Drawing.Size(250, 22);
            this.editarInformaçãoToolStripMenuItem1.Text = "Editar Informação";
            this.editarInformaçãoToolStripMenuItem1.Click += new System.EventHandler(this.editarInformaçãoToolStripMenuItem1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(247, 6);
            // 
            // fichaDoResponsávelToolStripMenuItem
            // 
            this.fichaDoResponsávelToolStripMenuItem.Name = "fichaDoResponsávelToolStripMenuItem";
            this.fichaDoResponsávelToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.F)));
            this.fichaDoResponsávelToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.fichaDoResponsávelToolStripMenuItem.Text = "Ficha do Responsável";
            this.fichaDoResponsávelToolStripMenuItem.Click += new System.EventHandler(this.fichaDoResponsávelToolStripMenuItem_Click);
            // 
            // ferramentasToolStripMenuItem
            // 
            this.ferramentasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buscarToolStripMenuItem2,
            this.inserirDadosToolStripMenuItem,
            this.registroRápidoToolStripMenuItem,
            this.toolStripMenuItem1,
            this.dadosDuplicadosToolStripMenuItem,
            this.listaDeAniversariantesToolStripMenuItem,
            this.históricoDoCaixaToolStripMenuItem});
            this.ferramentasToolStripMenuItem.Name = "ferramentasToolStripMenuItem";
            this.ferramentasToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.ferramentasToolStripMenuItem.Text = "Ferramentas";
            // 
            // buscarToolStripMenuItem2
            // 
            this.buscarToolStripMenuItem2.Enabled = false;
            this.buscarToolStripMenuItem2.Name = "buscarToolStripMenuItem2";
            this.buscarToolStripMenuItem2.Size = new System.Drawing.Size(258, 22);
            this.buscarToolStripMenuItem2.Text = "Buscar";
            // 
            // inserirDadosToolStripMenuItem
            // 
            this.inserirDadosToolStripMenuItem.Enabled = false;
            this.inserirDadosToolStripMenuItem.Name = "inserirDadosToolStripMenuItem";
            this.inserirDadosToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.inserirDadosToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.inserirDadosToolStripMenuItem.Text = "Adaptador de Dados (Beta)";
            this.inserirDadosToolStripMenuItem.Click += new System.EventHandler(this.inserirDadosToolStripMenuItem_Click);
            // 
            // registroRápidoToolStripMenuItem
            // 
            this.registroRápidoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alunoToolStripMenuItem1,
            this.responsavelToolStripMenuItem,
            this.pagamentoToolStripMenuItem1,
            this.pontuaçãoToolStripMenuItem1});
            this.registroRápidoToolStripMenuItem.Enabled = false;
            this.registroRápidoToolStripMenuItem.Name = "registroRápidoToolStripMenuItem";
            this.registroRápidoToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.registroRápidoToolStripMenuItem.Text = "Registro Rápido";
            // 
            // alunoToolStripMenuItem1
            // 
            this.alunoToolStripMenuItem1.Name = "alunoToolStripMenuItem1";
            this.alunoToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.alunoToolStripMenuItem1.Text = "Aluno";
            // 
            // responsavelToolStripMenuItem
            // 
            this.responsavelToolStripMenuItem.Name = "responsavelToolStripMenuItem";
            this.responsavelToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.responsavelToolStripMenuItem.Text = "Responsável";
            // 
            // pagamentoToolStripMenuItem1
            // 
            this.pagamentoToolStripMenuItem1.Name = "pagamentoToolStripMenuItem1";
            this.pagamentoToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.pagamentoToolStripMenuItem1.Text = "Pagamento";
            // 
            // pontuaçãoToolStripMenuItem1
            // 
            this.pontuaçãoToolStripMenuItem1.Name = "pontuaçãoToolStripMenuItem1";
            this.pontuaçãoToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.pontuaçãoToolStripMenuItem1.Text = "Pontuação";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(255, 6);
            // 
            // dadosDuplicadosToolStripMenuItem
            // 
            this.dadosDuplicadosToolStripMenuItem.Name = "dadosDuplicadosToolStripMenuItem";
            this.dadosDuplicadosToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.dadosDuplicadosToolStripMenuItem.Text = "Dados Duplicados";
            this.dadosDuplicadosToolStripMenuItem.Click += new System.EventHandler(this.dadosDuplicadosToolStripMenuItem_Click);
            // 
            // listaDeAniversariantesToolStripMenuItem
            // 
            this.listaDeAniversariantesToolStripMenuItem.Name = "listaDeAniversariantesToolStripMenuItem";
            this.listaDeAniversariantesToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.listaDeAniversariantesToolStripMenuItem.Text = "Lista de Aniversariantes";
            this.listaDeAniversariantesToolStripMenuItem.Click += new System.EventHandler(this.listaDeAniversariantesToolStripMenuItem_Click);
            // 
            // históricoDoCaixaToolStripMenuItem
            // 
            this.históricoDoCaixaToolStripMenuItem.Name = "históricoDoCaixaToolStripMenuItem";
            this.históricoDoCaixaToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.históricoDoCaixaToolStripMenuItem.Text = "Histórico do Caixa";
            // 
            // opçõesToolStripMenuItem
            // 
            this.opçõesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alterarSenhaToolStripMenuItem,
            this.autosaveDoCaixaToolStripMenuItem});
            this.opçõesToolStripMenuItem.Name = "opçõesToolStripMenuItem";
            this.opçõesToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.opçõesToolStripMenuItem.Text = "Opções";
            // 
            // alterarSenhaToolStripMenuItem
            // 
            this.alterarSenhaToolStripMenuItem.Name = "alterarSenhaToolStripMenuItem";
            this.alterarSenhaToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.alterarSenhaToolStripMenuItem.Text = "Alterar Senha";
            this.alterarSenhaToolStripMenuItem.Click += new System.EventHandler(this.alterarSenhaToolStripMenuItem_Click);
            // 
            // autosaveDoCaixaToolStripMenuItem
            // 
            this.autosaveDoCaixaToolStripMenuItem.Name = "autosaveDoCaixaToolStripMenuItem";
            this.autosaveDoCaixaToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.autosaveDoCaixaToolStripMenuItem.Text = "Autosave do Caixa";
            this.autosaveDoCaixaToolStripMenuItem.Click += new System.EventHandler(this.autosaveDoCaixaToolStripMenuItem_Click);
            // 
            // ajudaToolStripMenuItem
            // 
            this.ajudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sobreToolStripMenuItem,
            this.toolStripSeparator4,
            this.informaçõesDoProgramadorToolStripMenuItem});
            this.ajudaToolStripMenuItem.Name = "ajudaToolStripMenuItem";
            this.ajudaToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.ajudaToolStripMenuItem.Text = "Ajuda";
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F4)));
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.sobreToolStripMenuItem.Text = "Sobre";
            this.sobreToolStripMenuItem.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(158, 6);
            // 
            // informaçõesDoProgramadorToolStripMenuItem
            // 
            this.informaçõesDoProgramadorToolStripMenuItem.Name = "informaçõesDoProgramadorToolStripMenuItem";
            this.informaçõesDoProgramadorToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this.informaçõesDoProgramadorToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.informaçõesDoProgramadorToolStripMenuItem.Text = "Suporte";
            this.informaçõesDoProgramadorToolStripMenuItem.Click += new System.EventHandler(this.informaçõesDoProgramadorToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SandyBrown;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(191, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(724, 470);
            this.panel1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::AllData.Images.PNG_Logo_Eagle;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(724, 470);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 494);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(915, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel2
            // 
            this.StatusLabel2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusLabel2.ForeColor = System.Drawing.Color.White;
            this.StatusLabel2.Name = "StatusLabel2";
            this.StatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // NotificationIcon
            // 
            this.NotificationIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.NotificationIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("NotificationIcon.Icon")));
            this.NotificationIcon.Text = "Orange System";
            this.NotificationIcon.Visible = true;
            this.NotificationIcon.BalloonTipClicked += new System.EventHandler(this.NotificationIcon_BalloonTipClicked);
            // 
            // Saver
            // 
            this.Saver.Tick += new System.EventHandler(this.Saver_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 516);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.MenuPanel);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "All Data - Orange System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Enter += new System.EventHandler(this.MainForm_Enter);
            this.MenuPanel.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel MenuPanel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoAlunoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarInformaçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem pagamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pontuaçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem responsávelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoResponsávelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarInformaçãoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem ferramentasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem inserirDadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem informaçõesDoProgramadorToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem novoPagamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EditarPagamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novaPontuaçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarPontuaçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registroRápidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem responsavelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pagamentoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pontuaçãoToolStripMenuItem1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem fichaDoEstudanteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fichaDoResponsávelToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dadosDuplicadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaDeAniversariantesToolStripMenuItem;
        public System.Windows.Forms.NotifyIcon NotificationIcon;
        private System.Windows.Forms.Button AbrirCaixaBtn;
        private System.Windows.Forms.ToolStripMenuItem opçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarSenhaToolStripMenuItem;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button FecharCaixaBtn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox WatchCaixaValue;
        private System.Windows.Forms.CheckBox WatchID;
        private System.Windows.Forms.ToolStripMenuItem históricoDoCaixaToolStripMenuItem;
        private System.Windows.Forms.Button PauseCaixa;
        private System.Windows.Forms.Timer Saver;
        private System.Windows.Forms.ToolStripMenuItem autosaveDoCaixaToolStripMenuItem;
    }
}

