﻿namespace AllData
{
    partial class LivroCaixa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tipoPagamentoColunaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.alunoIDColunaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alunoNomeColunaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.alunoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSet = new AllData.MainDataSet();
            this.valorColunaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referenciaColunaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rematriculaColunaDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.observacoesColunaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sincronizado = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.caixaComandosRowBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.alunoTableAdapter = new AllData.MainDataSetTableAdapters.AlunoTableAdapter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CaixaValor = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.caixaComandosRowBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(798, 422);
            this.panel1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tipoPagamentoColunaDataGridViewTextBoxColumn,
            this.alunoIDColunaDataGridViewTextBoxColumn,
            this.alunoNomeColunaDataGridViewTextBoxColumn,
            this.valorColunaDataGridViewTextBoxColumn,
            this.referenciaColunaDataGridViewTextBoxColumn,
            this.rematriculaColunaDataGridViewCheckBoxColumn,
            this.observacoesColunaDataGridViewTextBoxColumn,
            this.Sincronizado});
            this.dataGridView1.DataSource = this.caixaComandosRowBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(798, 422);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // tipoPagamentoColunaDataGridViewTextBoxColumn
            // 
            this.tipoPagamentoColunaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tipoPagamentoColunaDataGridViewTextBoxColumn.DataPropertyName = "TipoPagamentoColuna";
            this.tipoPagamentoColunaDataGridViewTextBoxColumn.HeaderText = "Tipo de Pagamento";
            this.tipoPagamentoColunaDataGridViewTextBoxColumn.Items.AddRange(new object[] {
            "Pagamento",
            "Compra",
            "Troco",
            "Outro"});
            this.tipoPagamentoColunaDataGridViewTextBoxColumn.Name = "tipoPagamentoColunaDataGridViewTextBoxColumn";
            this.tipoPagamentoColunaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tipoPagamentoColunaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // alunoIDColunaDataGridViewTextBoxColumn
            // 
            this.alunoIDColunaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.alunoIDColunaDataGridViewTextBoxColumn.DataPropertyName = "AlunoIDColuna";
            this.alunoIDColunaDataGridViewTextBoxColumn.HeaderText = "ID";
            this.alunoIDColunaDataGridViewTextBoxColumn.Name = "alunoIDColunaDataGridViewTextBoxColumn";
            this.alunoIDColunaDataGridViewTextBoxColumn.ReadOnly = true;
            this.alunoIDColunaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.alunoIDColunaDataGridViewTextBoxColumn.Visible = false;
            // 
            // alunoNomeColunaDataGridViewTextBoxColumn
            // 
            this.alunoNomeColunaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.alunoNomeColunaDataGridViewTextBoxColumn.DataPropertyName = "AlunoNomeColuna";
            this.alunoNomeColunaDataGridViewTextBoxColumn.DataSource = this.alunoBindingSource;
            this.alunoNomeColunaDataGridViewTextBoxColumn.DisplayMember = "Nome";
            this.alunoNomeColunaDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.alunoNomeColunaDataGridViewTextBoxColumn.HeaderText = "Aluno";
            this.alunoNomeColunaDataGridViewTextBoxColumn.Name = "alunoNomeColunaDataGridViewTextBoxColumn";
            this.alunoNomeColunaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.alunoNomeColunaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // alunoBindingSource
            // 
            this.alunoBindingSource.DataMember = "Aluno";
            this.alunoBindingSource.DataSource = this.mainDataSet;
            // 
            // mainDataSet
            // 
            this.mainDataSet.DataSetName = "MainDataSet";
            this.mainDataSet.EnforceConstraints = false;
            this.mainDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // valorColunaDataGridViewTextBoxColumn
            // 
            this.valorColunaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.valorColunaDataGridViewTextBoxColumn.DataPropertyName = "ValorColuna";
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = null;
            this.valorColunaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.valorColunaDataGridViewTextBoxColumn.HeaderText = "Valor";
            this.valorColunaDataGridViewTextBoxColumn.Name = "valorColunaDataGridViewTextBoxColumn";
            // 
            // referenciaColunaDataGridViewTextBoxColumn
            // 
            this.referenciaColunaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.referenciaColunaDataGridViewTextBoxColumn.DataPropertyName = "ReferenciaColuna";
            dataGridViewCellStyle2.Format = "d";
            dataGridViewCellStyle2.NullValue = null;
            this.referenciaColunaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.referenciaColunaDataGridViewTextBoxColumn.HeaderText = "Referencia";
            this.referenciaColunaDataGridViewTextBoxColumn.Name = "referenciaColunaDataGridViewTextBoxColumn";
            // 
            // rematriculaColunaDataGridViewCheckBoxColumn
            // 
            this.rematriculaColunaDataGridViewCheckBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rematriculaColunaDataGridViewCheckBoxColumn.DataPropertyName = "RematriculaColuna";
            this.rematriculaColunaDataGridViewCheckBoxColumn.HeaderText = "Rematricula";
            this.rematriculaColunaDataGridViewCheckBoxColumn.Name = "rematriculaColunaDataGridViewCheckBoxColumn";
            // 
            // observacoesColunaDataGridViewTextBoxColumn
            // 
            this.observacoesColunaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.observacoesColunaDataGridViewTextBoxColumn.DataPropertyName = "ObservacoesColuna";
            this.observacoesColunaDataGridViewTextBoxColumn.HeaderText = "Observacoes";
            this.observacoesColunaDataGridViewTextBoxColumn.Name = "observacoesColunaDataGridViewTextBoxColumn";
            // 
            // Sincronizado
            // 
            this.Sincronizado.DataPropertyName = "Sincronizado";
            this.Sincronizado.HeaderText = "Sincronizado";
            this.Sincronizado.Name = "Sincronizado";
            this.Sincronizado.Visible = false;
            // 
            // caixaComandosRowBindingSource
            // 
            this.caixaComandosRowBindingSource.DataSource = typeof(AllData.CaixaComandosRow);
            // 
            // alunoTableAdapter
            // 
            this.alunoTableAdapter.ClearBeforeFill = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.CaixaValor);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 422);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(798, 26);
            this.panel2.TabIndex = 1;
            // 
            // CaixaValor
            // 
            this.CaixaValor.AutoSize = true;
            this.CaixaValor.Location = new System.Drawing.Point(13, 7);
            this.CaixaValor.Name = "CaixaValor";
            this.CaixaValor.Size = new System.Drawing.Size(74, 13);
            this.CaixaValor.TabIndex = 0;
            this.CaixaValor.Text = "Valor no caixa";
            this.CaixaValor.Visible = false;
            // 
            // LivroCaixa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 448);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LivroCaixa";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LivroCaixa_FormClosed);
            this.Load += new System.EventHandler(this.LivroCaixa_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.caixaComandosRowBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewComboBoxColumn alunoColunaDataGridViewTextBoxColumn;
        private MainDataSetTableAdapters.AlunoTableAdapter alunoTableAdapter;
        private System.Windows.Forms.BindingSource alunoBindingSource;
        private MainDataSet mainDataSet;
        private System.Windows.Forms.BindingSource caixaComandosRowBindingSource;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label CaixaValor;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Sincronizado;
        private System.Windows.Forms.DataGridViewTextBoxColumn observacoesColunaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn rematriculaColunaDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn referenciaColunaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorColunaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn alunoNomeColunaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn alunoIDColunaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn tipoPagamentoColunaDataGridViewTextBoxColumn;
    }
}