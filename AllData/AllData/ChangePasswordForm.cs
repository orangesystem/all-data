﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;
using AllData.Properties;
using AllData.Security;

namespace AllData
{
    public partial class ChangePasswordForm : Form
    {
        private string var0x0hb
        {
            get
            {
                RegistryKey k = Registry.CurrentUser.CreateSubKey(Internal.var0x0hb);
                string pass = (string)k.GetValue("var0x0hb");
                if (pass == null)
                {
                    pass = Cryptographer.EncryptString("ee1234567");
                    k.SetValue("var0x0hb", pass);
                }

                return pass;
            }
        }

        private string pass { get
            {
                return Cryptographer.DecryptString(var0x0hb);
            }

            set
            {
                RegistryKey k = Registry.CurrentUser.CreateSubKey(Internal.var0x0hb);
                k.SetValue("var0x0hb", Cryptographer.EncryptString(value));
            }
        }
        public ChangePasswordForm()
        {
            InitializeComponent();
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            if(CurrentPassword.Text == pass)
            {
                if(NewPassword.Text == CheckPassword.Text)
                {
                    pass = NewPassword.Text;
                }

                else
                {
                    MessageBox.Show("As senhas de confirmação não corresponde!");
                    ClearControls();
                }
            }

            else
            {
                MessageBox.Show("Senha incorreta!");
                ClearControls();
            }
        }

        private void ClearControls()
        {
            CurrentPassword.Clear();
            NewPassword.Clear();
            CheckPassword.Clear();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
