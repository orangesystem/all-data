﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AllData
{
    public partial class EditPoints : Form
    {
        bool controlesValidos = false;
        public EditPoints()
        {
            InitializeComponent();
        }

        private void EditPoints_Load(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    ConnectionHelper.PontuacaoTable.Fill(mainDataSet.Pontuacao);
                    ConnectionHelper.AlunoTable.Fill(mainDataSet.Aluno);
                    break;

                case false:
                    MessageBox.Show("Indisponível no modo offline!");
                    Close();
                    break;
            }
        }

        #region Botões
        //Botão Editar
        private void EditBtn_Click(object sender, EventArgs e)
        {
            //Busca as informações selecionadas pelo o usuário através da LessonComb e envia um comando para carregá-las
            //nos controles
            try {
                MainDataSet.PontuacaoRow pontuacao = mainDataSet.Pontuacao.FindByID((int)LessonComb.SelectedValue);
                LoadInformations(pontuacao);
            }
            catch (NullReferenceException) { MessageBox.Show("Não ha registro de pontuação para o aluno escolhido!"); }
        }

        //Botão Confirmar
        private void CommitBtn_Click(object sender, EventArgs e)
        {
            //Confirmar com o usuário se ele realmente deseja alterar os dados
            if (MessageBox.Show("Se você confirmar, os dados serão atualizados, deseja prosseguir?", "Atualizar dados", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                //Pega a linha da tabela no banco de dados
                MainDataSet.PontuacaoRow pontuacao = mainDataSet.Pontuacao.FindByID((int)LessonComb.SelectedValue);
                try { pontuacao = ValidarControles(pontuacao);
                }
                catch { MessageBox.Show("Verifique-se de que os campos estão preenchidos corretamente e a pontuação está em um formato correto!"); }

                if(controlesValidos)
                switch (ConnectionHelper.Online)
                {
                    case true:
                            try //tenta executar as mudanças no banco de dados
                            {
                                DataSet tmpData = mainDataSet.GetChanges();
                                ConnectionHelper.PontuacaoTable.Update(pontuacao);
                                mainDataSet.Merge(tmpData);
                                StatusLabel.Text = "Dados atualizados com sucesso!";
                            }
                            catch (SqlException message) //Caso ocorra algum erro informa ao usuário sobre o acontecido
                            {
                                statusStrip1.BackColor = Color.Red;
                                StatusLabel.Text = "Erro ao Atualizar Tabela!";
                                if (MessageBox.Show("Erro ao atulizar dados: " + message.Message) == DialogResult.OK)
                                {
                                    statusStrip1.BackColor = Color.White;
                                    StatusLabel.Text = "Ultima atualização não realizada!";
                                }

                                if (message.ErrorCode == -2146232060)
                                {
                                    if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                                    {
                                        InputForm IP = new InputForm();
                                        if (IP.ShowDialog() == DialogResult.OK)
                                        {
                                            ConnectionHelper.GoOfflineMode();
                                            OfflinePontuacao pontuacaoOff = new OfflinePontuacao(pontuacao, ComandType.Update);
                                            ConnectionHelper.OfflineData.OfflinePontuacaoDataTable.Add(pontuacaoOff);
                                            ConnectionHelper.SaveOfflineData();

                                            StatusLabel.Text = "Dados salvos com sucesso!";
                                        }
                                    }
                                }
                            }
                            catch (Exception err)
                            {
                                MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                            }
                            finally
                            {
                                //limpa os controles e ativa o cabeçalho para selecionar os dados a serem alterados
                                ClearControls();
                                Header.Enabled = true;
                                Body.Enabled = false;
                                Bottom.Enabled = false;
                            }
                        break;


                    case false:
                        MessageBox.Show("Não é possível editar no modo offline!");
                        break;
            }
            }
        }

        //Botão Deletar
        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            //Confirma com o usuário se ele realmente deseja deletar o dado selecionado
            if (MessageBox.Show("Deseja realmente remover este dado? Se prosseguir, as informações do mesmo podem ser movidas para o banco de arquivos-mortos ",
              "Remover responsável", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                MainDataSet.PontuacaoRow pontuacao = null;

                try
                {
                    //Procura pelo LessonComb a pontuação a ser removida e tenta deletá-la do banco de dados
                    pontuacao = mainDataSet.Pontuacao.FindByID((int)LessonComb.SelectedValue);
                    ConnectionHelper.PontuacaoTable.Delete(pontuacao.ID, pontuacao.Aluno, pontuacao.Modulo, pontuacao.Licao,
                        pontuacao.Data, pontuacao.Resultado);

                    pontuacao.Delete();
                    StatusLabel.Text = "Dado deletado com sucesso!";
                }
                catch (SqlException message)
                {
                    //Caso ocorra algum erro, informa ao usuário sobre o ocorrido
                    statusStrip1.BackColor = Color.Red;
                    StatusLabel.Text = "Erro ao Atualizar Tabela!";
                    if (MessageBox.Show("Não foi possível remover dados: " + message.Message) == DialogResult.OK)
                    {
                        statusStrip1.BackColor = Color.White;
                        StatusLabel.Text = "Ultima atualização não realizada!";
                    }

                    if (message.ErrorCode == -2146232060)
                    {
                        if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            InputForm IP = new InputForm();
                            if (IP.ShowDialog() == DialogResult.OK)
                            {
                                ConnectionHelper.GoOfflineMode();
                                OfflinePontuacao pontuacaoDeletada = new OfflinePontuacao(pontuacao, ComandType.Delete);
                                ConnectionHelper.OfflineData.OfflinePontuacaoDataTable.Add(pontuacaoDeletada);
                                ConnectionHelper.SaveOfflineData();

                                StatusLabel.Text = "Dados salvos com sucesso!";
                                ClearControls();
                            }
                        }
                    }
                }

                catch (NullReferenceException) { MessageBox.Show("Não há registros a serem removidos!"); }
                catch (Exception err)
                {
                    MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                }
            }
        }

        //Botão Cancelar
        private void CancelBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Se você cancelar, os dados que não foram atualizados serão perdidos, deseja prosseguir?", "Cancelar operação", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                ClearControls();
                Header.Enabled = true;
                Body.Enabled = false;
                Bottom.Enabled = false;

                StatusLabel.Text = "Operação Cancelada!";
            }
        }
        #endregion

        /// <summary>
        /// Limpa os controles de edição
        /// </summary>
        void ClearControls()
        {
            LessonBox.Clear();
            PointsBox.Clear();
            TestDate.Value = DateTime.Today;
        }

        /// <summary>
        /// Carrega as informações selecionadas nos controles de edição
        /// </summary>
        /// <param name="pontuacao">Informações a serem editadas</param>
        void LoadInformations(MainDataSet.PontuacaoRow pontuacao)
        {
            LessonBox.Text = pontuacao.Licao;
            PointsBox.Text = pontuacao.Resultado.ToString();
            TestDate.Value = pontuacao.Data;

            Header.Enabled = false;
            Body.Enabled = true;
            Bottom.Enabled = true;
        }

        MainDataSet.PontuacaoRow ValidarControles(MainDataSet.PontuacaoRow pontuacao)
        {
            try
            {
                pontuacao.Aluno = (Guid)NomeComb2.SelectedValue; controlesValidos = true;
            }
            catch
            {
                controlesValidos = false;
                throw new InvalidControlValue();
            }

            if (LessonBox.Text.Length > 0)
            {
                if(LessonBox.Text == "" || LessonBox.Text == " ")
                {
                    controlesValidos = false;
                    throw new InvalidControlValue();
                }
                else
                {
                    pontuacao.Licao = LessonBox.Text;
                    controlesValidos = true;
                }
            }
            else
            {
                controlesValidos = false;
                throw new InvalidControlValue();
            }

            if (PointsBox.Text.Length > 0)
            {
                if (PointsBox.Text == "" || PointsBox.Text == " ")
                {
                    controlesValidos = false;
                    throw new InvalidControlValue();
                }
                else
                {
                    try { pontuacao.Resultado = decimal.Parse(PointsBox.Text);
                        controlesValidos = true;
                    }
                    catch
                    {
                        throw new InvalidControlValue();
                    }
                }
            }
            else
            {
                controlesValidos = false;
                throw new InvalidControlValue();
            }

            if(TestDate.Checked)
            { pontuacao.Data = TestDate.Value; }
            else { pontuacao.Data = new DateTime(); }


            return pontuacao;
        }
    }
}
