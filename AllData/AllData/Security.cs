﻿using System;
using System.Text;
using System.Security.Cryptography;
using AllData.Properties;

namespace AllData.Security
{
    public abstract class Cryptographer
    {
        static public string EncryptString(string stringToEncrypt)
        {
            byte[] resultado;
            UTF8Encoding utfEncoding = new UTF8Encoding();
            MD5CryptoServiceProvider hashProvider = new MD5CryptoServiceProvider();

            byte[] TDESKey = hashProvider.ComputeHash(utfEncoding.GetBytes(Settings.Default.var0x0ck));
            byte[] dataToEncrypt = utfEncoding.GetBytes(stringToEncrypt);

            try
            {
                resultado = hashProvider.TransformFinalBlock(dataToEncrypt, 0, dataToEncrypt.Length);
            }
            finally
            {
                hashProvider.Clear();
            }

            return utfEncoding.GetString(resultado);
        }

        static public string DecryptString(string stringToDecrypt)
        {
            byte[] resultado;
            UTF8Encoding utfEncoding = new UTF8Encoding();
            MD5CryptoServiceProvider hashProvider = new MD5CryptoServiceProvider();

            byte[] TDESKey = hashProvider.ComputeHash(utfEncoding.GetBytes(Settings.Default.var0x0ck));
            TripleDESCryptoServiceProvider TDESAlgoritmo = new TripleDESCryptoServiceProvider();

            TDESAlgoritmo.Key = TDESKey;
            TDESAlgoritmo.Mode = CipherMode.ECB;
            byte[] dataToDecrypt = utfEncoding.GetBytes(stringToDecrypt);

            try
            {
                ICryptoTransform Decryptor = TDESAlgoritmo.CreateDecryptor();
                resultado = Decryptor.TransformFinalBlock(dataToDecrypt, 0, dataToDecrypt.Length);
            }
            finally
            {
                TDESAlgoritmo.Clear();
                hashProvider.Clear();
            }

            return utfEncoding.GetString(resultado);
        }
    }

    public class AllDataCryptoServiceProvider
    {
        public byte[] key { get; set; }
        CipherStyle Cipher { get; set; }
        
        public byte[] TransformBlock(byte[] input, int inputOffset, int inputCount)
        {

        }
    }

    public enum CipherStyle
    {
        /// <summary>
        /// O bloco final contem a mesma quantidade de bytes o input não sendo possível verificar a integridade
        /// </summary>
        Mirrored = 0,

        /// <summary>
        /// O bloco final contém mais bytes que o input sendo possível uma verificação máxima de integridade
        /// </summary>
        Expanded,

        /// <summary>
        /// O bloco final contém menos bytes que o inicial
        /// </summary>
        Compacted,

        /// <summary>
        /// O bloco final contem uma assinatura que garante sua integridade
        /// </summary>
        Signed,
    }
}