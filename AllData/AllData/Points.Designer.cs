﻿namespace AllData
{
    partial class Points
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Points));
            this.ModuleComb1 = new System.Windows.Forms.ComboBox();
            this.LessonBox1 = new System.Windows.Forms.TextBox();
            this.ReferenceDate1 = new System.Windows.Forms.DateTimePicker();
            this.ResultBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CommitBtn3 = new System.Windows.Forms.Button();
            this.AlunoComb2 = new System.Windows.Forms.ComboBox();
            this.alunoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSet = new AllData.MainDataSet();
            this.alunoTableAdapter = new AllData.MainDataSetTableAdapters.AlunoTableAdapter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabelPontuacao = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CancelBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ModuleComb1
            // 
            this.ModuleComb1.FormattingEnabled = true;
            this.ModuleComb1.Items.AddRange(new object[] {
            "KIDS 1",
            "KIDS 2",
            "KIDS 3",
            "KIDS 4",
            "YWC 1",
            "YWC 2",
            "YWC 3",
            "YWC 4",
            "CONVERSATION"});
            this.ModuleComb1.Location = new System.Drawing.Point(58, 46);
            this.ModuleComb1.Name = "ModuleComb1";
            this.ModuleComb1.Size = new System.Drawing.Size(220, 21);
            this.ModuleComb1.TabIndex = 1;
            // 
            // LessonBox1
            // 
            this.LessonBox1.Location = new System.Drawing.Point(58, 73);
            this.LessonBox1.Name = "LessonBox1";
            this.LessonBox1.Size = new System.Drawing.Size(220, 20);
            this.LessonBox1.TabIndex = 2;
            // 
            // ReferenceDate1
            // 
            this.ReferenceDate1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ReferenceDate1.Location = new System.Drawing.Point(58, 19);
            this.ReferenceDate1.Name = "ReferenceDate1";
            this.ReferenceDate1.Size = new System.Drawing.Size(220, 20);
            this.ReferenceDate1.TabIndex = 3;
            // 
            // ResultBox1
            // 
            this.ResultBox1.Location = new System.Drawing.Point(84, 45);
            this.ResultBox1.Name = "ResultBox1";
            this.ResultBox1.Size = new System.Drawing.Size(103, 20);
            this.ResultBox1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Aluno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Modulo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Lição";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Data";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Pontuação";
            // 
            // CommitBtn3
            // 
            this.CommitBtn3.Location = new System.Drawing.Point(312, 3);
            this.CommitBtn3.Name = "CommitBtn3";
            this.CommitBtn3.Size = new System.Drawing.Size(75, 23);
            this.CommitBtn3.TabIndex = 5;
            this.CommitBtn3.Text = "Confirmar";
            this.CommitBtn3.UseVisualStyleBackColor = true;
            this.CommitBtn3.Click += new System.EventHandler(this.CommitBtn3_Click);
            // 
            // AlunoComb2
            // 
            this.AlunoComb2.DataSource = this.alunoBindingSource;
            this.AlunoComb2.DisplayMember = "Nome";
            this.AlunoComb2.FormattingEnabled = true;
            this.AlunoComb2.Location = new System.Drawing.Point(58, 19);
            this.AlunoComb2.Name = "AlunoComb2";
            this.AlunoComb2.Size = new System.Drawing.Size(220, 21);
            this.AlunoComb2.TabIndex = 0;
            this.AlunoComb2.ValueMember = "ID";
            // 
            // alunoBindingSource
            // 
            this.alunoBindingSource.DataMember = "Aluno";
            this.alunoBindingSource.DataSource = this.mainDataSet;
            // 
            // mainDataSet
            // 
            this.mainDataSet.DataSetName = "MainDataSet";
            this.mainDataSet.EnforceConstraints = false;
            this.mainDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // alunoTableAdapter
            // 
            this.alunoTableAdapter.ClearBeforeFill = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.AlunoComb2);
            this.groupBox1.Controls.Add(this.ModuleComb1);
            this.groupBox1.Controls.Add(this.LessonBox1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(375, 104);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informações";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ReferenceDate1);
            this.groupBox2.Controls.Add(this.ResultBox1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 122);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(375, 73);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabelPontuacao});
            this.statusStrip1.Location = new System.Drawing.Point(0, 235);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(390, 22);
            this.statusStrip1.TabIndex = 15;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabelPontuacao
            // 
            this.StatusLabelPontuacao.Name = "StatusLabelPontuacao";
            this.StatusLabelPontuacao.Size = new System.Drawing.Size(39, 17);
            this.StatusLabelPontuacao.Text = "Status";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CancelBtn);
            this.panel1.Controls.Add(this.CommitBtn3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 201);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(390, 34);
            this.panel1.TabIndex = 16;
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Location = new System.Drawing.Point(231, 3);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 23);
            this.CancelBtn.TabIndex = 6;
            this.CancelBtn.Text = "Cancelar";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // Points
            // 
            this.AcceptButton = this.CommitBtn3;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(390, 257);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Points";
            this.Text = "Pontuação";
            this.Load += new System.EventHandler(this.Points_Load);
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox ModuleComb1;
        private System.Windows.Forms.TextBox LessonBox1;
        private System.Windows.Forms.DateTimePicker ReferenceDate1;
        private System.Windows.Forms.TextBox ResultBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button CommitBtn3;
        private System.Windows.Forms.ComboBox AlunoComb2;
        private MainDataSet mainDataSet;
        private System.Windows.Forms.BindingSource alunoBindingSource;
        private MainDataSetTableAdapters.AlunoTableAdapter alunoTableAdapter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabelPontuacao;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CancelBtn;
    }
}