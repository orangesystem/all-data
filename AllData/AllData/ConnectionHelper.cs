﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using AllData.MainDataSetTableAdapters;

namespace AllData
{
    /// <summary>
    /// Auxiliar de conexão com o banco de dados
    /// </summary>
    abstract class ConnectionHelper
    //Utilizar o ConnectionHelper ao invés do TableAdapter padrão por conta das senhas
    // uma vez que pelo ConnectionHelper é possível gerenciar a conta de Logon a ser usada no banco de dados, coisa
    //que pelo TableAdapter padrão não é.
    {
        #region Modificadores

        #region Publicos
        static TableAdapterManager TableManager { get { return _manager; } }
        static public AlunoTableAdapter AlunoTable { get { return _aluno; } }
        static public ResponsavelTableAdapter ResponsavelTable { get { return _responsavel; } }
        static public PagamentoTableAdapter PagamentoTable { get { return _pagamento; } }
        static public PontuacaoTableAdapter PontuacaoTable { get { return _pontuacao; } }
        static public bool Online { get { return _online; } }
        #endregion Publicos

        #region Privados
        static private TableAdapterManager _manager;
        static private AlunoTableAdapter _aluno;
        static private ResponsavelTableAdapter _responsavel;
        static private PagamentoTableAdapter _pagamento;
        static private PontuacaoTableAdapter _pontuacao;
        static private bool _online;
        #endregion Privados

        #endregion Modificadores

        static public OfflineDataSet OfflineData = new OfflineDataSet();

        /// <summary>
        /// Cria a conexão base para a comunicação com o banco de dados
        /// </summary>
        /// <param name="nome">Nome do usuário a se conectar</param>
        /// <param name="senha">Senha do usuário a se conectar</param>
        /// <returns>Status da conexão</returns>
        static public bool Awake(string nome, string senha)
        {
            try {
                //Cria uma nova instância dos modificadores
                _manager = new TableAdapterManager();
                _aluno = new AlunoTableAdapter();
                _responsavel = new ResponsavelTableAdapter();
                _pagamento = new PagamentoTableAdapter();
                _pontuacao = new PontuacaoTableAdapter();

                //Tenta conectá-los ao banco de dados de acordo com as informações de Logon fornecidas na tela
                //de Logon (LogonForm)
                _manager.Connection = new SqlConnection("Data Source=EAGLE;Initial Catalog=Main;User ID=" + nome + ";Password=" + senha);
                _aluno.Connection = new SqlConnection("Data Source=EAGLE;Initial Catalog=Main;User ID=" + nome + ";Password=" + senha);
                _responsavel.Connection = new SqlConnection("Data Source=EAGLE;Initial Catalog=Main;User ID=" + nome + ";Password=" + senha);
                _pagamento.Connection = new SqlConnection("Data Source=EAGLE;Initial Catalog=Main;User ID=" + nome + ";Password=" + senha);
                _pontuacao.Connection = new SqlConnection("Data Source=EAGLE;Initial Catalog=Main;User ID=" + nome + ";Password=" + senha);

                //Abre as conexões
                _manager.Connection.Open();
                _aluno.Connection.Open();
                _responsavel.Connection.Open();
                _pagamento.Connection.Open();
                _pontuacao.Connection.Open();

                //Retorna verdadeiro quando a operação é bem-sucedida
                return _online = true;
            }

            //Caso ocorra algum erro, informa ao usuário sobre o ocorrido
            catch (SqlException message) {
                MessageBox.Show("Não foi possível fazer logon no servidor: " + message.Message);

                if (message.ErrorCode == -2146232060)
                {
                    if (MessageBox.Show("O servidor parece estar desconectado dessa máquina ou inativo, mas ainda assim é " +
                        "possível fazer algumas operações, mesmo com o servidor offline e enviar tais informações para o servidor assim " +
                        "que possível. Deseja usar o software offline?", "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        InputForm IF = new InputForm();
                        if (IF.ShowDialog() == DialogResult.OK)
                        {
                            GoOfflineMode();
                            _online = false;
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Acesso negado: senha incorreta.");
                            return false;
                        }
                    }
                    else { return false; }
                }

                else { return false; }
            }

            catch(Exception err)
            {
                MessageBox.Show(string.Format("Ocorreu um erro inesperado\n {0} {1}", err.Message, Internal.ErroEntreEmContato));
                return false;
            }
        }

        /// <summary>
        /// Cria uma falsa conexão para testes (experimental)
        /// </summary>
        /// <returns>Estado da conexão</returns>
        static public bool PseudoAwake()
        {
            try
            {
                //Cria uma nova instância dos modificadores
                _manager = new TableAdapterManager();
                _aluno = new AlunoTableAdapter();
                _responsavel = new ResponsavelTableAdapter();
                _pagamento = new PagamentoTableAdapter();
                _pontuacao = new PontuacaoTableAdapter();

                //Tenta conectá-los ao banco de dados de acordo com as informações de Logon fornecidas na tela
                //de Logon (LogonForm)
                _manager.Connection = new SqlConnection("Data Source=BRENO-PC;Initial Catalog=Main; User ID=OrangeSystem; Password=pixax790225164");
                _aluno.Connection = new SqlConnection("Data Source=BRENO-PC;Initial Catalog=Main; User ID=OrangeSystem; Password=pixax790225164");
                _responsavel.Connection = new SqlConnection("Data Source=BRENO-PC;Initial Catalog=Main; User ID=OrangeSystem; Password=pixax790225164");
                _pagamento.Connection = new SqlConnection("Data Source=BRENO-PC;Initial Catalog=Main; User ID=OrangeSystem; Password=pixax790225164");
                _pontuacao.Connection = new SqlConnection("Data Source=BRENO-PC;Initial Catalog=Main; User ID=OrangeSystem; Password=pixax790225164");

                //Abre as conexões
                _manager.Connection.Open();
                _aluno.Connection.Open();
                _responsavel.Connection.Open();
                _pagamento.Connection.Open();
                _pontuacao.Connection.Open();

                //Retorna verdadeiro quando a operação é bem-sucedida
                return _online = true;
            }

            //Caso ocorra algum erro, informa ao usuário sobre o ocorrido
            catch (SqlException message) { MessageBox.Show("Não foi possível fazer logon no servidor: " + message.Message); return false; }
        }

        #region offline
        /// <summary>
        /// Altera o sistema para o modo offline
        /// </summary>
        static public void GoOfflineMode()
        {
            _manager = new TableAdapterManager();
            _aluno = new AlunoTableAdapter();
            _responsavel = new ResponsavelTableAdapter();
            _pagamento = new PagamentoTableAdapter();
            _pontuacao = new PontuacaoTableAdapter();

            //Lê algum dado offline previamente salvo
            OfflineData.ReadSavedData();
            _online = false;
        }

        /// <summary>
        /// Salva os dados no disco rígido
        /// </summary>
        static public void SaveOfflineData()
        {
            OfflineData.SaveData();
        }

        /// <summary>
        /// Lê dados salvos offline
        /// </summary>
        static public void CatchOfflineSavedData()
        {
            OfflineData.ReadSavedData();
        }

        /// <summary>
        /// Limpa os dados salvos no disco rígido
        /// </summary>
        static public void ClearOfflineSavedData()
        {
            OfflineData.ClearSavedData();
        }
        #endregion
    }
}