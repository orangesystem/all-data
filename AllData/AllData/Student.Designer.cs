﻿namespace AllData
{
    partial class Student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.NameBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.FindRespBtn1 = new System.Windows.Forms.Button();
            this.BirthdayDate1 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.RegisteringDate1 = new System.Windows.Forms.DateTimePicker();
            this.commitBtn1 = new System.Windows.Forms.Button();
            this.Tel1Box1 = new System.Windows.Forms.MaskedTextBox();
            this.Tel2Box1 = new System.Windows.Forms.MaskedTextBox();
            this.Tel3Box1 = new System.Windows.Forms.MaskedTextBox();
            this.RespComb1 = new System.Windows.Forms.ComboBox();
            this.responsavelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSet = new MainDataSet();
            this.mainDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.alunoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.alunoTableAdapter = new MainDataSetTableAdapters.AlunoTableAdapter();
            this.responsavelTableAdapter = new MainDataSetTableAdapters.ResponsavelTableAdapter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabelStudent = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Cancelbtn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.responsavelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // NameBox1
            // 
            this.NameBox1.Location = new System.Drawing.Point(88, 17);
            this.NameBox1.Name = "NameBox1";
            this.NameBox1.Size = new System.Drawing.Size(145, 20);
            this.NameBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Responsável";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Telefone 1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Telefone 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Telefone 3";
            // 
            // FindRespBtn1
            // 
            this.FindRespBtn1.Location = new System.Drawing.Point(250, 41);
            this.FindRespBtn1.Name = "FindRespBtn1";
            this.FindRespBtn1.Size = new System.Drawing.Size(75, 23);
            this.FindRespBtn1.TabIndex = 10;
            this.FindRespBtn1.Text = "Procurar";
            this.FindRespBtn1.UseVisualStyleBackColor = true;
            // 
            // BirthdayDate1
            // 
            this.BirthdayDate1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.BirthdayDate1.Location = new System.Drawing.Point(16, 38);
            this.BirthdayDate1.Name = "BirthdayDate1";
            this.BirthdayDate1.ShowCheckBox = true;
            this.BirthdayDate1.Size = new System.Drawing.Size(217, 20);
            this.BirthdayDate1.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Nascimento";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Matrícula";
            // 
            // RegisteringDate1
            // 
            this.RegisteringDate1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.RegisteringDate1.Location = new System.Drawing.Point(16, 79);
            this.RegisteringDate1.Name = "RegisteringDate1";
            this.RegisteringDate1.ShowCheckBox = true;
            this.RegisteringDate1.Size = new System.Drawing.Size(217, 20);
            this.RegisteringDate1.TabIndex = 13;
            // 
            // commitBtn1
            // 
            this.commitBtn1.Location = new System.Drawing.Point(430, 4);
            this.commitBtn1.Name = "commitBtn1";
            this.commitBtn1.Size = new System.Drawing.Size(75, 23);
            this.commitBtn1.TabIndex = 15;
            this.commitBtn1.Text = "Confirmar";
            this.commitBtn1.UseVisualStyleBackColor = true;
            this.commitBtn1.Click += new System.EventHandler(this.commitBtn1_Click);
            // 
            // Tel1Box1
            // 
            this.Tel1Box1.Location = new System.Drawing.Point(87, 19);
            this.Tel1Box1.Mask = "(99) 9 0000-0000";
            this.Tel1Box1.Name = "Tel1Box1";
            this.Tel1Box1.PromptChar = ' ';
            this.Tel1Box1.Size = new System.Drawing.Size(145, 20);
            this.Tel1Box1.TabIndex = 16;
            // 
            // Tel2Box1
            // 
            this.Tel2Box1.Location = new System.Drawing.Point(87, 45);
            this.Tel2Box1.Mask = "(99) 9 0000-0000";
            this.Tel2Box1.Name = "Tel2Box1";
            this.Tel2Box1.PromptChar = ' ';
            this.Tel2Box1.Size = new System.Drawing.Size(145, 20);
            this.Tel2Box1.TabIndex = 17;
            // 
            // Tel3Box1
            // 
            this.Tel3Box1.Location = new System.Drawing.Point(87, 71);
            this.Tel3Box1.Mask = "(99) 9 0000-0000";
            this.Tel3Box1.Name = "Tel3Box1";
            this.Tel3Box1.PromptChar = ' ';
            this.Tel3Box1.Size = new System.Drawing.Size(145, 20);
            this.Tel3Box1.TabIndex = 18;
            // 
            // RespComb1
            // 
            this.RespComb1.DataSource = this.responsavelBindingSource;
            this.RespComb1.DisplayMember = "Nome";
            this.RespComb1.FormattingEnabled = true;
            this.RespComb1.Location = new System.Drawing.Point(88, 41);
            this.RespComb1.Name = "RespComb1";
            this.RespComb1.Size = new System.Drawing.Size(145, 21);
            this.RespComb1.TabIndex = 19;
            this.RespComb1.ValueMember = "ID";
            // 
            // responsavelBindingSource
            // 
            this.responsavelBindingSource.DataMember = "Responsavel";
            this.responsavelBindingSource.DataSource = this.mainDataSet;
            // 
            // mainDataSet
            // 
            this.mainDataSet.DataSetName = "MainDataSet";
            this.mainDataSet.EnforceConstraints = false;
            this.mainDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // mainDataSetBindingSource
            // 
            this.mainDataSetBindingSource.DataSource = this.mainDataSet;
            this.mainDataSetBindingSource.Position = 0;
            // 
            // alunoBindingSource
            // 
            this.alunoBindingSource.DataMember = "Aluno";
            this.alunoBindingSource.DataSource = this.mainDataSetBindingSource;
            // 
            // alunoTableAdapter
            // 
            this.alunoTableAdapter.ClearBeforeFill = true;
            // 
            // responsavelTableAdapter
            // 
            this.responsavelTableAdapter.ClearBeforeFill = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.NameBox1);
            this.groupBox1.Controls.Add(this.RespComb1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.FindRespBtn1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(515, 72);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informações";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabelStudent});
            this.statusStrip1.Location = new System.Drawing.Point(0, 226);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(515, 22);
            this.statusStrip1.TabIndex = 21;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabelStudent
            // 
            this.StatusLabelStudent.Name = "StatusLabelStudent";
            this.StatusLabelStudent.Size = new System.Drawing.Size(39, 17);
            this.StatusLabelStudent.Text = "Status";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BirthdayDate1);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.RegisteringDate1);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 72);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(273, 124);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Cancelbtn);
            this.panel1.Controls.Add(this.commitBtn1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 196);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(515, 30);
            this.panel1.TabIndex = 23;
            // 
            // Cancelbtn
            // 
            this.Cancelbtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancelbtn.Location = new System.Drawing.Point(349, 4);
            this.Cancelbtn.Name = "Cancelbtn";
            this.Cancelbtn.Size = new System.Drawing.Size(75, 23);
            this.Cancelbtn.TabIndex = 16;
            this.Cancelbtn.Text = "Cancelar";
            this.Cancelbtn.UseVisualStyleBackColor = true;
            this.Cancelbtn.Click += new System.EventHandler(this.Cancelbtn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Tel1Box1);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.Tel2Box1);
            this.groupBox3.Controls.Add(this.Tel3Box1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox3.Location = new System.Drawing.Point(273, 72);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(242, 124);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Contato";
            // 
            // Student
            // 
            this.AcceptButton = this.commitBtn1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancelbtn;
            this.ClientSize = new System.Drawing.Size(515, 248);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Student";
            this.ShowInTaskbar = false;
            this.Text = "Novo Aluno";
            this.Load += new System.EventHandler(this.Student_Load);
            ((System.ComponentModel.ISupportInitialize)(this.responsavelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NameBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button FindRespBtn1;
        private System.Windows.Forms.DateTimePicker BirthdayDate1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker RegisteringDate1;
        private System.Windows.Forms.Button commitBtn1;
        private System.Windows.Forms.MaskedTextBox Tel3Box1;
        private System.Windows.Forms.MaskedTextBox Tel2Box1;
        private System.Windows.Forms.MaskedTextBox Tel1Box1;
        private System.Windows.Forms.ComboBox RespComb1;
        private System.Windows.Forms.BindingSource mainDataSetBindingSource;
        private MainDataSet mainDataSet;
        private System.Windows.Forms.BindingSource alunoBindingSource;
        private MainDataSetTableAdapters.AlunoTableAdapter alunoTableAdapter;
        private System.Windows.Forms.BindingSource responsavelBindingSource;
        private MainDataSetTableAdapters.ResponsavelTableAdapter responsavelTableAdapter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabelStudent;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Cancelbtn;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}