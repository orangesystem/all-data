﻿using System;
using System.Windows.Forms;
using AllData.MainDataSetTableAdapters;
using System.Data.SqlClient;

namespace AllData
{
    public partial class Payment : Form
    {
        private bool controlesValidos = false;
        public Payment()
        {
            InitializeComponent();
        }

        private void Commitbtn2_Click(object sender, EventArgs e)
        {
            MainDataSet.PagamentoRow pagamentoValido = null;
            try { pagamentoValido = ValidarControles(); }
            catch { MessageBox.Show("Certifique-se de que todos os campos obrigatórios estão preenchidos corretamente e o valor pago é um valor válido!"); }

            if(controlesValidos)
            switch (ConnectionHelper.Online)
            {
                case true:
                        try
                        {
                            PagamentoTableAdapter pagamento = ConnectionHelper.PagamentoTable;
                            pagamento.Insert((Guid)AlunoComb1.SelectedValue, ReferenceTime1.Value, PaymentDateTime1.Value, decimal.Parse(ValueBox1.Text), IsRemCkBox.Checked);
                            StatusLabelPagamento.Text = "Dados inseridos com sucesso!";
                            ClearControls();
                        }

                        catch (SqlException message)
                        {
                            MessageBox.Show("Não foi possível inserir os dados: " + message.Message);
                            if (message.ErrorCode == -2146232060)
                            {
                                if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    InputForm IP = new InputForm();
                                    if (IP.ShowDialog() == DialogResult.OK)
                                    {
                                        ConnectionHelper.GoOfflineMode();
                                        OfflinePagamento pagamento = new OfflinePagamento(pagamentoValido, ComandType.Insert);
                                        ConnectionHelper.OfflineData.OfflinePagamentoDataTable.Add(pagamento);
                                        ConnectionHelper.SaveOfflineData();

                                        StatusLabelPagamento.Text = "Dados salvos com sucesso!";
                                        ClearControls();
                                    }
                                }
                            }
                        }
                        catch(Exception err)
                        {
                            MessageBox.Show(string.Format("Ocorreu um erro inesperado!\n{0}\n{1}", err.Message, Internal.ErroEntreEmContato));
                            this.Close();
                        }
                    break;

                case false:
                    //Sem suporte ainda
                    break;
            }

        }

        void ClearControls()
        {
            ValueBox1.Clear();
            ReferenceTime1.Value = DateTime.Today;
            PaymentDateTime1.Value = DateTime.Today;
            IsRemCkBox.Checked = false;
        }

        private void Payment_Load(object sender, EventArgs e)
        {
            switch (ConnectionHelper.Online)
            {
                case true:
                    ConnectionHelper.AlunoTable.ClearBeforeFill = true;
                    ConnectionHelper.AlunoTable.Fill(this.mainDataSet.Aluno);
                    break;

                case false:
                    //Sem estrutura pronta para receber os dados offline
                    MessageBox.Show("Indisponível no modo offline!");
                    Close();
                    break;
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Descartar dados?", "Cancelar dados", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                ClearControls();
                StatusLabelPagamento.Text = "Operação cancelada!";
            }
        }

        private void IsRemCkBox_CheckedChanged(object sender, EventArgs e)
        {
            ReferenceTime1.Format = (IsRemCkBox.Checked) ? DateTimePickerFormat.Custom : DateTimePickerFormat.Short;
        }

        MainDataSet.PagamentoRow ValidarControles()
        {
            MainDataSet dataset = new MainDataSet();
            MainDataSet.PagamentoRow pagamentoValido = dataset.Pagamento.NewPagamentoRow();

            try { pagamentoValido.Aluno = (Guid)AlunoComb1.SelectedValue; controlesValidos = true; }
            catch { controlesValidos = false; throw new InvalidControlValue(); }

            try { pagamentoValido.Valor = decimal.Parse(ValueBox1.Text); controlesValidos = true; }
            catch { controlesValidos = false; throw new InvalidControlValue(); }

            pagamentoValido.Rematricula = IsRemCkBox.Checked;
            pagamentoValido.DataPagamento = PaymentDateTime1.Value;
            pagamentoValido.Referencia = ReferenceTime1.Value;
            controlesValidos = true;

            return pagamentoValido;
        }
    }
}
