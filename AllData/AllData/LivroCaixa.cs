﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AllData.MainDataSetTableAdapters;
using Microsoft.Win32;

namespace AllData
{
    public partial class LivroCaixa : Form
    {
        public LivroCaixa()
        {
            InitializeComponent();
            
        }

        public bool IsFormOpen { get; set; }

        #region etc
        private void LivroCaixa_Load(object sender, EventArgs e)
        { 
            ConnectionHelper.AlunoTable.Fill(mainDataSet.Aluno);
            IsFormOpen = true;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CaixaComandosRow c = (CaixaComandosRow)caixaComandosRowBindingSource.Current;
            c.AlunoIDColuna = mainDataSet.Aluno[alunoBindingSource.Position].ID;
        }

        public void ChangeID(bool status)
        {
            dataGridView1.Columns[1].Visible = status;
        }

        public void ShowCaixaValor(bool mostrar)
        {
            if(mostrar)
            {
                CalcularCaixa();
                CaixaValor.Visible = true;
            }
            else { CaixaValor.Visible = false; }
        }

        decimal CalcularCaixa()
        {
            decimal valor = new decimal();
            for(int i=0; i< caixaComandosRowBindingSource.Count;i++)
            {
                CaixaComandosRow linha = (CaixaComandosRow)caixaComandosRowBindingSource[i];
                valor += linha.ValorColuna;
            }

            CaixaValor.Text = string.Format("Valor no caixa: {0}", valor.ToString("C"));
            return valor;
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            CalcularCaixa();
        }
        #endregion

        public bool EditarCaixaHoje()
        {
            bool editar = true;
            CaixaComandos caixa = new CaixaComandos();
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            object[] parametros = new object[2];
            parametros[0] = path;
            parametros[1] = DateTime.Today.ToShortDateString().Replace("/", "-");

            try
            {
                caixa.ReadXmlSchema(string.Format(@"{0}\AllData\Caixa\Schema.adlcs", path));
                caixa.ReadXml(string.Format(@"{0}\AllData\Caixa\{1}.adlc", parametros));
            }

            catch (DirectoryNotFoundException)
            { editar = true; }

            catch (FileNotFoundException)
            { editar = true; }

            for(int i=0;i<caixa.Rows.Count;i++)
            {
                if(caixa.Rows[i].Field<bool>(7))
                {
                    editar = false;
                    break;
                }
            }

            if(editar)
            {
                for(int i=0;i<caixa.Rows.Count;i++)
                {
                    CaixaComandosRow valores = new CaixaComandosRow();
                    valores.TipoPagamentoColuna = caixa.Rows[i].Field<string>(0);
                    valores.AlunoIDColuna = caixa.Rows[i].Field<Guid>(1);
                    valores.AlunoNomeColuna = caixa.Rows[i].Field<string>(2);
                    valores.ValorColuna = caixa.Rows[i].Field<decimal>(3);
                    valores.ReferenciaColuna = caixa.Rows[i].Field<DateTime>(4);
                    valores.RematriculaColuna = caixa.Rows[i].Field<bool>(5);
                    valores.ObservacoesColuna = caixa.Rows[i].Field<string>(6);
                    valores.Sincronizado = caixa.Rows[i].Field<bool>(7);

                    caixaComandosRowBindingSource.Add(valores);
                }
            }

            return editar;
        }

        public void FecharCaixa()
        {
            //Para fechar o caixa é necessário primeiro inserir os dados no banco de dados, logo após
            //salvar no histórico do caixa

            if (ConnectionHelper.Online)
            { SalvarPagamentos(); }


            CaixaComandosRow[] comandosDados = new CaixaComandosRow[caixaComandosRowBindingSource.Count];
            for (int i = 0; i < caixaComandosRowBindingSource.Count; i++)
            { comandosDados[i] = (CaixaComandosRow)caixaComandosRowBindingSource[i]; }

            CaixaComandos caixaHoje = new CaixaComandos(comandosDados);
            SalvarNoDisco(caixaHoje);
        }

        public void PausarCaixa()
        {
            CaixaComandosRow[] comandosDados = new CaixaComandosRow[caixaComandosRowBindingSource.Count];
            for (int i = 0; i < caixaComandosRowBindingSource.Count; i++)
            { comandosDados[i] = (CaixaComandosRow)caixaComandosRowBindingSource[i]; }

            CaixaComandos caixaHoje = new CaixaComandos(comandosDados);
            SalvarNoDisco(caixaHoje);
            Close();
        }

        /// <summary>
        /// Salva os comandos no histórico do caixa
        /// </summary>
        /// <param name="caixa">caixa a ser salvo</param>
        void SalvarNoDisco(CaixaComandos caixa)
        {
            object[] parametros = new object[2];
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            parametros[0] = path;
            parametros[1] = DateTime.Today.ToShortDateString().Replace("/", "-");

            //Tenta salvar o caixa em seu determinado arquivo
            try
            { caixa.WriteXml(string.Format(@"{0}\AllData\Caixa\{1}.adlc", parametros)); }

            //caso não encontre o diretório, cria o mesmo
            catch(DirectoryNotFoundException)
            {
                Directory.CreateDirectory(string.Format(@"{0}\AllData\Caixa", path));
                caixa.WriteXmlSchema(string.Format(@"{0}\AllData\Caixa\Schema.adlcs", path));
                caixa.WriteXml(string.Format(@"{0}\AllData\Caixa\{1}.adlc", parametros));
            }
        }

        /// <summary>
        /// Salva os pagamentos realizados para dentro do banco de dados
        /// </summary>
        void SalvarPagamentos()
        {
            //Visualiza todos os pagamentos e aquele que tiver em seu tipo de pagamento escrito ´pagamento´ é salvo dentro do banco de dados
            for (int i = 0; i < caixaComandosRowBindingSource.Count; i++)
            {
                CaixaComandosRow linha = caixaComandosRowBindingSource[i] as CaixaComandosRow;

                if (linha.TipoPagamentoColuna == "Pagamento")
                {
                    try {
                        PagamentoTableAdapter pagamento = ConnectionHelper.PagamentoTable;
                        pagamento.Insert(linha.AlunoIDColuna, linha.ReferenciaColuna, DateTime.Today, linha.ValorColuna, linha.RematriculaColuna);
                        linha.Sincronizado = true;
                        caixaComandosRowBindingSource[i] = linha;
                    }

                    catch (System.Data.SqlClient.SqlException err)
                    {
                        MessageBox.Show(string.Format("Erro ao sincronizar pagamentos: {0}.\nOs dados serão enviados ao banco de dados na próxima vez em que " +
                            "o programa estiver conectado.", err.Message));

                        string nomeArquivo = string.Format("{0}.adlc", DateTime.Today.ToShortDateString().Replace("/", "-"));
                        RegistrarProximaChecagemDeCaixa(nomeArquivo);
                        break;
                    }
                }

                else
                { linha.Sincronizado = true; caixaComandosRowBindingSource[i] = linha; }
            }
        }

        /// <summary>
        /// Salva nos registros informando que determinado arquivo deve ser sincronizado com o banco de dados
        /// </summary>
        /// <param name="arquivoNome">Nome do historico de caixa que deve ser checado</param>
        void RegistrarProximaChecagemDeCaixa(string arquivoNome)
        {
            RegistryKey k =  Registry.CurrentUser.CreateSubKey(@"Software\AllData\Caixa");
            int nomeARegistrar = GetLastRegisteredValueName(k);
            nomeARegistrar++;

            k.SetValue(nomeARegistrar.ToString(), arquivoNome, RegistryValueKind.String);
        }

        /// <summary>
        /// Obtém o último valor registrado nos registros
        /// </summary>
        /// <param name="key">Chave a ser checada os valores</param>
        /// <returns>Último valor ou o valor mais alto registrado. Retorna -1 se não houver nenhum valor registrado</returns>
        private int GetLastRegisteredValueName(RegistryKey key)
        {
            int valor = -1;
            string[] valores = key.GetValueNames();
            for(int i=0;i<valores.Length; i++)
            {
                int atualValor = int.Parse(valores[i]);
                if(atualValor > valor)
                { valor = atualValor; }
            }

            return valor;
        }

        /// <summary>
        /// Insere no banco de dados os pagamentos descritos no arquivo passado como parametro
        /// </summary>
        public void SincronizarDadosSalvos(string arquivo)
        {
            string pasta = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string caminho = string.Format(@"{0}\Alldata\Caixa\Schema.adlcs", pasta);
            CaixaComandos caixa = new CaixaComandos();
            CaixaComandosRow linha = new CaixaComandosRow();
            bool conectado = true;

            try { caixa.ReadXmlSchema(caminho); }
            catch { WriteSchema(); caixa.ReadXmlSchema(caminho); }

            caixa.ReadXml(string.Format(@"{0}\Alldata\Caixa\{1}", pasta, arquivo));

            for (int i=0; i<caixa.Rows.Count;i++)
            {
                if(!caixa.Rows[i].Field<bool>(7))
                {
                    if(caixa.Rows[i].Field<string>(0) == "Pagamento")
                    {
                        Guid aluno = caixa.Rows[i].Field<Guid>(1);
                        DateTime referencia = caixa.Rows[i].Field<DateTime>(4);
                        DateTime? DataPagamento = ParseDate(arquivo);
                        decimal valor = caixa.Rows[i].Field<decimal>(3);
                        bool rematricula = caixa.Rows[i].Field<bool>(5);

                        try {
                            ConnectionHelper.PagamentoTable.Insert(aluno, referencia, DataPagamento, valor, rematricula);
                            caixa.Rows[i].SetField(7, true);
                        }

                        catch(System.Data.SqlClient.SqlException)
                        {
                            conectado = false;
                            break;
                        }                        
                    }

                    else
                    { caixa.Rows[i].SetField(7, true); }
                }
            }

            MessageBox.Show(caixa.Rows.Count.ToString());
            caixa.WriteXml(string.Format(@"{0}\Alldata\Caixa\{1}", pasta, arquivo));
            if (!conectado)
            {
                throw new Exception();
            }

        }

        /// <summary>
        /// Interpreta a data do arquivo com base no seu nome
        /// </summary>
        /// <param name="arquivo">Arquivo para interpretar</param>
        /// <returns>Data do caixa</returns>
        private DateTime ParseDate(string arquivo)
        {
            DateTime data = new DateTime();
            arquivo = arquivo.Replace(".adlc", "");
            arquivo = arquivo.Replace("-", "/");
            data = DateTime.Parse(arquivo);

            return data;
        }

        private void WriteSchema()
        {
            CaixaComandosRow[] c = new CaixaComandosRow[0];
            CaixaComandos caixa = new CaixaComandos(c);
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            try {
                caixa.WriteXmlSchema(string.Format(@"{0}\AllData\Caixa\Schema.adlcs", path));
            }

            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory(string.Format(@"{0}\AllData\Caixa", path));
                caixa.WriteXmlSchema(string.Format(@"{0}\AllData\Caixa\Schema.adlcs", path));
            }
        }

        private void LivroCaixa_FormClosed(object sender, FormClosedEventArgs e)
        {
            IsFormOpen = false;
        }
    }

    public class CaixaComandos : DataTable
    {
        public string Nome { get { return _nome; } }
        public CaixaComandosRow[] Comandos { get { return _comandos; } }

        private string _nome;
        private CaixaComandosRow[] _comandos;

        /// <summary>
        /// Inicializa um caixa totalmente vazio pronto para ser editado
        /// </summary>
        public CaixaComandos()
        {
            _nome = "";
        }

        /// <summary>
        /// Cria uma instância do caixa pronta para ser salva no histórico
        /// </summary>
        /// <param name="comandos">Comandos a serem inseridos para o salvamento do arquivo</param>
        public CaixaComandos(CaixaComandosRow[] comandos)
        {
            InicializarColunas();
            //O nome do arquivo é a sua data de criação
            //string nomeArquivo = DateTime.Today.ToShortDateString();
            string nomeArquivo = "Comando";
            TableName = _nome = nomeArquivo;
            _comandos = comandos;

            foreach(CaixaComandosRow c in comandos)
            {
                object[] valores = new object[8];
                valores[0] = c.TipoPagamentoColuna;
                valores[1] = c.AlunoIDColuna;
                valores[2] = c.AlunoNomeColuna;
                valores[3] = c.ValorColuna;
                valores[4] = c.ReferenciaColuna;
                valores[5] = c.RematriculaColuna;
                valores[6] = c.ObservacoesColuna;
                valores[7] = c.Sincronizado;

                Rows.Add(valores);
            }
        }

        /// <summary>
        /// Método utilizado somente para visualização de histórico
        /// </summary>
        /// <param name="nome">Nome diário do caixa</param>
        /// <param name="data">Data do caixa</param>
        /// <param name="comandos">Comandos utlizados</param>
        public CaixaComandos(string nome, CaixaComandosRow[] comandos)
        {
            TableName = _nome = nome;
            _comandos = comandos;
        }

        private void InicializarColunas()
        {
            Columns.Add(new DataColumn("Tipo Pagamento", typeof(string)));
            Columns.Add(new DataColumn("Aluno ID", typeof(Guid)));
            Columns.Add(new DataColumn("Nome Aluno", typeof(string)));
            Columns.Add(new DataColumn("Valor", typeof(decimal)));
            Columns.Add(new DataColumn("Referencia", typeof(DateTime)));
            Columns.Add(new DataColumn("Rematricula", typeof(bool)));
            Columns.Add(new DataColumn("Observacoes", typeof(string)));
            Columns.Add(new DataColumn("Sincronizado", typeof(bool)));
        }

    }

    public class CaixaComandosRow
    {
        public string TipoPagamentoColuna { get { return _tipoPagamento; } set { _tipoPagamento = value; } }
        public Guid AlunoIDColuna { get { return _alunoId; } set { _alunoId = value; } }
        public string AlunoNomeColuna { get { return _alunoNome; } set { _alunoNome = value; } }
        public decimal ValorColuna { get { return _valor; } set { _valor = value; } }
        public DateTime ReferenciaColuna { get { return _referencia; } set { _referencia = value; } }
        public bool RematriculaColuna { get { return _rematricula; } set { _rematricula = value; } }
        public string ObservacoesColuna { get { return _observacoes; } set { _observacoes = value; } }
        public bool Sincronizado { get; set; }

        private string _tipoPagamento;
        private Guid _alunoId;
        private string _alunoNome;
        private decimal _valor;
        private DateTime _referencia;
        private bool _rematricula;
        private string _observacoes;
        private bool _sincronizado;

        /// <summary>
        /// Inicializa uma nova insância de uma linha de comandos
        /// </summary>
        public CaixaComandosRow()
        {
            _tipoPagamento = "";
            _alunoId = new Guid();
            _alunoNome = "";
            _valor = new decimal();
            _referencia = DateTime.Today;
            _rematricula = new bool();
            _observacoes = "";
            _sincronizado = false;
        }

        /// <summary>
        /// Inicializa uma nova linha de comandos do caixa
        /// </summary>
        /// <param name="tipoPagamento">Tipo de pagamento efetuado</param>
        /// <param name="alunoId">ID do aluno</param>
        /// <param name="alunoNome">Nome do aluno</param>
        /// <param name="valor">Valor pago</param>
        /// <param name="referencia">Data a que se refere o pagamento</param>
        /// <param name="rematricula">É pagamento de rematrícula?</param>
        /// <param name="observacoes">Observações</param>
        /// <param name="sincronizado">O dado está sincronizado com o servidor?</param>
        public CaixaComandosRow(string tipoPagamento, Guid alunoId,string alunoNome, decimal valor, DateTime referencia, 
            bool rematricula, string observacoes, bool sincronizado)
        {
            _tipoPagamento = tipoPagamento;
            _alunoId = alunoId;
            _alunoNome = alunoNome;
            _valor = valor;
            _referencia = referencia;
            _rematricula = rematricula;
            _observacoes = observacoes;
            _sincronizado = sincronizado;
        }

        /// <summary>
        /// Inicializa uma nova linha de comandos com base em uma linha preestabelecida
        /// </summary>
        public CaixaComandosRow(CaixaComandosRow comando)
        {
            _tipoPagamento = comando.TipoPagamentoColuna;
            _alunoId = comando.AlunoIDColuna;
            _alunoNome = comando.AlunoNomeColuna;
            _valor = comando.ValorColuna;
            _referencia = comando.ReferenciaColuna;
            _rematricula = comando.RematriculaColuna;
            _observacoes = comando.ObservacoesColuna;
            _sincronizado = comando.Sincronizado;
        }
    }

    public enum CaixaTipoValor { Pagamento = 0, Compra, Troco, Outro}
}
