﻿namespace AllData
{
    partial class Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Payment));
            this.ReferenceTime1 = new System.Windows.Forms.DateTimePicker();
            this.PaymentDateTime1 = new System.Windows.Forms.DateTimePicker();
            this.ValueBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Commitbtn2 = new System.Windows.Forms.Button();
            this.AlunoComb1 = new System.Windows.Forms.ComboBox();
            this.alunoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSet = new AllData.MainDataSet();
            this.alunoTableAdapter = new AllData.MainDataSetTableAdapters.AlunoTableAdapter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.IsRemCkBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabelPagamento = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CancelBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReferenceTime1
            // 
            this.ReferenceTime1.CustomFormat = "yyyy";
            this.ReferenceTime1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ReferenceTime1.Location = new System.Drawing.Point(115, 23);
            this.ReferenceTime1.Name = "ReferenceTime1";
            this.ReferenceTime1.Size = new System.Drawing.Size(215, 20);
            this.ReferenceTime1.TabIndex = 2;
            // 
            // PaymentDateTime1
            // 
            this.PaymentDateTime1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.PaymentDateTime1.Location = new System.Drawing.Point(115, 49);
            this.PaymentDateTime1.Name = "PaymentDateTime1";
            this.PaymentDateTime1.Size = new System.Drawing.Size(215, 20);
            this.PaymentDateTime1.TabIndex = 4;
            // 
            // ValueBox1
            // 
            this.ValueBox1.Location = new System.Drawing.Point(116, 46);
            this.ValueBox1.Name = "ValueBox1";
            this.ValueBox1.Size = new System.Drawing.Size(132, 20);
            this.ValueBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Aluno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Valor Pago";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Referente a";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Data de Pagamento";
            // 
            // Commitbtn2
            // 
            this.Commitbtn2.Location = new System.Drawing.Point(366, 3);
            this.Commitbtn2.Name = "Commitbtn2";
            this.Commitbtn2.Size = new System.Drawing.Size(75, 23);
            this.Commitbtn2.TabIndex = 5;
            this.Commitbtn2.Text = "Confirmar";
            this.Commitbtn2.UseVisualStyleBackColor = true;
            this.Commitbtn2.Click += new System.EventHandler(this.Commitbtn2_Click);
            // 
            // AlunoComb1
            // 
            this.AlunoComb1.DataSource = this.alunoBindingSource;
            this.AlunoComb1.DisplayMember = "Nome";
            this.AlunoComb1.FormattingEnabled = true;
            this.AlunoComb1.Location = new System.Drawing.Point(116, 21);
            this.AlunoComb1.Name = "AlunoComb1";
            this.AlunoComb1.Size = new System.Drawing.Size(132, 21);
            this.AlunoComb1.TabIndex = 0;
            this.AlunoComb1.ValueMember = "ID";
            // 
            // alunoBindingSource
            // 
            this.alunoBindingSource.DataMember = "Aluno";
            this.alunoBindingSource.DataSource = this.mainDataSet;
            // 
            // mainDataSet
            // 
            this.mainDataSet.DataSetName = "MainDataSet";
            this.mainDataSet.EnforceConstraints = false;
            this.mainDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // alunoTableAdapter
            // 
            this.alunoTableAdapter.ClearBeforeFill = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.IsRemCkBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ReferenceTime1);
            this.groupBox1.Controls.Add(this.PaymentDateTime1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(15, 103);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(426, 90);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Referência";
            // 
            // IsRemCkBox
            // 
            this.IsRemCkBox.AutoSize = true;
            this.IsRemCkBox.Location = new System.Drawing.Point(336, 26);
            this.IsRemCkBox.Name = "IsRemCkBox";
            this.IsRemCkBox.Size = new System.Drawing.Size(84, 17);
            this.IsRemCkBox.TabIndex = 3;
            this.IsRemCkBox.Text = "Rematrícula";
            this.IsRemCkBox.UseVisualStyleBackColor = true;
            this.IsRemCkBox.CheckedChanged += new System.EventHandler(this.IsRemCkBox_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.ValueBox1);
            this.groupBox2.Controls.Add(this.AlunoComb1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(15, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(426, 85);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Informações";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabelPagamento});
            this.statusStrip1.Location = new System.Drawing.Point(0, 228);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(446, 22);
            this.statusStrip1.TabIndex = 13;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabelPagamento
            // 
            this.StatusLabelPagamento.Name = "StatusLabelPagamento";
            this.StatusLabelPagamento.Size = new System.Drawing.Size(39, 17);
            this.StatusLabelPagamento.Text = "Status";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CancelBtn);
            this.panel1.Controls.Add(this.Commitbtn2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 199);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(446, 29);
            this.panel1.TabIndex = 14;
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Location = new System.Drawing.Point(285, 3);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 23);
            this.CancelBtn.TabIndex = 9;
            this.CancelBtn.Text = "Cancelar";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // Payment
            // 
            this.AcceptButton = this.Commitbtn2;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(446, 250);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Payment";
            this.Text = "Pagamento";
            this.Load += new System.EventHandler(this.Payment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.alunoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker ReferenceTime1;
        private System.Windows.Forms.DateTimePicker PaymentDateTime1;
        private System.Windows.Forms.TextBox ValueBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Commitbtn2;
        private System.Windows.Forms.ComboBox AlunoComb1;
        private MainDataSet mainDataSet;
        private System.Windows.Forms.BindingSource alunoBindingSource;
        private MainDataSetTableAdapters.AlunoTableAdapter alunoTableAdapter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox IsRemCkBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabelPagamento;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CancelBtn;
    }
}