﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using AllData.MainDataSetTableAdapters;

namespace AllData
{
    public class DataHelper
    {
        public enum AlunoCheckingtype { Nome = 0, Telefone1 = 1, Telefone2 = 2, Telefone3 = 3, AllTelefones = 4,
        NomeETelefones = 5};
        public enum ResponsavelCheckingType { Nome = 0, Telefone1 = 1, Telefone2 = 2, Telefone3 = 3, AllTelefones = 4, CPF = 5};

        FileStream tempFile = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)+@"\Orange System\temp\temp comands.adtc", FileMode.OpenOrCreate);
        static private OfflineDataSet OfflineData = new OfflineDataSet();
        static private MainDataSet maindataSet = new MainDataSet();

        #region BirthdatesFunctions

        /// <summary>
        /// Checa se há algum ocorrência de aniversários no mês
        /// </summary>
        /// <returns>Verdadeiro se houver</returns>
        static public bool CheckMonthBirthdates()
        {
            MainDataSet.AlunoDataTable alunos = ConnectionHelper.AlunoTable.GetData(); // Pega os alunos 
            List<MainDataSet.AlunoRow> alunosAniversariantes = new List<MainDataSet.AlunoRow>();
            int alunosCount = alunos.Count;
            
            //Compara o mês de aniversário de cada aluno com o mês atual
            for(int i=0;i<alunosCount; i++)
            {
                try
                {
                    //Se o mês for igual, adiciona o aluno à lista de anivesariantes
                    if (alunos[i].Nascimento.Month == DateTime.Today.Month)
                    { alunosAniversariantes.Add(alunos[i]); }
                }
                catch { }
            }

            //Mesmo sistema dos alunos, mas agora para os responsáveis
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            List<MainDataSet.ResponsavelRow> responsaveisAniversariantes = new List<MainDataSet.ResponsavelRow>();
            int responsaveisCount = responsaveis.Count;

            for(int i=0; i<responsaveisCount; i++)
            {
                try {
                    if (responsaveis[i].Nascimento.Month == DateTime.Today.Month)
                    { responsaveisAniversariantes.Add(responsaveis[i]); }
                }
                catch { }
            }


            if(alunosAniversariantes.Count > 0 || responsaveisAniversariantes.Count > 0)
            {
                return true;
            }
            else { return false; }
        }

        /// <summary>
        /// Checa se há alguma ocorrência de aniversários na semana
        /// </summary>
        /// <returns>Verdadeiro se houver</returns>
        static public bool CheckWeekBirthdates()
        {
            MainDataSet.AlunoDataTable alunos = ConnectionHelper.AlunoTable.GetData();
            List<MainDataSet.AlunoRow> alunosAniversariantes = new List<MainDataSet.AlunoRow>();
            int alunosCount = alunos.Count;

            //Checa cada aluno se o dia de seu aniversário corresponde à atual semana
            for (int i = 0; i < alunosCount; i++)
            {
                try {
                    //Adiciona o aluno ao índice se corresponder
                    if (DateTime.Today.DayOfYear <= alunos[i].Nascimento.DayOfYear && DateTime.Today.AddDays(7).DayOfYear >= alunos[i].Nascimento.DayOfYear)
                    { alunosAniversariantes.Add(alunos[i]); }
                }
                catch { }
            }

            //mesmo sistema para os pais
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            List<MainDataSet.ResponsavelRow> responsaveisAniversariantes = new List<MainDataSet.ResponsavelRow>();
            int responsaveisCount = responsaveis.Count;

            for (int i = 0; i < responsaveisCount; i++)
            {
                try {
                    if (DateTime.Today.DayOfYear <= responsaveis[i].Nascimento.DayOfYear && DateTime.Today.AddDays(7).DayOfYear >= responsaveis[i].Nascimento.DayOfYear)
                    { responsaveisAniversariantes.Add(responsaveis[i]); }
                }
                catch { }
            }


            if (alunosAniversariantes.Count > 0 || responsaveisAniversariantes.Count > 0)
            {
                return true;
            }
            else { return false; }
        }

        /// <summary>
        /// Checa se há alguma ocorrência de aniversários no dia atual ou no próximo
        /// </summary>
        /// <returns>Verdadeiro se houver</returns>
        static public bool CheckDayBirthdates()
        {
            MainDataSet.AlunoDataTable alunos = ConnectionHelper.AlunoTable.GetData();
            List<MainDataSet.AlunoRow> alunosAniversariantes = new List<MainDataSet.AlunoRow>();
            int alunosCount = alunos.Count;

            //Se o dia atual ou o próximo corresponder ao dia de aniversário do aluno, adiciona-o ao índice
            for (int i = 0; i < alunosCount; i++)
            {
                try {
                    if (DateTime.Today.DayOfYear == alunos[i].Nascimento.DayOfYear ||
                        DateTime.Today.AddDays(1).DayOfYear == alunos[i].Nascimento.DayOfYear)
                    { alunosAniversariantes.Add(alunos[i]); }
                }
                catch { }
            }

            //mesmo sistema de alunos mas para os responsáveis
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            List<MainDataSet.ResponsavelRow> responsaveisAniversariantes = new List<MainDataSet.ResponsavelRow>();
            int responsaveisCount = responsaveis.Count;

            for (int i = 0; i < responsaveisCount; i++)
            {
                try {
                    if (DateTime.Today.DayOfYear == responsaveis[i].Nascimento.DayOfYear ||
                        DateTime.Today.AddDays(1).DayOfYear == responsaveis[i].Nascimento.DayOfYear)
                    { responsaveisAniversariantes.Add(responsaveis[i]); }
                }
                catch { }
            }

            if (alunosAniversariantes.Count > 0 || responsaveisAniversariantes.Count > 0)
            {
                return true;
            }
            else { return false; }
        }

        /// <summary>
        /// Retorna os aniversariantes do mês atual
        /// </summary>
        static public MainDataSet.AlunoDataTable GetAlunoMonthBirthdates()
        {
            MainDataSet.AlunoDataTable alunos = ConnectionHelper.AlunoTable.GetData();
            List<MainDataSet.AlunoRow> alunosAniversariantes = new List<MainDataSet.AlunoRow>();
            MainDataSet.AlunoDataTable alunosRetornados = new MainDataSet.AlunoDataTable();
            int alunosCount = alunos.Count;

            //Se for o mesmo mês, adiciona o aluno ao índice
            for (int i = 0; i < alunosCount; i++)
            {
                try
                {
                    if (alunos[i].Nascimento.Month == DateTime.Today.Month)
                    { alunosAniversariantes.Add(alunos[i]); }
                }
                catch { }
            }

            foreach(MainDataSet.AlunoRow aluno in alunosAniversariantes)
            { alunosRetornados.ImportRow(aluno); }

            return alunosRetornados;
        }

        /// <summary>
        /// Retorna os aniversariantes da semana atual
        /// </summary>
        static public MainDataSet.AlunoDataTable GetAlunoWeekBirthdates()
        {
            MainDataSet.AlunoDataTable alunos = ConnectionHelper.AlunoTable.GetData();
            List<MainDataSet.AlunoRow> alunosAniversariantes = new List<MainDataSet.AlunoRow>();
            MainDataSet.AlunoDataTable alunosRetornados = new MainDataSet.AlunoDataTable();
            int alunosCount = alunos.Count;

            for (int i = 0; i < alunosCount; i++)
            {
                try
                {
                    if (DateTime.Today.DayOfYear <= alunos[i].Nascimento.DayOfYear && DateTime.Today.AddDays(7).DayOfYear >= alunos[i].Nascimento.DayOfYear)
                    { alunosAniversariantes.Add(alunos[i]); }
                }
                catch { }
            }

            foreach(MainDataSet.AlunoRow aluno in alunosAniversariantes)
            { alunosRetornados.ImportRow(aluno); }

            return alunosRetornados;
        }

        /// <summary>
        /// Retorna os aniversariantes do dia atual ou do próximo
        /// </summary>
        static public MainDataSet.AlunoDataTable GetAlunoDayBirthdates()
        {
            MainDataSet.AlunoDataTable alunos = ConnectionHelper.AlunoTable.GetData();
            List<MainDataSet.AlunoRow> alunosAniversariantes = new List<MainDataSet.AlunoRow>();
            MainDataSet.AlunoDataTable alunosRetornados = new MainDataSet.AlunoDataTable();
            int alunosCount = alunos.Count;

            for (int i = 0; i < alunosCount; i++)
            {
                try
                {
                    if (DateTime.Today.DayOfYear == alunos[i].Nascimento.DayOfYear ||
                        DateTime.Today.AddDays(1).DayOfYear == alunos[i].Nascimento.DayOfYear)
                    { alunosAniversariantes.Add(alunos[i]); }
                }
                catch { }
            }

            foreach(MainDataSet.AlunoRow aluno in alunosAniversariantes)
            { alunosRetornados.ImportRow(aluno); }

            return alunosRetornados;
        }

        /// <summary>
        /// Retorna os aniversariantes do mês atual
        /// </summary>
        static public MainDataSet.ResponsavelDataTable GetResponsavelMonthBirthdates()
        {
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            List<MainDataSet.ResponsavelRow> responsaveisAniversariantes = new List<MainDataSet.ResponsavelRow>();
            MainDataSet.ResponsavelDataTable responsaveisRetornados = new MainDataSet.ResponsavelDataTable();
            int responsaveisCount = responsaveis.Count;

            for (int i = 0; i < responsaveisCount; i++)
            {
                try
                {
                    if (responsaveis[i].Nascimento.Month == DateTime.Today.Month)
                    { responsaveisAniversariantes.Add(responsaveis[i]); }
                }
                catch { }
            }

            foreach(MainDataSet.ResponsavelRow responsavel in responsaveisAniversariantes)
            {
                responsaveisRetornados.ImportRow(responsavel);
            }

            return responsaveisRetornados;
        }

        /// <summary>
        /// Retorna os aniversariantes da semana atual
        /// </summary>
        static public MainDataSet.ResponsavelDataTable GetResponsavelWeekBirthdates()
        {
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            List<MainDataSet.ResponsavelRow> responsaveisAniversariantes = new List<MainDataSet.ResponsavelRow>();
            MainDataSet.ResponsavelDataTable responsaveisRetornados = new MainDataSet.ResponsavelDataTable();
            int responsaveisCount = responsaveis.Count;

            for (int i = 0; i < responsaveisCount; i++)
            {
                try
                {
                    if (DateTime.Today.DayOfYear <= responsaveis[i].Nascimento.DayOfYear && DateTime.Today.AddDays(7).DayOfYear >= responsaveis[i].Nascimento.DayOfYear)
                    { responsaveisAniversariantes.Add(responsaveis[i]); }
                }
                catch { }
            }

            foreach(MainDataSet.ResponsavelRow responsavel in responsaveisAniversariantes)
            {
                responsaveisRetornados.ImportRow(responsavel);
            }

            return responsaveisRetornados;
        }

        /// <summary>
        /// Retorna os aniversariantes do dia atual ou do próximo
        /// </summary>
        static public MainDataSet.ResponsavelDataTable GetResponsavelDayBirthdates()
        {
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            List<MainDataSet.ResponsavelRow> responsaveisAniversariantes = new List<MainDataSet.ResponsavelRow>();
            MainDataSet.ResponsavelDataTable responsaveisRetornados = new MainDataSet.ResponsavelDataTable();
            int responsaveisCount = responsaveis.Count;

            for (int i = 0; i < responsaveisCount; i++)
            {
                try
                {
                    if (DateTime.Today.DayOfYear == responsaveis[i].Nascimento.DayOfYear ||
                        DateTime.Today.AddDays(1).DayOfYear == responsaveis[i].Nascimento.DayOfYear)
                    { responsaveisAniversariantes.Add(responsaveis[i]); }
                }
                catch { }
            }

            foreach(MainDataSet.ResponsavelRow responsavel in responsaveisAniversariantes)
            {
                responsaveisRetornados.ImportRow(responsavel);
            }

            return responsaveisRetornados;
        }
        #endregion BirthdatesFunctions

        #region ExistOverloads

        #region AlunoOverloads

        /// <summary>
        /// Checa se há alguma repetição com base no tipo de checagem escolhida
        /// </summary>
        /// <param name="aluno">Aluno a ser checado</param>
        /// <param name="check">tipo de checagem</param>
        /// <returns>Retorna verdadeiro se houver alguma repetição</returns>
        static public bool Exist(MainDataSet.AlunoRow aluno, AlunoCheckingtype check)
        {
            MainDataSet.AlunoDataTable alunos = ConnectionHelper.AlunoTable.GetData();
            bool exist = false;

            //Checa a ocorrência com base no tipo de checagem
            switch (check)
            {
                case AlunoCheckingtype.Nome:
                    foreach(MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if(aluno.Nome == alunoAtual.Nome)
                        { exist = true; break; }
                    }
                    break;

                case AlunoCheckingtype.Telefone1:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (aluno.Telefone1 == alunoAtual.Telefone1)
                        { exist = true; break; }
                    }
                    break;

                case AlunoCheckingtype.Telefone2:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (aluno.Telefone2 == alunoAtual.Telefone2)
                        { exist = true; break; }
                    }
                    break;

                case AlunoCheckingtype.Telefone3:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (aluno.Telefone3 == alunoAtual.Telefone3)
                        { exist = true; break; }
                    }
                    break;

                case AlunoCheckingtype.AllTelefones:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (aluno.Telefone1 == alunoAtual.Telefone1 || 
                            aluno.Telefone2 == alunoAtual.Telefone2 ||
                            aluno.Telefone3 == alunoAtual.Telefone3)
                        { exist = true; break; }
                    }
                    break;

                case AlunoCheckingtype.NomeETelefones:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (aluno.Telefone1 == alunoAtual.Telefone1 ||
                            aluno.Telefone2 == alunoAtual.Telefone2 ||
                            aluno.Telefone3 == alunoAtual.Telefone3 ||
                            aluno.Nome == alunoAtual.Nome)
                        { exist = true; break; }
                    }
                    break;

            }

            return exist;
        }

        /// <summary>
        /// Checa se há alguma repetição com base no tipo de checagem escolhida
        /// </summary>
        /// <param name="alunoValor">Valor a ser checado</param>
        /// <param name="check">Tipo de checagem</param>
        /// <returns>Retorna verdadeiro se houver alguma repetição</returns>
        static public bool Exist(string alunoValor, AlunoCheckingtype check)
        {
            MainDataSet.AlunoDataTable alunos = ConnectionHelper.AlunoTable.GetData();
            bool exist = false;

            //Faz a checagem com base no tipo de checagem escolhida
            switch (check)
            {
                case AlunoCheckingtype.Nome:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (alunoValor == alunoAtual.Nome)
                        { exist = true; break; }
                    }
                    break;

                case AlunoCheckingtype.Telefone1:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (alunoValor == alunoAtual.Telefone1)
                        { exist = true; break; }
                    }
                    break;

                case AlunoCheckingtype.Telefone2:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (alunoValor == alunoAtual.Telefone2)
                        { exist = true; break; }
                    }
                    break;

                case AlunoCheckingtype.Telefone3:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (alunoValor == alunoAtual.Telefone3)
                        { exist = true; break; }
                    }
                    break;

                case AlunoCheckingtype.AllTelefones:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (alunoValor == alunoAtual.Telefone1 ||
                            alunoValor == alunoAtual.Telefone2 ||
                            alunoValor == alunoAtual.Telefone3)
                        { exist = true; break; }
                    }
                    break;

                case AlunoCheckingtype.NomeETelefones:
                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        if (alunoValor == alunoAtual.Telefone1 ||
                            alunoValor == alunoAtual.Telefone2 ||
                            alunoValor == alunoAtual.Telefone3 ||
                            alunoValor == alunoAtual.Nome)
                        { exist = true; break; }
                    }
                    break;

            }

            return exist;
        }
        #endregion AlunoOverloads

        #region ResponsávelOverloads

        /// <summary>
        /// Faz a checagem de repetição de dados de responsável com base no tipo de checagem escolhida
        /// </summary>
        /// <param name="responsavel">Responsável a ser checado</param>
        /// <param name="check">Tipo de checagem</param>
        /// <returns>Verdadeiro se houver alguma repetição de dados</returns>
        static public bool Exist(MainDataSet.ResponsavelRow responsavel, ResponsavelCheckingType check)
        {
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            bool exist = false;

            //Checa com base no tipo de checagem escolhida
            switch(check)
            {
                case ResponsavelCheckingType.Nome:
                    foreach(MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if(responsavel.Nome == responsavelAtual.Nome)
                        { exist = true;  break; }
                    }
                    break;

                case ResponsavelCheckingType.CPF:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavel.CPF == responsavelAtual.CPF)
                        { exist = true; break; }
                    }
                    break;

                case ResponsavelCheckingType.Telefone1:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavel.Telefone1 == responsavelAtual.Telefone1)
                        { exist = true; break; }
                    }
                    break;

                case ResponsavelCheckingType.Telefone2:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavel.Telefone2 == responsavelAtual.Telefone2)
                        { exist = true; break; }
                    }
                    break;

                case ResponsavelCheckingType.Telefone3:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavel.Telefone3 == responsavelAtual.Telefone3)
                        { exist = true; break; }
                    }
                    break;

                case ResponsavelCheckingType.AllTelefones:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavel.Telefone1 == responsavelAtual.Telefone1 ||
                            responsavel.Telefone2 == responsavelAtual.Telefone2 ||
                            responsavel.Telefone3 == responsavelAtual.Telefone3)
                        { exist = true; break; }
                    }
                    break;

            }

            return exist;
        }

        /// <summary>
        /// Faz a checagem de dados de responsável com base no tipo de checagem escolhida
        /// </summary>
        /// <param name="responsavelValor">Valor a ser checado</param>
        /// <param name="check">Tipo de checagem escolhida</param>
        /// <returns>Verdadeiro se houver repetição de dados</returns>
        static public bool Exist(string responsavelValor, ResponsavelCheckingType check)
        {
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            bool exist = false;

            //Faz a checagem com base no tipo de checagem escolhida
            switch (check)
            {
                case ResponsavelCheckingType.Nome:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavelValor == responsavelAtual.Nome)
                        { exist = true; break; }
                    }
                    break;

                case ResponsavelCheckingType.CPF:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavelValor == responsavelAtual.CPF)
                        { exist = true; break; }
                    }
                    break;

                case ResponsavelCheckingType.Telefone1:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavelValor == responsavelAtual.Telefone1)
                        { exist = true; break; }
                    }
                    break;

                case ResponsavelCheckingType.Telefone2:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavelValor == responsavelAtual.Telefone2)
                        { exist = true; break; }
                    }
                    break;

                case ResponsavelCheckingType.Telefone3:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavelValor == responsavelAtual.Telefone3)
                        { exist = true; break; }
                    }
                    break;

                case ResponsavelCheckingType.AllTelefones:
                    foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                    {
                        if (responsavelValor == responsavelAtual.Telefone1 ||
                            responsavelValor == responsavelAtual.Telefone2 ||
                            responsavelValor == responsavelAtual.Telefone3)
                        { exist = true; break; }
                    }
                    break;

            }

            return exist;
        }
        #endregion ResponsávelOverloads

        #endregion ExistOverloads

        #region DuplicatasFunctions

        /// <summary>
        /// Obtém as ocorrências duplicadas dos alunos com base no nome
        /// </summary>
        static public MainDataSet.AlunoDataTable GetAlunoDuplicatasByName()
        {
            MainDataSet.AlunoDataTable alunosDeRetorno = null;
            switch (ConnectionHelper.Online)
            {
                case true:
                    MainDataSet.AlunoDataTable alunos = ConnectionHelper.AlunoTable.GetData();
                    List<MainDataSet.AlunoRow> alunosDuplicados = new List<MainDataSet.AlunoRow>();
                    int alunoCount = 0;

                    foreach (MainDataSet.AlunoRow alunoAtual in alunos)
                    {
                        List<MainDataSet.AlunoRow> alunoDuplicado = new List<MainDataSet.AlunoRow>();
                        for (int i = alunoCount + 1; i < alunos.Count; i++)
                        {
                            if (alunoAtual.Nome == alunos[i].Nome)
                            {
                                alunoDuplicado.Add(alunos[i]);
                            }
                        }

                        if (alunoDuplicado.Count > 0) { alunoDuplicado.Add(alunoAtual); }

                        foreach (MainDataSet.AlunoRow alunoConcorrente in alunoDuplicado)
                        {
                            bool entrarNaLista = true;
                            for (int i = 0; i < alunosDuplicados.Count; i++)
                            {
                                if (alunoConcorrente == alunosDuplicados[i])
                                { entrarNaLista = false; break; }
                            }

                            if (entrarNaLista) { alunosDuplicados.Add(alunoConcorrente); }
                        }
                        alunoCount++;
                    }

                    alunosDeRetorno = new MainDataSet.AlunoDataTable();
                    foreach (MainDataSet.AlunoRow aln in alunosDuplicados)
                    {
                        MainDataSet.AlunoRow r = aln;
                        alunosDeRetorno.ImportRow(r);
                    }
                    break;
                case false:
                    alunosDeRetorno = new MainDataSet.AlunoDataTable();
                    break;
            }
            return alunosDeRetorno;
        }

        /// <summary>
        /// Obtém as ocorrências duplicadas dos responsáveis com base no nome
        /// </summary>
        static public MainDataSet.ResponsavelDataTable GetResponsavelDuplicatasByName()
        {
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            List<MainDataSet.ResponsavelRow> responsaveisDuplicados = new List<MainDataSet.ResponsavelRow>();
            int responsavelCount = 0;

            foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
            {
                List<MainDataSet.ResponsavelRow> responsavelDuplicado = new List<MainDataSet.ResponsavelRow>();
                for (int i = responsavelCount + 1; i < responsaveis.Count; i++)
                {
                    if (responsavelAtual.Nome == responsaveis[i].Nome)
                    {
                        responsavelDuplicado.Add(responsaveis[i]);
                    }
                }

                if (responsavelDuplicado.Count > 0) { responsavelDuplicado.Add(responsavelAtual); }

                foreach (MainDataSet.ResponsavelRow responsavelConcorrente in responsavelDuplicado)
                {
                    bool entrarNaLista = true;
                    for (int i = 0; i < responsaveisDuplicados.Count; i++)
                    {
                        if (responsavelConcorrente == responsaveisDuplicados[i])
                        { entrarNaLista = false; break; }
                    }

                    if (entrarNaLista) { responsaveisDuplicados.Add(responsavelConcorrente); }
                }
                responsavelCount++;
            }

            MainDataSet.ResponsavelDataTable responsaveisDeRetorno = new MainDataSet.ResponsavelDataTable();
            foreach (MainDataSet.ResponsavelRow resp in responsaveisDuplicados)
            {
                MainDataSet.ResponsavelRow r = resp;
                responsaveisDeRetorno.ImportRow(r);
            }

            return responsaveisDeRetorno;
        }

        static public MainDataSet.ResponsavelDataTable GetResponsavelDuplicatasByCPF()
        {
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            List<MainDataSet.ResponsavelRow> responsaveisDuplicados = new List<MainDataSet.ResponsavelRow>();
            int responsavelCount = 0;

            foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
            {
                List<MainDataSet.ResponsavelRow> responsavelDuplicado = new List<MainDataSet.ResponsavelRow>();
                for (int i = responsavelCount + 1; i < responsaveis.Count; i++)
                {
                    if (responsavelAtual.CPF == responsaveis[i].CPF)
                    {
                        responsavelDuplicado.Add(responsaveis[i]);
                    }
                }

                if (responsavelDuplicado.Count > 0) { responsavelDuplicado.Add(responsavelAtual); }

                foreach (MainDataSet.ResponsavelRow responsavelConcorrente in responsavelDuplicado)
                {
                    bool entrarNaLista = true;
                    for (int i = 0; i < responsaveisDuplicados.Count; i++)
                    {
                        if (responsavelConcorrente == responsaveisDuplicados[i])
                        { entrarNaLista = false; break; }
                    }

                    if (entrarNaLista) { responsaveisDuplicados.Add(responsavelConcorrente); }
                }
                responsavelCount++;
            }

            MainDataSet.ResponsavelDataTable responsaveisDeRetorno = new MainDataSet.ResponsavelDataTable();
            foreach (MainDataSet.ResponsavelRow resp in responsaveisDuplicados)
            {
                MainDataSet.ResponsavelRow r = resp;
                responsaveisDeRetorno.ImportRow(r);
            }

            return responsaveisDeRetorno;
        }

        static public MainDataSet.ResponsavelDataTable GetResponsavelDuplicatasByNameAndCPF()
        {
            MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
            List<MainDataSet.ResponsavelRow> responsaveisDuplicados = new List<MainDataSet.ResponsavelRow>();
            int responsavelCount = 0;

            foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
            {
                List<MainDataSet.ResponsavelRow> responsavelDuplicado = new List<MainDataSet.ResponsavelRow>();
                for (int i=responsavelCount+1; i<responsaveis.Count;i++)
                {
                    if(responsavelAtual.Nome == responsaveis[i].Nome || responsavelAtual.CPF == responsaveis[i].CPF)
                    {
                        responsavelDuplicado.Add(responsaveis[i]);
                    }
                }

                if(responsavelDuplicado.Count > 0) { responsavelDuplicado.Add(responsavelAtual); }

                foreach(MainDataSet.ResponsavelRow responsavelConcorrente in responsavelDuplicado)
                {
                    bool entrarNaLista = true;
                    for(int i=0; i<responsaveisDuplicados.Count; i++)
                    {
                        if(responsavelConcorrente == responsaveisDuplicados[i])
                        { entrarNaLista = false; break; }
                    }

                    if(entrarNaLista) { responsaveisDuplicados.Add(responsavelConcorrente); }
                }
                responsavelCount++;
            }

            MainDataSet.ResponsavelDataTable responsaveisDeRetorno = new MainDataSet.ResponsavelDataTable();
            foreach(MainDataSet.ResponsavelRow resp in responsaveisDuplicados)
            {
                MainDataSet.ResponsavelRow r = resp;
                responsaveisDeRetorno.ImportRow(r);
            }

            return responsaveisDeRetorno;
        }

        static public bool CheckDuplicatas()
        {
            MainDataSet.AlunoDataTable alunos = ConnectionHelper.AlunoTable.GetData();
            int alunoCount = 0;
            bool hasDuplicates = false;

            foreach (MainDataSet.AlunoRow alunoAtual in alunos)
            {
                List<MainDataSet.AlunoRow> alunoDuplicado = new List<MainDataSet.AlunoRow>();
                for (int i = alunoCount + 1; i < alunos.Count; i++)
                {
                    if (alunoAtual.Nome == alunos[i].Nome)
                    {
                        hasDuplicates = true;
                        break;
                    }
                }

                if (hasDuplicates) break;
            }

            if (!hasDuplicates)
            {
                MainDataSet.ResponsavelDataTable responsaveis = ConnectionHelper.ResponsavelTable.GetData();
                int responsavelCount = 0;

                foreach (MainDataSet.ResponsavelRow responsavelAtual in responsaveis)
                {
                    List<MainDataSet.ResponsavelRow> responsavelDuplicado = new List<MainDataSet.ResponsavelRow>();
                    for (int i = responsavelCount + 1; i < responsaveis.Count; i++)
                    {
                        if (responsavelAtual.Nome == responsaveis[i].Nome || responsavelAtual.CPF == responsaveis[i].CPF)
                        {
                            hasDuplicates = true;
                            break;
                        }
                    }

                    if (hasDuplicates) break;
                }
            }
            return hasDuplicates;
        }

        #endregion DuplicatasFunctions

        static private void FillMainDataset()
        {
            ConnectionHelper.AlunoTable.Fill(maindataSet.Aluno);
            ConnectionHelper.ResponsavelTable.Fill(maindataSet.Responsavel);
            ConnectionHelper.PagamentoTable.Fill(maindataSet.Pagamento);
            ConnectionHelper.PontuacaoTable.Fill(maindataSet.Pontuacao);
        }

        static public bool SaveOfflineDataIntoDataBase()
        {
            bool HasData = false;
            ConnectionHelper.CatchOfflineSavedData();
            OfflineData = ConnectionHelper.OfflineData;
            OfflineAluno[] aluno = OfflineData.OfflineAlunoDataTable.GetSavedData();
            OfflineResponsavel[] responsavel = OfflineData.OfflineResponsavelDataTable.GetSavedData();
            OfflinePagamento[] pagamento = OfflineData.OfflinePagamentoDataTable.GetSavedData();
            OfflinePontuacao[] pontuacao = OfflineData.OfflinePontuacaoDataTable.GetSavedData();

            FillMainDataset();

            int size = aluno.Length + responsavel.Length + pagamento.Length + pontuacao.Length;
            if(size > 0) { HasData = true; }
            object[] objetosOrdenados = new object[size];

            foreach(OfflineAluno tmpAluno in aluno)
            { objetosOrdenados.SetValue(tmpAluno, tmpAluno.OfflineIDComand); }

            foreach(OfflineResponsavel tmpResponsavel in responsavel)
            { objetosOrdenados.SetValue(tmpResponsavel, tmpResponsavel.OfflineIDComand); }

            foreach(OfflinePagamento tmpPagamento in pagamento)
            { objetosOrdenados.SetValue(tmpPagamento, tmpPagamento.OfflineIDComand); }

            foreach(OfflinePontuacao tmpPontuacao in pontuacao)
            { objetosOrdenados.SetValue(tmpPontuacao, tmpPontuacao.OfflineIDComand); }


            for (int i = 0; i < objetosOrdenados.Length; i++)
            {
                OfflineAluno alunoCandidato;
                OfflineResponsavel responsavelCandidato;
                OfflinePagamento pagamentoCandidato;
                OfflinePontuacao pontuacaoCandidato;

                try
                {
                    alunoCandidato = (OfflineAluno)objetosOrdenados[i];
                    AlunoTableAdapter alunoTable = ConnectionHelper.AlunoTable;
                    MainDataSet.AlunoRow tmpAlunoRow = maindataSet.Aluno.FindByID(alunoCandidato.ID);

                    switch (alunoCandidato.Comando)
                    {
                        case ComandType.Insert:
                            alunoTable.Insert(alunoCandidato.ID, alunoCandidato.Nome, alunoCandidato.Nascimento, alunoCandidato.Telefone1,
                                alunoCandidato.Telefone2, alunoCandidato.Telefone3, alunoCandidato.Responsavel, alunoCandidato.Matricula);
                            break;

                        case ComandType.Update:
                            tmpAlunoRow.Nome = alunoCandidato.Nome;
                            tmpAlunoRow.Nascimento = alunoCandidato.Nascimento;
                            tmpAlunoRow.Telefone1 = alunoCandidato.Telefone1;
                            tmpAlunoRow.Telefone2 = alunoCandidato.Telefone2;
                            tmpAlunoRow.Telefone3 = alunoCandidato.Telefone3;
                            tmpAlunoRow.Responsável = alunoCandidato.Responsavel;
                            tmpAlunoRow.Matricula = alunoCandidato.Matricula.Value;

                            DataSet tmpDataset = maindataSet.GetChanges();
                            ConnectionHelper.AlunoTable.Update(tmpAlunoRow);

                            maindataSet.Merge(tmpDataset);
                            maindataSet.AcceptChanges();
                            break;

                        case ComandType.Delete:
                            ConnectionHelper.AlunoTable.Delete(tmpAlunoRow.ID, tmpAlunoRow.Nome, tmpAlunoRow.Nascimento, tmpAlunoRow.Telefone1,
                                tmpAlunoRow.Telefone2, tmpAlunoRow.Telefone3, tmpAlunoRow.Responsável, tmpAlunoRow.Matricula);
                            tmpAlunoRow.Delete();
                            break;
                    }
                    continue;
                }
                catch { }

                try
                {
                    responsavelCandidato = (OfflineResponsavel)objetosOrdenados[i];
                    ResponsavelTableAdapter responsavelTable = ConnectionHelper.ResponsavelTable;
                    MainDataSet.ResponsavelRow tmpResponsavelRow = maindataSet.Responsavel.FindByID(responsavelCandidato.ID);

                    switch (responsavelCandidato.Comando)
                    {
                        case ComandType.Insert:
                            responsavelTable.Insert(responsavelCandidato.Nome, responsavelCandidato.CPF, responsavelCandidato.Telefone1,
                                responsavelCandidato.Telefone2, responsavelCandidato.Telefone3, responsavelCandidato.Nascimento);
                            break;

                        case ComandType.Update:
                            tmpResponsavelRow.Nome = responsavelCandidato.Nome;
                            tmpResponsavelRow.CPF = responsavelCandidato.CPF;
                            tmpResponsavelRow.Telefone1 = responsavelCandidato.Telefone1;
                            tmpResponsavelRow.Telefone2 = responsavelCandidato.Telefone2;
                            tmpResponsavelRow.Telefone3 = responsavelCandidato.Telefone3;
                            tmpResponsavelRow.Nascimento = responsavelCandidato.Nascimento.Value;

                            DataSet tmpDataset = maindataSet.GetChanges();
                            ConnectionHelper.ResponsavelTable.Update(tmpResponsavelRow);
                            maindataSet.Merge(tmpDataset);
                            maindataSet.AcceptChanges();
                            break;

                        case ComandType.Delete:
                            ConnectionHelper.ResponsavelTable.Delete(tmpResponsavelRow.ID, tmpResponsavelRow.Nome, tmpResponsavelRow.CPF, tmpResponsavelRow.Telefone1,
                                tmpResponsavelRow.Telefone2, tmpResponsavelRow.Telefone3, tmpResponsavelRow.Nascimento);
                            tmpResponsavelRow.Delete();
                            break;

                    }
                    continue;
                }
                catch { }

                try
                {
                    pagamentoCandidato = (OfflinePagamento)objetosOrdenados[i];
                    PagamentoTableAdapter pagamentoTable = ConnectionHelper.PagamentoTable;
                    MainDataSet.PagamentoRow tmpPagamentoRow = maindataSet.Pagamento.FindByID(pagamentoCandidato.ID);

                    switch (pagamentoCandidato.Comando)
                    {
                        case ComandType.Insert:
                            pagamentoTable.Insert(pagamentoCandidato.Aluno, pagamentoCandidato.Referencia, pagamentoCandidato.DataPagamento, pagamentoCandidato.Valor, pagamentoCandidato.Rematricula);
                            break;

                        case ComandType.Update:
                            tmpPagamentoRow.Aluno = pagamentoCandidato.Aluno;
                            tmpPagamentoRow.Referencia = pagamentoCandidato.Referencia;
                            tmpPagamentoRow.DataPagamento = pagamentoCandidato.DataPagamento.Value;
                            tmpPagamentoRow.Valor = pagamentoCandidato.Valor;
                            tmpPagamentoRow.Rematricula = pagamentoCandidato.Rematricula;

                            DataSet tmpDataset = maindataSet.GetChanges();
                            ConnectionHelper.PagamentoTable.Update(tmpPagamentoRow);
                            maindataSet.Merge(tmpDataset);
                            maindataSet.AcceptChanges();
                            break;

                        case ComandType.Delete:
                            ConnectionHelper.PagamentoTable.Delete(tmpPagamentoRow.ID, tmpPagamentoRow.Aluno, tmpPagamentoRow.Referencia,
                                tmpPagamentoRow.DataPagamento, tmpPagamentoRow.Valor, tmpPagamentoRow.Rematricula);
                            tmpPagamentoRow.Delete();
                            break;
                    }
                    continue;
                }
                catch { }

                try
                {
                    pontuacaoCandidato = (OfflinePontuacao)objetosOrdenados[i];
                    PontuacaoTableAdapter pontuacaoTable = ConnectionHelper.PontuacaoTable;
                    MainDataSet.PontuacaoRow tmpPontuacaoRow = maindataSet.Pontuacao.FindByID(pontuacaoCandidato.ID);

                    switch (pontuacaoCandidato.Comando)
                    {
                        case ComandType.Insert:
                            pontuacaoTable.Insert(pontuacaoCandidato.Aluno, pontuacaoCandidato.Modulo, pontuacaoCandidato.Licao,
                                pontuacaoCandidato.Data, pontuacaoCandidato.Pontuacao);
                            break;

                        case ComandType.Update:
                            tmpPontuacaoRow.Aluno = pontuacaoCandidato.Aluno;
                            tmpPontuacaoRow.Modulo = pontuacaoCandidato.Modulo;
                            tmpPontuacaoRow.Licao = pontuacaoCandidato.Licao;
                            tmpPontuacaoRow.Data = pontuacaoCandidato.Data.Value;
                            tmpPontuacaoRow.Resultado = pontuacaoCandidato.Pontuacao;

                            DataSet tmpDataset = maindataSet.GetChanges();
                            ConnectionHelper.PontuacaoTable.Update(tmpPontuacaoRow);
                            maindataSet.Merge(tmpDataset);
                            maindataSet.AcceptChanges();
                            break;

                        case ComandType.Delete:
                            ConnectionHelper.PontuacaoTable.Delete(tmpPontuacaoRow.ID, tmpPontuacaoRow.Aluno, tmpPontuacaoRow.Modulo,
                                tmpPontuacaoRow.Licao, tmpPontuacaoRow.Data, tmpPontuacaoRow.Resultado);
                            tmpPontuacaoRow.Delete();
                            break;
                    }
                    continue;
                }
                catch { }
            }

            ConnectionHelper.ClearOfflineSavedData();
            return HasData;
        }
    }
}
