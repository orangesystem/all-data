﻿namespace AllData
{
    partial class EditResponsavel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditResponsavel));
            this.HeaderGBox = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.responsavelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSet = new AllData.MainDataSet();
            this.label7 = new System.Windows.Forms.Label();
            this.RemoveBtn = new System.Windows.Forms.Button();
            this.EditBtn = new System.Windows.Forms.Button();
            this.RespComb = new System.Windows.Forms.ComboBox();
            this.ContatoGBox = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Tel3 = new System.Windows.Forms.MaskedTextBox();
            this.Tel2 = new System.Windows.Forms.MaskedTextBox();
            this.Tel1 = new System.Windows.Forms.MaskedTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabelInfoResp = new System.Windows.Forms.ToolStripStatusLabel();
            this.AcaoPainel = new System.Windows.Forms.Panel();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.commitbtn = new System.Windows.Forms.Button();
            this.InfoGBox = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Birthdate = new System.Windows.Forms.DateTimePicker();
            this.CPFBox = new System.Windows.Forms.MaskedTextBox();
            this.NameBox = new System.Windows.Forms.TextBox();
            this.responsavelTableAdapter = new AllData.MainDataSetTableAdapters.ResponsavelTableAdapter();
            this.HeaderGBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.responsavelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).BeginInit();
            this.ContatoGBox.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.AcaoPainel.SuspendLayout();
            this.InfoGBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeaderGBox
            // 
            this.HeaderGBox.Controls.Add(this.label8);
            this.HeaderGBox.Controls.Add(this.label7);
            this.HeaderGBox.Controls.Add(this.RemoveBtn);
            this.HeaderGBox.Controls.Add(this.EditBtn);
            this.HeaderGBox.Controls.Add(this.RespComb);
            this.HeaderGBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderGBox.Location = new System.Drawing.Point(0, 0);
            this.HeaderGBox.Name = "HeaderGBox";
            this.HeaderGBox.Size = new System.Drawing.Size(326, 77);
            this.HeaderGBox.TabIndex = 0;
            this.HeaderGBox.TabStop = false;
            this.HeaderGBox.Text = "Responsável";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.responsavelBindingSource, "ID", true));
            this.label8.Location = new System.Drawing.Point(256, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "ID";
            // 
            // responsavelBindingSource
            // 
            this.responsavelBindingSource.DataMember = "Responsavel";
            this.responsavelBindingSource.DataSource = this.mainDataSet;
            // 
            // mainDataSet
            // 
            this.mainDataSet.DataSetName = "MainDataSet";
            this.mainDataSet.EnforceConstraints = false;
            this.mainDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(232, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "ID";
            // 
            // RemoveBtn
            // 
            this.RemoveBtn.Location = new System.Drawing.Point(60, 44);
            this.RemoveBtn.Name = "RemoveBtn";
            this.RemoveBtn.Size = new System.Drawing.Size(75, 23);
            this.RemoveBtn.TabIndex = 1;
            this.RemoveBtn.Text = "Deletar";
            this.RemoveBtn.UseVisualStyleBackColor = true;
            this.RemoveBtn.Click += new System.EventHandler(this.RemoveBtn_Click);
            // 
            // EditBtn
            // 
            this.EditBtn.Location = new System.Drawing.Point(141, 46);
            this.EditBtn.Name = "EditBtn";
            this.EditBtn.Size = new System.Drawing.Size(75, 23);
            this.EditBtn.TabIndex = 1;
            this.EditBtn.Text = "Editar";
            this.EditBtn.UseVisualStyleBackColor = true;
            this.EditBtn.Click += new System.EventHandler(this.EditBtn_Click);
            // 
            // RespComb
            // 
            this.RespComb.DataSource = this.responsavelBindingSource;
            this.RespComb.DisplayMember = "Nome";
            this.RespComb.FormattingEnabled = true;
            this.RespComb.Location = new System.Drawing.Point(15, 19);
            this.RespComb.Name = "RespComb";
            this.RespComb.Size = new System.Drawing.Size(200, 21);
            this.RespComb.TabIndex = 0;
            this.RespComb.ValueMember = "ID";
            // 
            // ContatoGBox
            // 
            this.ContatoGBox.Controls.Add(this.label3);
            this.ContatoGBox.Controls.Add(this.label2);
            this.ContatoGBox.Controls.Add(this.label1);
            this.ContatoGBox.Controls.Add(this.Tel3);
            this.ContatoGBox.Controls.Add(this.Tel2);
            this.ContatoGBox.Controls.Add(this.Tel1);
            this.ContatoGBox.Dock = System.Windows.Forms.DockStyle.Right;
            this.ContatoGBox.Enabled = false;
            this.ContatoGBox.Location = new System.Drawing.Point(326, 0);
            this.ContatoGBox.Name = "ContatoGBox";
            this.ContatoGBox.Size = new System.Drawing.Size(182, 188);
            this.ContatoGBox.TabIndex = 1;
            this.ContatoGBox.TabStop = false;
            this.ContatoGBox.Text = "Contato";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Telefone 3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Telefone 2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Telefone 1";
            // 
            // Tel3
            // 
            this.Tel3.Location = new System.Drawing.Point(70, 72);
            this.Tel3.Mask = "(99) 9 0000-0000";
            this.Tel3.Name = "Tel3";
            this.Tel3.PromptChar = ' ';
            this.Tel3.Size = new System.Drawing.Size(100, 20);
            this.Tel3.TabIndex = 7;
            // 
            // Tel2
            // 
            this.Tel2.Location = new System.Drawing.Point(70, 46);
            this.Tel2.Mask = "(99) 9 0000-0000";
            this.Tel2.Name = "Tel2";
            this.Tel2.PromptChar = ' ';
            this.Tel2.Size = new System.Drawing.Size(100, 20);
            this.Tel2.TabIndex = 6;
            // 
            // Tel1
            // 
            this.Tel1.Location = new System.Drawing.Point(70, 20);
            this.Tel1.Mask = "(99) 9 0000-0000";
            this.Tel1.Name = "Tel1";
            this.Tel1.PromptChar = ' ';
            this.Tel1.Size = new System.Drawing.Size(100, 20);
            this.Tel1.TabIndex = 5;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabelInfoResp});
            this.statusStrip1.Location = new System.Drawing.Point(0, 217);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(508, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabelInfoResp
            // 
            this.StatusLabelInfoResp.Name = "StatusLabelInfoResp";
            this.StatusLabelInfoResp.Size = new System.Drawing.Size(39, 17);
            this.StatusLabelInfoResp.Text = "Status";
            // 
            // AcaoPainel
            // 
            this.AcaoPainel.Controls.Add(this.CancelBtn);
            this.AcaoPainel.Controls.Add(this.commitbtn);
            this.AcaoPainel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.AcaoPainel.Enabled = false;
            this.AcaoPainel.Location = new System.Drawing.Point(0, 188);
            this.AcaoPainel.Name = "AcaoPainel";
            this.AcaoPainel.Size = new System.Drawing.Size(508, 29);
            this.AcaoPainel.TabIndex = 3;
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Location = new System.Drawing.Point(349, 3);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 23);
            this.CancelBtn.TabIndex = 1;
            this.CancelBtn.Text = "Cancelar";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // commitbtn
            // 
            this.commitbtn.Location = new System.Drawing.Point(430, 3);
            this.commitbtn.Name = "commitbtn";
            this.commitbtn.Size = new System.Drawing.Size(75, 23);
            this.commitbtn.TabIndex = 8;
            this.commitbtn.Text = "Confirmar";
            this.commitbtn.UseVisualStyleBackColor = true;
            this.commitbtn.Click += new System.EventHandler(this.commitbtn_Click);
            // 
            // InfoGBox
            // 
            this.InfoGBox.Controls.Add(this.label6);
            this.InfoGBox.Controls.Add(this.label5);
            this.InfoGBox.Controls.Add(this.label4);
            this.InfoGBox.Controls.Add(this.Birthdate);
            this.InfoGBox.Controls.Add(this.CPFBox);
            this.InfoGBox.Controls.Add(this.NameBox);
            this.InfoGBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoGBox.Enabled = false;
            this.InfoGBox.Location = new System.Drawing.Point(0, 77);
            this.InfoGBox.Name = "InfoGBox";
            this.InfoGBox.Size = new System.Drawing.Size(326, 111);
            this.InfoGBox.TabIndex = 4;
            this.InfoGBox.TabStop = false;
            this.InfoGBox.Text = "Informações";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Nascimento";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "CPF";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nome";
            // 
            // Birthdate
            // 
            this.Birthdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Birthdate.Location = new System.Drawing.Point(97, 85);
            this.Birthdate.Name = "Birthdate";
            this.Birthdate.ShowCheckBox = true;
            this.Birthdate.Size = new System.Drawing.Size(210, 20);
            this.Birthdate.TabIndex = 4;
            // 
            // CPFBox
            // 
            this.CPFBox.Location = new System.Drawing.Point(97, 59);
            this.CPFBox.Mask = "000,000,000-00";
            this.CPFBox.Name = "CPFBox";
            this.CPFBox.PromptChar = ' ';
            this.CPFBox.Size = new System.Drawing.Size(100, 20);
            this.CPFBox.TabIndex = 3;
            // 
            // NameBox
            // 
            this.NameBox.Location = new System.Drawing.Point(97, 33);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(100, 20);
            this.NameBox.TabIndex = 2;
            // 
            // responsavelTableAdapter
            // 
            this.responsavelTableAdapter.ClearBeforeFill = true;
            // 
            // EditResponsavel
            // 
            this.AcceptButton = this.commitbtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(508, 239);
            this.Controls.Add(this.InfoGBox);
            this.Controls.Add(this.HeaderGBox);
            this.Controls.Add(this.ContatoGBox);
            this.Controls.Add(this.AcaoPainel);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditResponsavel";
            this.Text = "Editar Informações";
            this.Load += new System.EventHandler(this.EditResponsavel_Load);
            this.HeaderGBox.ResumeLayout(false);
            this.HeaderGBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.responsavelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSet)).EndInit();
            this.ContatoGBox.ResumeLayout(false);
            this.ContatoGBox.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.AcaoPainel.ResumeLayout(false);
            this.InfoGBox.ResumeLayout(false);
            this.InfoGBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox HeaderGBox;
        private System.Windows.Forms.ComboBox RespComb;
        private System.Windows.Forms.Button RemoveBtn;
        private System.Windows.Forms.Button EditBtn;
        private System.Windows.Forms.GroupBox ContatoGBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox Tel3;
        private System.Windows.Forms.MaskedTextBox Tel2;
        private System.Windows.Forms.MaskedTextBox Tel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel AcaoPainel;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button commitbtn;
        private System.Windows.Forms.GroupBox InfoGBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker Birthdate;
        private System.Windows.Forms.MaskedTextBox CPFBox;
        private System.Windows.Forms.TextBox NameBox;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabelInfoResp;
        private MainDataSet mainDataSet;
        private System.Windows.Forms.BindingSource responsavelBindingSource;
        private MainDataSetTableAdapters.ResponsavelTableAdapter responsavelTableAdapter;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
    }
}