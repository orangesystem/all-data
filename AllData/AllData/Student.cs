﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using AllData.MainDataSetTableAdapters;

namespace AllData
{
    public partial class Student : Form
    {
        private bool controlesValidos = false;
        public Student()
        {
            InitializeComponent();
        }

        //Botão de confirmar dados
        private void commitBtn1_Click(object sender, EventArgs e)
        {
            DateTime?  mDay;
            DateTime bday;
            if (BirthdayDate1.Checked) { bday = BirthdayDate1.Value; } else { bday = new DateTime(); }
            if (RegisteringDate1.Checked) { mDay = RegisteringDate1.Value; } else { mDay = null; }
            
            MainDataSet.AlunoRow alunoValido = null;
            try { alunoValido = ValidarControles(); }
            catch (InvalidControlValue) { MessageBox.Show("Algum(ns) campo(s) não estão preenchidos corretamente!"); }

            if(controlesValidos)
            switch (ConnectionHelper.Online)
            {
                case true:
                    try
                    {
                        AlunoTableAdapter aluno = ConnectionHelper.AlunoTable;

                        StatusLabelStudent.Text = "Validando nome... por favor aguarde!";
                        if (DataHelper.Exist(alunoValido.Nome, DataHelper.AlunoCheckingtype.Nome))
                        {
                            if (MessageBox.Show("Já existe um registro com o nome do aluno que está tentando inserir, deseja continuar mesmo assim?", "Ocorrencia de possível duplicata detectada!", MessageBoxButtons.OKCancel) == DialogResult.OK)
                            {
                                aluno.Insert(Guid.NewGuid(), alunoValido.Nome, bday, alunoValido.Telefone1, alunoValido.Telefone2, alunoValido.Telefone3, alunoValido.Responsável, mDay);
                                StatusLabelStudent.Text = "Dados inseridos com sucesso!";
                                ClearControls();
                            }

                            else
                            {
                                StatusLabelStudent.Text = "Operação cancelada com sucesso!";
                                ClearControls();
                            }
                        }
                        else
                        {
                            aluno.Insert(Guid.NewGuid(), alunoValido.Nome, bday, alunoValido.Telefone1, alunoValido.Telefone2, alunoValido.Telefone3, alunoValido.Responsável, mDay);
                            StatusLabelStudent.Text = "Dados inseridos com sucesso!";
                            ClearControls();
                        }

                    }
                    catch (SqlException message)
                    {
                        MessageBox.Show("Erro ao inserir dados: " + message.Message); StatusLabelStudent.Text = "Falha ao enviar dados ao servidor!";

                        if (message.ErrorCode == -2146232060)
                        {
                            if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                            {
                                InputForm IP = new InputForm();
                                if (IP.ShowDialog() == DialogResult.OK)
                                {
                                    ConnectionHelper.GoOfflineMode();
                                    OfflineAluno aluno = new OfflineAluno(Guid.NewGuid(), alunoValido.Nome, bday, alunoValido.Telefone1, alunoValido.Telefone2, alunoValido.Telefone3, alunoValido.Responsável, mDay, ComandType.Insert);
                                        ConnectionHelper.OfflineData.OfflineAlunoDataTable.Add(aluno);
                                    ConnectionHelper.SaveOfflineData();

                                    StatusLabelStudent.Text = "Dados salvos com sucesso!";
                                    ClearControls();
                                }
                            }
                        }
                    }
                        catch(Exception err)
                        {
                            MessageBox.Show("Ocorreu um erro inseperado! \nCaso o mesmo volte a repetir, entre em contato com o suporte.\n" + err.Message);
                        }
                    break;

                case false:
                    OfflineAluno aluno2 = new OfflineAluno(Guid.NewGuid(), alunoValido.Nome, bday, alunoValido.Telefone1, alunoValido.Telefone2, alunoValido.Telefone3, alunoValido.Responsável, mDay, ComandType.Insert);
                        ConnectionHelper.OfflineData.OfflineAlunoDataTable.Add(aluno2);
                    ConnectionHelper.SaveOfflineData();

                    StatusLabelStudent.Text = "Dados salvos com sucesso!";
                    ClearControls();
                    break;
            }

        }

        /// <summary>
        /// Limpa todos os dados de todos os controles da janela
        /// </summary>
        void ClearControls()
        {
            NameBox1.Clear();
            Tel1Box1.Clear();
            Tel2Box1.Clear();
            Tel3Box1.Clear();
            BirthdayDate1.Value = DateTime.Today;
            RegisteringDate1.Value = DateTime.Today;
        }

        private void Student_Load(object sender, EventArgs e)
        {
            if (ConnectionHelper.Online)
            {
                try
                { ConnectionHelper.ResponsavelTable.Fill((mainDataSet.Responsavel)); }
                catch (SqlException err)
                {
                    MessageBox.Show("Erro ao carregar dados: " + err.Message);

                    if (err.ErrorCode == -2146232060)
                    {
                        if (MessageBox.Show(Internal.ContinuarOffline, "Trabalhar offline?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            InputForm IP = new InputForm();
                            if (IP.ShowDialog() == DialogResult.OK)
                            {
                                ConnectionHelper.GoOfflineMode();
                            }
                            else { Close(); }
                        }
                    }

                    else { Close(); }
                }
                catch(Exception err)
                {
                    MessageBox.Show("Ocorreu um erro inesperado!\n" + err.Message + Internal.ErroEntreEmContato);
                }
            }
            else
            {
                MessageBox.Show("Indisponível no modo offline!");
                Close();
            }
        }

        private void Cancelbtn_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Os dados serão descartados, deseja prosseguir?", "Cancelar dados", MessageBoxButtons.OKCancel) == DialogResult.OK)
            { ClearControls(); }
        }

        private MainDataSet.AlunoRow ValidarControles()
        {
            MainDataSet.AlunoDataTable a = mainDataSet.Aluno;
            MainDataSet.AlunoRow alunoValidado = a.NewAlunoRow();
            #region CheckNome
            if(NameBox1.Text.Length > 0)
            {
                if(NameBox1.Text == "" || NameBox1.Text == " ")
                { controlesValidos = false; throw new InvalidControlValue("O controle " + NameBox1.Name + " estava com dados incorretos"); }

                else { alunoValidado.Nome = NameBox1.Text; controlesValidos = true; }
            }
            else { controlesValidos = false; throw new InvalidControlValue("O controle " + NameBox1.Name + " estava com dados incorretos"); }
            #endregion

            #region CheckResponsavel
            int responsavelId;
            try
            { responsavelId = (int)RespComb1.SelectedValue; }
            catch { responsavelId = new int(); }

            alunoValidado.Responsável = responsavelId;
            #endregion

            #region CheckPhones
            string tel1 = Tel1Box1.TextNoFormatting();
            string tel2 = Tel2Box1.TextNoFormatting();
            string tel3 = Tel3Box1.TextNoFormatting();

            alunoValidado.Telefone1 = tel1;
            alunoValidado.Telefone2 = tel2;
            alunoValidado.Telefone3 = tel3;
            #endregion

            return alunoValidado;
        }
    }

    [Serializable]
    public class InvalidControlValue : Exception
    {
        public InvalidControlValue() { }
        public InvalidControlValue(string message) : base(message) 
        {
        }
        public InvalidControlValue(string message, Exception inner) : base(message, inner)
        {
        }
        protected InvalidControlValue(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context)
        { }
    }
}
